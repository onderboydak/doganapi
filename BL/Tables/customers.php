<?php
require_once dirname ( dirname ( dirname ( __FILE__ ) ) ) . "/DL/DAL.php";
use data\TableItem;
class customers extends TableItem {
	// fields
	public $ID;
	public $customer;
	public $customerType;
	public $address;
	public $vatNumber;
	public $vatAuthority;
	public $commerceAuthorityNo;
	public $phone;
	public $email;
	public $customerContact;
	public $customerContactEmail;
	public $buildingNumber;
	public $district;
	public $city;
	public $country;
	public $isVendor;
	public $asID;
	public $dueDateOptions;
	public $date_;
	public $pbox;
	public $parentID;

	// Counctructor
	function __construct($ID = NULL) {
		parent::__construct ();
		$this->ID = $ID;
		$this->settable ( "customers" );
		$this->refresh ( $ID );
	}
	function __set($property, $value) {
		$this->$property = $value;
	}
	function __get($property) {
		if (isset ( $this->$property )) {
			return $this->$property;
		}
	}

	function getCustomers ($customerID,$asID,$isVendor){
		$sql = "call getCustomers($customerID,$asID,$isVendor)";
		return $this->executenonquery($sql,true);

		//$sql = "select * from customers where $customerID=0 or ID=$customerID order by customer";
		//return $this->executenonquery($sql,true);
	}

	public static function getCustomerFromtckVKNNo ($vatNumber,$asID) {
		$intc = new self();
		$sql = "select * from customers where asID=".$asID." and vatNumber='".$vatNumber."' order by ID limit 1";
		$intc->refreshprocedure($sql);
		return $intc;
	}

	
}
?>
