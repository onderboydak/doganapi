<?php
require_once dirname ( dirname ( dirname ( __FILE__ ) ) ) . "/DL/DAL.php";
use data\TableItem;
class bankAccess extends TableItem {
	// fields
	public $ID;
	public $asID;
	public $bankID;
	public $ftp_server;
	public $ftp_port;
	public $ftp_user;
	public $ftp_pass;
	
	// Counctructor
	function __construct($ID = NULL) {
		parent::__construct ();
		$this->ID = $ID;
		$this->settable ( "bankAccess" );
		$this->refresh ( $ID );
	}
	function __set($property, $value) {
		$this->$property = $value;
	}
	function __get($property) {
		if (isset ( $this->$property )) {
			return $this->$property;
		}
	}	

	
}
?>
