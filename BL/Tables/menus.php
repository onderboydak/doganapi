<?php
require_once dirname ( dirname ( dirname ( __FILE__ ) ) ) . "/DL/DAL.php";
use data\TableItem;
class menus extends TableItem {
	// fields
	public $ID;
	public $menu;
	public $isVisible;


	// Counctructor
	function __construct($ID = NULL) {
		parent::__construct ();
		$this->ID = $ID;
		$this->settable ( "menus" );
		$this->refresh ( $ID );
	}
	function __set($property, $value) {
		$this->$property = $value;
	}
	function __get($property) {
		if (isset ( $this->$property )) {
			return $this->$property;
		}
	}	

	function getMenus ($userID) {
		$sql = "call getMenus($userID)";	
		return $this->executenonquery($sql,true);
	}

	
}
?>
