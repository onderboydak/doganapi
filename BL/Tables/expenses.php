<?php
require_once dirname ( dirname ( dirname ( __FILE__ ) ) ) . "/DL/DAL.php";
use data\TableItem;
class expenses extends TableItem {
	// fields
	public $ID;
	public $merchant;
	public $expenseDate;
	public $expenseAccount;
	public $expenseCategory;
	public $currency;
	public $amount;
	public $vatAmount;
	public $totalAmount;
	public $notes;
	public $asID;
	public $expenseNumber;
	public $vatRate;
	public $status;
	public $accounted;

	
	// Counctructor
	function __construct($ID = NULL) {
		parent::__construct ();
		$this->ID = $ID;
		$this->settable ( "expenses" );
		$this->refresh ( $ID );
	}
	function __set($property, $value) {
		$this->$property = $value;
	}
	function __get($property) {
		if (isset ( $this->$property )) {
			return $this->$property;
		}
	}

	function getExpenses ($expenseID,$asID) {
		$sql = "call getExpenses($expenseID,$asID)";
		return $this->executenonquery($sql,true);
	}


	 public static function getExpenseFromNumber ($expenseNumber) {
		$intc = new self();
		$sql = "select * from expenses where expenseNumber='".$expenseNumber."' limit 1";
		$intc->refreshprocedure($sql);
		return $intc;
	} 

}
?>
