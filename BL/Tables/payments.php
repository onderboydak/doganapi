<?php
require_once dirname ( dirname ( dirname ( __FILE__ ) ) ) . "/DL/DAL.php";
use data\TableItem;
class payments extends TableItem {
	// fields
	public $ID;
	public $invoiceID;
	public $paymentDate;
	public $amount;
	public $paymentAccount;
	public $paymentMethod;
	public $notes;

	// Counctructor
	function __construct($ID = NULL) {
		parent::__construct ();
		$this->ID = $ID;
		$this->settable ( "payments" );
		$this->refresh ( $ID );
	}
	function __set($property, $value) {
		$this->$property = $value;
	}
	function __get($property) {
		if (isset ( $this->$property )) {
			return $this->$property;
		}
	}

	public static function getPaymentFromInvoice ($invoiceID) {
		$intc = new self();
		$sql = "select * from payments where invoiceID=".$invoiceID." limit 1";
		$intc->refreshprocedure($sql);
		return $intc;
	}

	function getAggr($asID,$customerID,$startDate,$endDate,$currencyID) {
		$sql = "call getAggrInvoices($asID,$customerID,'$startDate','$endDate',$currencyID)";
		return $this->executenonquery($sql,true);
	}

	function getBalance($startDate,$endDate,$currencyID,$asID) {
		$sql = "call getBalance('$startDate','$endDate',$currencyID,$asID)";
		return $this->executenonquery($sql,true);
	}

	function getBalanceDetails($startDate,$endDate,$currencyID,$asID,$accountID) {
		$sql = "call getBalanceDetails('$startDate','$endDate',$currencyID,$asID,$accountID)";
		return $this->executenonquery($sql,true);
	}
	
}
?>
