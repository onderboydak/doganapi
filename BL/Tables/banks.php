<?php
require_once dirname ( dirname ( dirname ( __FILE__ ) ) ) . "/DL/DAL.php";
use data\TableItem;
class banks extends TableItem {
	// fields
	public $ID;
	public $name;
	public $countryID;
	public $bankID;
	public $SWIFT;
	public $phone;
	public $kepEmail;
	public $address;
	


	// Counctructor
	function __construct($ID = NULL) {
		parent::__construct ();
		$this->ID = $ID;
		$this->settable ( "banks" );
		$this->refresh ( $ID );
	}
	function __set($property, $value) {
		$this->$property = $value;
	}
	function __get($property) {
		if (isset ( $this->$property )) {
			return $this->$property;
		}
	}	

	function getBanks ($bankID) {
		$sql ="select banks.*, country.name as country
				from banks 
				left join country on country.ID = banks.countryID 
				where ($bankID=0 or banks.bankID=$bankID) order by banks.name";
		return $this->executenonquery($sql,true);
	}

	public static function getBankFrombankID ($bankID) {
		$intc = new self();
		$sql = "select * from banks where bankID=".$bankID;
		$intc->refreshprocedure($sql);
		return $intc;
	}

}
?>
