<?php
require_once dirname ( dirname ( dirname ( __FILE__ ) ) ) . "/DL/DAL.php";
use data\TableItem;
class xsltfiles extends TableItem {
	// fields
	public $ID;
	public $template;
	public $xsltfile;
	public $asID;
	public $date_;
	public $templateCode;
	
	// Counctructor
	function __construct($ID = NULL) {
		parent::__construct ();
		$this->ID = $ID;
		$this->settable ( "xsltfiles" );
		$this->refresh ( $ID );
	}
	function __set($property, $value) {
		$this->$property = $value;
	}
	function __get($property) {
		if (isset ( $this->$property )) {
			return $this->$property;
		}
	}	

	function getxsltfiles ($asID,$xsltID) {
		$sql = "call getXsltFiles($asID,$xsltID)";
		return $this->executenonquery($sql,true);
	}


	
}
?>
