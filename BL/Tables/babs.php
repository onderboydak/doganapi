<?php
require_once dirname ( dirname ( dirname ( __FILE__ ) ) ) . "/DL/DAL.php";
use data\TableItem;
class babs extends TableItem {
	// fields
	# concat('public $',COLUMN_NAME,';')
	public $ID;
	public $asID;
	public $supplier;
	public $customerID;
	public $customer;
	public $vatNumber;
	public $email;
	public $invoiceType;
	public $formType;
	public $amount;
	public $invoiceCount;
	public $year;
	public $month;
	public $date_;
	public $responseDate;
	public $response;
	public $responseFile;

	
	// Counctructor
	function __construct($ID = NULL) {
		parent::__construct ();
		$this->ID = $ID;
		$this->settable ( "babs" );
		$this->refresh ( $ID );
	}
	function __set($property, $value) {
		$this->$property = $value;
	}
	function __get($property) {
		if (isset ( $this->$property )) {
			return $this->$property;
		}
	}	

	public static function getbabs ($token) {
		$intc = new self();
		$sql = "select * from babs where md5(concat(`month`,':',`year`,':',asID,':',customerID))='".$token."'";
		$intc->refreshprocedure($sql);
		return $intc;
	}

	function getbabswithsupplier($asID) {
		$sql = "call getBABSwithSupplier($asID)";
		return $this->executenonquery($sql,true);
	}

	
}
?>
