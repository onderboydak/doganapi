<?php
require_once dirname ( dirname ( dirname ( __FILE__ ) ) ) . "/DL/DAL.php";
use data\TableItem;
class chartOfAccounts extends TableItem {
	// fields
	public $ID;
	public $account;
	public $subAccount;
	public $accName;
	public $type;
	public $asID;

	// Counctructor
	function __construct($ID = NULL) {
		parent::__construct ();
		$this->ID = $ID;
		$this->settable ( "chartOfAccounts" );
		$this->refresh ( $ID );
	}
	function __set($property, $value) {
		$this->$property = $value;
	}
	function __get($property) {
		if (isset ( $this->$property )) {
			return $this->$property;
		}
	}

	function getCoa ($asID,$accounts){
		$sql = "call getChartOfAccounts ($asID,'$accounts')";
		//echo $sql;
		return $this->executenonquery($sql,true);
	}

	public static function getCoaFromAccount ($asID,$account) {
		$intc = new self();
		$sql = "select * from chartOfAccounts where asID=" . $intc->checkInjection($asID) . " and concat(account,'.',subAccount)='" . $intc->checkInjection($account) . "' order by ID desc limit 1";
		$intc->refreshprocedure($sql);
		return $intc;
	}

	
	
}
?>
