<?php
require_once dirname ( dirname ( dirname ( __FILE__ ) ) ) . "/DL/DAL.php";
use data\TableItem;
class banners extends TableItem {
	// fields
	# concat('public $',COLUMN_NAME,';')
	public $ID;
	public $banner;
	public $photo;
	public $startTime;
	public $endTime;

	
	// Counctructor
	function __construct($ID = NULL) {
		parent::__construct ();
		$this->ID = $ID;
		$this->settable ( "banners" );
		$this->refresh ( $ID );
	}
	function __set($property, $value) {
		$this->$property = $value;
	}
	function __get($property) {
		if (isset ( $this->$property )) {
			return $this->$property;
		}
	}	

	function getBanners ($bannerID) {
		$sql = "select banners.ID,banners.banner,banners.photo,date_format(banners.startTime,'%d-%m-%Y %H:%i') as startTime,
				date_format(banners.endTime,'%d-%m-%Y %H:%i') as endTime,files.fileName from banners 
				left outer join files on files.ID=banners.photo
		where ($bannerID=0 or banners.ID=$bannerID) order by banners.startTime desc";
		return $this->executenonquery($sql,true);
	}

	function showBanners () {
		$sql = "select banners.banner as title,concat('https://localhost/doganapi/Uploads/files/',files.fileName) as url from banners 
				left outer join files on files.ID=banners.photo
		where banners.startTime<=now() and banners.endTime>=now() order by banners.startTime desc";
		return $this->executenonquery($sql,true);
	}

	
	
}
?>
