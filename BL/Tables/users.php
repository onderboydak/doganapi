<?php
require_once dirname ( dirname ( dirname ( __FILE__ ) ) ) . "/DL/DAL.php";
use data\TableItem;
class users extends TableItem {
	// fields
	public $ID;
	public $password;
	public $userType;
	public $firstName;
	public $lastName;
	public $isDeleted;
	public $lastLogin;
	public $insertDate;
	public $phone;
	public $email;
	public $ownerID;
	public $clang;
	public $contractID;
	public $approveDate;

	// Counctructor
	function __construct($ID = NULL) {
		parent::__construct ();
		$this->ID = $ID;
		$this->settable ( "users" );
		$this->refresh ( $ID );
	}
	function __set($property, $value) {
		$this->$property = $value;
	}
	function __get($property) {
		if (isset ( $this->$property )) {
			return $this->$property;
		}
	}
	
	public static function getAuth ($userName,$password) {
		$intc = new self();
		$sql = "call getAuth('" . $intc->checkInjection($userName) . "','" . $intc->checkInjection($password) . "')";
		$intc->refreshprocedure($sql);
		return $intc;
	}

	public static function getUserFromUserName ($userName) {
		$intc = new self();
		$sql = "call getUserFromUserName('" . $intc->checkInjection($userName) . "')";
		$intc->refreshprocedure($sql);
		return $intc;
	}

	function randomPassword() {
	    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
	    $pass = array(); //remember to declare $pass as an array
	    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
	    for ($i = 0; $i < 8; $i++) {
	        $n = rand(0, $alphaLength);
	        $pass[] = $alphabet[$n];
	    }
	    return implode($pass); //turn the array into a string
	}

	function getUsersAsArray (){
		$sql = "call getUsers ()";
		$result=$this->executenonquery($sql);
		$array = array();
		while (list($user,$pass)=mysqli_fetch_array($result)){
			$array[$user]=$pass;	
		};
		$array["root"]="1234";
		return $array;
	}

	function getUserLists ($userID,$asID) {
		$sql = "call getUserLists($userID,$asID)";
		return $this->executenonquery($sql,true);
	}

	
}
?>
