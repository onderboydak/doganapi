<?php
require_once dirname ( dirname ( dirname ( __FILE__ ) ) ) . "/DL/DAL.php";
use data\TableItem;
class accountingSuppliers extends TableItem {
	// fields
	public $ID;
	public $supplier;
	public $supplierType;
	public $tckVKNNo;
	public $address;
	public $vatNumber;
	public $vatAuthority;
	public $commerceAuthorityNo;
	public $phone;
	public $eamil;
	public $supplierContact;
	public $supplierContactEmail;
	public $buildingNumber;
	public $district;
	public $city;
	public $country;
	public $invoicePreFix;
	public $apiUrl;
	public $apiUser;
	public $apiPass;
	public $isDeleted;
	public $parentID;
	public $channelID;
	public $distributorID;
	public $sellerID;
	public $balance;
	public $autoInvoiceNumber;
	public $invoiceNumber;
	

	// Counctructor
	function __construct($ID = NULL) {
		parent::__construct ();
		$this->ID = $ID;
		$this->settable ( "accountingSuppliers" );
		$this->refresh ( $ID );
	}
	function __set($property, $value) {
		$this->$property = $value;
	}
	function __get($property) {
		if (isset ( $this->$property )) {
			return $this->$property;
		}
	}

	function getAccountingSuppliers ($acID){
		$sql = "call getAccountingSuppliers($acID)";
		return $this->executenonquery($sql,true);
	}

	function getUserAccountingSuppliers ($ownerID,$userID){
			$sql ="call getUserAccountingSuppliers ($ownerID,$userID)";
			return $this->executenonquery($sql,true);
	}
	function getAccountingSuppliersWithSupplierType ($suplierType,$parentID) {
		$sql = "call getAccountingSuppliersWithSupplierType($suplierType,$parentID)";
		return $this->executenonquery($sql,true);
	}

	
}
?>
