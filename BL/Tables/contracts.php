<?php
require_once dirname ( dirname ( dirname ( __FILE__ ) ) ) . "/DL/DAL.php";
use data\TableItem;
class contracts extends TableItem {
	// fields
	# concat('public $',COLUMN_NAME,';')
	public $ID;
	public $contractTitle;
	public $contract;
	public $date_;
	public $contractDate;
	public $userID;
	public $isActive;
	
	// Counctructor
	function __construct($ID = NULL) {
		parent::__construct ();
		$this->ID = $ID;
		$this->settable ( "contracts" );
		$this->refresh ( $ID );
	}
	function __set($property, $value) {
		$this->$property = $value;
	}
	function __get($property) {
		if (isset ( $this->$property )) {
			return $this->$property;
		}
	}	

	function getContracts ($contractID) {
		$sql = "select contracts.*,concat(users.firstName,' ',users.lastName) as fullName from contracts 
				left outer join users on contracts.userID=users.ID
		where ($contractID=0 or contracts.ID=$contractID) order by contracts.contractDate desc";
		return $this->executenonquery($sql,true);
	}

	public static function getActiveContract () {
		$intc = new self();
		$sql = "select * from contracts 
		where isActive=1 order by contracts.contractDate desc limit 1";
		$intc->refreshprocedure($sql);
		return $intc;
	}
	
	
}
?>
