<?php
require_once dirname ( dirname ( dirname ( __FILE__ ) ) ) . "/DL/DAL.php";
use data\TableItem;
class bankAccounts extends TableItem {
	// fields
	public $ID;
	public $asID;
	public $account;
	public $bankID;
	public $branchID;
	public $accountID;
	public $IBAN;
	public $currency;



	// Counctructor
	function __construct($ID = NULL) {
		parent::__construct ();
		$this->ID = $ID;
		$this->settable ( "bankAccounts" );
		$this->refresh ( $ID );
	}
	function __set($property, $value) {
		$this->$property = $value;
	}
	function __get($property) {
		if (isset ( $this->$property )) {
			return $this->$property;
		}
	}

	function getBankAccounts ($asID,$accountID,$isBank){
		$sql = "call getBankAccounts ($asID,$accountID,$isBank)";
		return $this->executenonquery($sql,true);
		
	}

	function getLatestBalances ($asID,$accountID,$isBank){
		$sql = "call getLatestBalances ($asID,$accountID,$isBank)";
		return $this->executenonquery($sql,true);
		
	}

	public static function getBankAccountsFromAccountID ($accountID) {
		$intc = new self();
		$sql = "select * from bankAccounts where accountID='".$accountID."' limit 1";
		$intc->refreshprocedure($sql);
		return $intc;
	}

	

	
}
?>
