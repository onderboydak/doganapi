<?php
require_once dirname ( dirname ( dirname ( __FILE__ ) ) ) . "/DL/DAL.php";
use data\TableItem;
class invoicePeriods  extends TableItem {
	// fields
	public $ID;
	public $invoicePeriod;
	public $periodLastDate;
	public $invoiceID;
	public $date_;

	
	// Counctructor
	function __construct($ID = NULL) {
		parent::__construct ();
		$this->ID = $ID;
		$this->settable ( "invoicePeriods" );
		$this->refresh ( $ID );
	}
	function __set($property, $value) {
		$this->$property = $value;
	}
	function __get($property) {
		if (isset ( $this->$property )) {
			return $this->$property;
		}
	}	

	public static function getInvoicePeriodsFromInvoiceID ($invoiceID) {
		$intc = new self();
		$sql = "select * from invoicePeriods where invoiceID=" . $intc->checkInjection($invoiceID) ;
		$intc->refreshprocedure($sql);
		return $intc;
	}

	
}
?>
