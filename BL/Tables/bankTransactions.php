<?php
require_once dirname ( dirname ( dirname ( __FILE__ ) ) ) . "/DL/DAL.php";
use data\TableItem;
class bankTransactions extends TableItem {
	// fields
	public $ID;
	public $accountID;
	public $openingBalance;
	public $transaction;
	public $description;
	public $closingBalance;
	public $asID;
	public $tranDate;
	public $notes;
	public $relatedInvoiceID;
	

	// Counctructor
	function __construct($ID = NULL) {
		parent::__construct ();
		$this->ID = $ID;
		$this->settable ( "bankTransactions" );
		$this->refresh ( $ID );
	}
	function __set($property, $value) {
		$this->$property = $value;
	}
	function __get($property) {
		if (isset ( $this->$property )) {
			return $this->$property;
		}
	}

	function getBankTransactions ($accountID,$asID,$transactionID,$startDate,$endDate){
		$sql ="call getBankTransactions($accountID,$asID,$transactionID,'$startDate','$endDate')";
		/*$sql = "select ELT(MOD((bankTransactions.accountID),10), 
        'orangered' , 'green' , 'blue', 'red', 'gray', 'black' , 'orange' ,  'magenda' , 'yellow') as color, bankTransactions.accountID, openingBalance, transaction, description, closingBalance, tranDate,  
				bankAccounts.currency as currency, bankAccounts.accountID as accountNumber, banks.name as bank, banks.ID as bankID, bankTransactions.notes,
				currency.currency
				from bankTransactions 
				left join bankAccounts on bankAccounts.ID = bankTransactions.accountID
				left join banks ON bankAccounts.bankID = banks.bankID
				left join currency ON currency.ID = bankAccounts.currency
				where ($accountID=0 or bankTransactions.accountID = $accountID) and bankTransactions.asID=$asID order by bankTransactions.tranDate desc";
		*/
				return $this->executenonquery($sql,true);
	}

	

	
}
?>
