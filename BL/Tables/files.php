<?php
require_once dirname ( dirname ( dirname ( __FILE__ ) ) ) . "/DL/DAL.php";
use data\TableItem;
class files extends TableItem {
	// fields
	public $ID;
	public $fileName;
	public $tableName;
	public $tableRowID;
	public $date_;
	public $userID;
	public $description;

	


	// Counctructor
	function __construct($ID = NULL) {
		parent::__construct ();
		$this->ID = $ID;
		$this->settable ( "files" );
		$this->refresh ( $ID );
	}
	function __set($property, $value) {
		$this->$property = $value;
	}
	function __get($property) {
		if (isset ( $this->$property )) {
			return $this->$property;
		}
	}	

	function getFiles ($itemID,$table) {
		$sql = "select * from files where tableRowID=$itemID and ('$table'='' or tableName='$table') order by ID desc";
		return $this->executenonquery($sql,true);
	}

	function check_file_type($mime_type) {		
		switch ($mime_type) {
			//case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document': // .docx
			//case 'application/msword':
			//case 'application/doc':
			//case 'application/rtf':
			//case 'application/x-rtf':
			case 'text/richtext':
			case 'text/rtf':
			case 'application/vnd.ms-excel':
			case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':    // .xlsx
			case 'image/jpeg':
			case 'image/png':
			case 'application/pdf': {
				return true;
			}
			
			default: {
				return false;
			}
		}
	}
	
	
	

}
?>
