<?php
require_once dirname ( dirname ( dirname ( __FILE__ ) ) ) . "/DL/DAL.php";
use data\TableItem;
class products extends TableItem {
	// fields
	public $ID;
	public $product;
	public $description;
	public $price;
	public $accountBuy;
	public $taxRate;
	public $asID;
	public $isVendor;
	public $accountSell;
	public $currency;
	public $addBalance;
	

	// Counctructor
	function __construct($ID = NULL) {
		parent::__construct ();
		$this->ID = $ID;
		$this->settable ( "products" );
		$this->refresh ( $ID );
	}
	function __set($property, $value) {
		$this->$property = $value;
	}
	function __get($property) {
		if (isset ( $this->$property )) {
			return $this->$property;
		}
	}

	function getProducts ($productID,$asID,$isVendor,$invoiceID){
		$sql = "call getProducts($productID,$asID,$isVendor,$invoiceID)";
		return $this->executenonquery($sql,true);

		//$sql = "select * from customers where $customerID=0 or ID=$customerID order by customer";
		//return $this->executenonquery($sql,true);
	}

	
}
?>
