<?php
require_once dirname ( dirname ( dirname ( __FILE__ ) ) ) . "/DL/DAL.php";
use data\TableItem;
class employeeHours extends TableItem {
	// fields
	public $ID;
	public $employeeID;
	public $month;
	public $hrsWorked;
	public $asID;

	// Counctructor
	function __construct($ID = NULL) {
		parent::__construct ();
		$this->ID = $ID;
		$this->settable ( "employeeHours" );
		$this->refresh ( $ID );
	}
	function __set($property, $value) {
		$this->$property = $value;
	}
	function __get($property) {
		if (isset ( $this->$property )) {
			return $this->$property;
		}
	}

	function getMembers ($employeeID,$asID){
		$sql = "call getEmployeeHours($employeeID,$asID)";
		return $this->executenonquery($sql,true);

		//$sql = "select * from customers where $customerID=0 or ID=$customerID order by customer";
		//return $this->executenonquery($sql,true);
	}

	
}
?>
