<?php
require_once dirname ( dirname ( dirname ( __FILE__ ) ) ) . "/DL/DAL.php";
use data\TableItem;
class invoiceDetails extends TableItem {
	// fields
	public $ID;
	public $invoiceID;
	public $rowNumber;
	public $item;
	public $vatRate;
	public $vatAmount;
	public $amount;
	public $unit;
	public $totalAmount;
	public $productID;

	// Counctructor
	function __construct($ID = NULL) {
		parent::__construct ();
		$this->ID = $ID;
		$this->settable ( "invoiceDetails" );
		$this->refresh ( $ID );
	}
	function __set($property, $value) {
		$this->$property = $value;
	}
	function __get($property) {
		if (isset ( $this->$property )) {
			return $this->$property;
		}
	}

	function getInvoiceDetails ($invoiceID) {
		$sql = "call getInvoiceDetails($invoiceID)";
		return $this->executenonquery($sql);

	}

	public static function getorderDetailFirst ($orderID) {
		$intc = new self();
		$sql = "select * from invoiceDetails where invoiceID=$orderID order by ID desc limit 1";
		$intc->refreshprocedure($sql);
		return $intc;
	}

	
}
?>
