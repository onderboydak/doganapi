<?php
require_once dirname ( dirname ( dirname ( __FILE__ ) ) ) . "/DL/DAL.php";
use data\TableItem;
class bankBranches extends TableItem {
	// fields
	public $ID;
	public $bankID;
	public $branchID;
	public $cityID;
	public $branch;
	
	// Counctructor
	function __construct($ID = NULL) {
		parent::__construct ();
		$this->ID = $ID;
		$this->settable ( "bankBranches" );
		$this->refresh ( $ID );
	}
	function __set($property, $value) {
		$this->$property = $value;
	}
	function __get($property) {
		if (isset ( $this->$property )) {
			return $this->$property;
		}
	}	

	function getBankBranches ($bankID) {
		$sql ="select * from bankBranches where bankID=$bankID order by branch";
		return $this->executenonquery($sql,true);
	}
}
?>
