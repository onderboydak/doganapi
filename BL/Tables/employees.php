<?php
require_once dirname ( dirname ( dirname ( __FILE__ ) ) ) . "/DL/DAL.php";
use data\TableItem;
class employees extends TableItem {
	// fields
	public $ID;
	public $TCKN;
	public $name;
	public $lastName;
	public $email;
	public $homePhone;
	public $cellPhone;
	public $birthday;
	public $title;
	public $startDate;
	public $salary;
	public $isNet;
	public $school;
	public $department;
	public $socialSecDoc;
	public $SSKNo;
	public $idDoc;
	public $gradDoc;
	public $asID;
	public $userID;
	public $isMarried;
	public $numberOfKids;
	public $isSpouseWorking;
	public $payrollInfo;




	// Counctructor
	function __construct($ID = NULL) {
		parent::__construct ();
		$this->ID = $ID;
		$this->settable ( "employees" );
		$this->refresh ( $ID );
	}
	function __set($property, $value) {
		$this->$property = $value;
	}
	function __get($property) {
		if (isset ( $this->$property )) {
			return $this->$property;
		}
	}

	function getMembers ($employeeID,$asID){
		$sql = "call getMembers($employeeID,$asID)";
		return $this->executenonquery($sql,true);
	}

	
}
?>
