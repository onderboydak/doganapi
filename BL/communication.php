<?php
header ( "Content-type: text/html; charset=utf-8" );
use PHPMailer\PHPMailer\PHPMailer;

require_once dirname(dirname(__FILE__)).'/Library/PHPMailer-master/src/PHPMailer.php';
require_once dirname(dirname(__FILE__)).'/Library/PHPMailer-master/src/Exception.php';
require_once dirname(dirname(__FILE__)).'/Library/PHPMailer-master/src/SMTP.php';

class Mail {
    private $recipients = '';
    private $recipientsName = '';
    private $subject = '';
    private $body;
    
    function __construct($recipients, $recipientsName,  $subject,$body) {
        $this->recipients = $recipients;
        $this->recipientsName = $recipientsName;
        $this->subject = $subject;
        $this->body = $body;
    }
    
    function sendMail () {
        $mail = new PHPMailer ( true ); // the true param means it will throw
        $mail->isSMTP (); // telling the class to use SendMail transport
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = "tls";
        $mail->AddAddress ( str_replace ( 'I', 'i', $this->recipients ), $this->recipientsName );
        try {
           
            $mail->SetFrom ( mailServer::from, mailServer::fromName );
            $mail->Subject = $this->subject;
            $mail->MsgHTML ( $this->body );
            $mail->AddCC('onderboydak@eglencefabrikasi.com', 'Onder Boydak');

            $mail->SMTPAuth = true;
            $mail->Port = mailserver::port;
            $mail->Host = mailServer::host;
            $mail->Username = mailServer::username;
            $mail->Password = mailServer::password;
            $mail->CharSet = "UTF-8";
            $mail->Encoding = "base64";
            // $mail->SMTPDebug=2;
            $mail->isHTML ( true );
            if (! $mail->Send ()) {
                // echo var_dump($mail);
                // echo $mail->ErrorInfo;
                exit ();
            }
            ;
            return true;
        } catch ( phpmailerException $e ) {
            // echo $e->errorMessage();
            return false;
        } catch ( Exception $e ) {
            // echo $e->errorMessage();
            return false;
        }
        
    }
    
}



?>