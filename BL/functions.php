<?php
require_once dirname(__FILE__). "/Consts/consts.php";
require_once dirname(__FILE__). "/Enums/enums.php";
require_once dirname(dirname(__FILE__)) . "/BL/Tables/accountingSuppliers.php";

class functions {
    function getValueFromEnums ($enumClass,$val) {
        $ec = new ReflectionClass ( $enumClass );
        $arr = $ec->getConstants();
        return $arr[$val];
    }

    function isEInvoice($vknTckn,$asID) {
        $accountingSupplier = new accountingSuppliers($asID);

        $xml='<?xml version="1.0" encoding="UTF-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://tempuri.org/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"> ';
        $xml.=' 	<SOAP-ENV:Body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"> ';
        $xml.=' 		<IsEInvoiceUser xmlns="http://tempuri.org/"> ';
        $xml.=' 			<userInfo Username="'.$accountingSupplier->apiUser.'" Password="'.$accountingSupplier->apiPass.'"/>';
        $xml.='             <vknTckn>3260276056</vknTckn>';
        $xml.='             <alias></alias>';
        $xml.=' 		</IsEInvoiceUser> ';
        $xml.=' 	</SOAP-ENV:Body> ';
        $xml.=' </SOAP-ENV:Envelope> ';
        //echo $xml;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $accountingSupplier->apiUrl);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: text/xml;charset=UTF-8',
            'SOAPAction: http://tempuri.org/IBasicIntegration/IsEInvoiceUser'
        ));

        $data = curl_exec($ch);
        $data = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $data);
        $data = str_ireplace(['s:', ''], '', $data);

        $data = new SimpleXMLElement($data);
        return ($data->Body->IsEInvoiceUserResponse->IsEInvoiceUserResult["Value"]);
           
    }
}
?>