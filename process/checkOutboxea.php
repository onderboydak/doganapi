<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once dirname(dirname(__FILE__)) . "/BL/Tables/invoices.php";
require_once dirname(dirname(__FILE__)) . "/BL/Tables/invoiceDetails.php";
require_once dirname(dirname(__FILE__)) . "/BL/Tables/customers.php";
require_once dirname(dirname(__FILE__)) . "/BL/Tables/accountingSuppliers.php";
require_once dirname(dirname(__FILE__)) . "/BL/functions.php";

header("Content-type: text/html; charset=utf-8");
date_default_timezone_set('Europe/Istanbul');
setlocale(LC_ALL, "tr_TR");

$as = new accountingSuppliers();
$sql = "select * from accountingSuppliers order by supplier";
$result = $as->executenonquery($sql);
$pageSize=20;

while($row=mysqli_fetch_array($result)){
$pageIndex=0;
$totalPage=1;
while ($pageIndex<=($totalPage-1)) {
$xml='<?xml version="1.0" encoding="UTF-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://tempuri.org/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"> ';
$xml.=' 	<SOAP-ENV:Body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"> ';
$xml.=' 		<GetOutboxInvoiceList xmlns="http://tempuri.org/"> ';
$xml.=' 			<userInfo Username="'.$row["apiUser"].'" Password="'.$row["apiPass"].'"/>';
$xml.=' <query PageIndex="'.$pageIndex.'" PageSize="'.$pageSize.'">';
$xml.=' <ExecutionStartDate xsi:nil="true"/>';
$xml.=' <ExecutionEndDate xsi:nil="true"/>';
$xml.=' <CreateStartDate xsi:nil="true"/>';
$xml.=' <CreateEndDate xsi:nil="true"/>';
$xml.=' <Status xsi:nil="true"/>';
$xml.=' <Scenario>eArchive</Scenario>';
$xml.=' </query>';
$xml.=' </GetOutboxInvoiceList>';
$xml.=' </SOAP-ENV:Body>';
$xml.=' </SOAP-ENV:Envelope>';
//echo "<pre>".$xml."</pre>";

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $row["apiUrl"]);
curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: text/xml;charset=UTF-8',
    'SOAPAction: http://tempuri.org/IBasicIntegration/GetOutboxInvoiceList',
    'Username:'.$row["apiUser"], 'Password:'.$row["apiPass"]
));

        $data = curl_exec($ch);
        //echo var_dump(curl_error($ch));
        //echo var_dump(curl_getinfo($ch, CURLINFO_HTTP_CODE));
        //echo var_dump($data);

        $data = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $data);
        $data = str_ireplace(['s:', ''], '', $data);

        $data = new SimpleXMLElement($data);
        //echo var_dump($data);
        //echo var_dump($data->Body->GetOutboxInvoiceListResponse->GetOutboxInvoiceListResult->Value);
        $totalPage=$data->Body->GetOutboxInvoiceListResponse->GetOutboxInvoiceListResult->Value["TotalPages"];

        foreach ($data->Body->GetOutboxInvoiceListResponse->GetOutboxInvoiceListResult->Value->Items as $item) {
            if ($item->StatusCode!=10) { // Fatura ıptal degılse
               
                $invoice = invoices::getInvoiceFromMerchant($item->InvoiceId);
            if ($invoice->ID>0) {
                $invoice->status=$item->StatusCode;
                $invoice->tags=(strlen(ltrim($invoice->tags))>0) ? $invoice->tags.";eArchive" : "eArchive";
                //echo $item->DocumentCurrencyCode;
                
                switch ($item->DocumentCurrencyCode) {
                    case 'TRY':
                                $invoice->invoiceCurrency=1;
                                break;
                    case 'USD':
                                $invoice->invoiceCurrency=2;
                                break;
                    case 'EUR':
                                $invoice->invoiceCurrency=3;
                                break;
                    case 'GBP':
                                $invoice->invoiceCurrency=4;
                                break;
                } 
                if ($invoice->pdfPath=="") {
                    // download pdf

                    $as = new accountingSuppliers($invoice->asID);

                    $xmlp='<?xml version="1.0" encoding="UTF-8"?>';
                    $xmlp.='<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://tempuri.org/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">';
                    $xmlp.='<SOAP-ENV:Body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
                    $xmlp.='   <GetOutboxInvoicePdf xmlns="http://tempuri.org/">';
                    $xmlp.='   <userInfo Username="'.$as->apiUser.'" Password="'.$as->apiPass.'"/>';
                    $xmlp.='    <invoiceId>'.$item->DocumentId.'</invoiceId>';
                    $xmlp.='   </GetOutboxInvoicePdf>';
                    $xmlp.='</SOAP-ENV:Body>';
                    $xmlp.='</SOAP-ENV:Envelope>';
                    //echo "<pre>".$xmlp."</pre>";
    
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $as->apiUrl);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlp);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: text/xml;charset=UTF-8',
                        'SOAPAction: http://tempuri.org/IBasicIntegration/GetOutboxInvoicePdf',
                        'Username:'.$as->apiUser, 'Password:'.$as->apiPass)
                    );
    
                    $datap = curl_exec($ch);
                    //echo var_dump(curl_error($ch));
                    //echo var_dump(curl_getinfo($ch, CURLINFO_HTTP_CODE));
                   
    
                    $datap = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $datap);
                    $datap = str_ireplace(['s:', ''], '', $datap);
    
                    $datap = new SimpleXMLElement($datap);  
                    //echo var_dump($datap);
                    echo var_dump($datap->Body->GetOutboxInvoicePdfResponse->GetOutboxInvoicePdfResult->Value->Data);
    
                    //base64_to_jpeg($datap->Body->GetInboxInvoicePdfResponse->GetInboxInvoicePdfResult->Value->Data,"../Uploads/".$item->InvoiceId.".pdf");
                    $base64String = "data:application/pdf;base64,".$datap->Body->GetOutboxInvoicePdfResponse->GetOutboxInvoicePdfResult->Value->Data;
                    $uploadFile= dirname ( dirname ( __FILE__ ) ) ."/Uploads/invoices/".$invoice->asID."/".$item->InvoiceId.".pdf";
                    file_put_contents($uploadFile, file_get_contents($base64String));
                    
                    
                    $invoice->pdfPath=$item->InvoiceId.".pdf";
                } 
            } else {
                   
                    $customer = customers::getCustomerFromtckVKNNo($item->TargetTcknVkn,$row["ID"]);
                    $customerID=$customer->ID;
                    if (!$customerID>0) {
                        $customer->vatNumber=$item->TargetTcknVkn;    
                        $customer->customer=$item->TargetTitle;
                        $customer->customerShortName=$item->TargetTitle;
                        $customer->customerType=1;
                        $customer->asID=$row["ID"];
                        $customer->isVendor=1;
                        $customer->dueDateOptions=3;
                        $customerID=$customer->save();  
                    } else {
                        $customer->isVendor=3;
                        $customerID=$customer->save();  
                    }   
                    $invoice->dueDateOptions=$customer->dueDateOptions;
                    $invoice->asID=$row["ID"];
                    $invoice->invoiceNumber=$item->InvoiceId;
                    $invoice->invoiceDate=date("Y-m-d",strtotime($item->CreateDateUtc));
        
                    switch ($item->TypeCode) {
                        case "0":
                            $invoice->invoiceClass=1;
                           break;
                        default : 
                           $invoice->invoiceClass=$item->TypeCode;
                    }
        
                    $invoice->invoiceType=1;
                    $invoice->merchantInvoiceID=$item->DocumentId;
                    $invoice->customerID=$customerID;
        
                    switch ($item->DocumentCurrencyCode) {
                        case 'TRY':
                                    $invoice->invoiceCurrency=1;
                                    break;
                        case 'USD':
                                    $invoice->invoiceCurrency=2;
                                    break;
                        case 'EUR':
                                    $invoice->invoiceCurrency=3;
                                    break;
                        case 'GBP':
                                    $invoice->invoiceCurrency=4;
                                    break;
                    }   
        
                    $invoice->status=$item->StatusCode;
                    $invoice->tags="eArchive";
                    $invoiceID=$invoice->save();
                    
                    if ($invoiceID>0) {
                        // download pdf
                        $xmlp='<?xml version="1.0" encoding="UTF-8"?>';
                        $xmlp.='<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://tempuri.org/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">';
                        $xmlp.='<SOAP-ENV:Body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
                        $xmlp.='   <GetOutboxInvoicePdf xmlns="http://tempuri.org/">';
                        $xmlp.='   <userInfo Username="'.$row["apiUser"].'" Password="'.$row["apiPass"].'"/>';
                        $xmlp.='    <invoiceId>'.$item->DocumentId.'</invoiceId>';
                        $xmlp.='   </GetOutboxInvoicePdf>';
                        $xmlp.='</SOAP-ENV:Body>';
                        $xmlp.='</SOAP-ENV:Envelope>';
                        //echo "<pre>".$xmlp."</pre>";
        
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $row["apiUrl"]);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlp);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                            'Content-Type: text/xml;charset=UTF-8',
                            'SOAPAction: http://tempuri.org/IBasicIntegration/GetOutboxInvoicePdf',
                            'Username:'.$row["apiUser"], 'Password:'.$row["apiPass"])
                        );
        
                        $datap = curl_exec($ch);
                        //echo var_dump(curl_error($ch));
                        //echo var_dump(curl_getinfo($ch, CURLINFO_HTTP_CODE));
                       
        
                        $datap = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $datap);
                        $datap = str_ireplace(['s:', ''], '', $datap);
        
                        $datap = new SimpleXMLElement($datap);  
                        //echo var_dump($datap);
                        //echo var_dump($datap->Body->GetInboxInvoicePdfResponse->GetInboxInvoicePdfResult->Value->Data);
        
                        //base64_to_jpeg($datap->Body->GetInboxInvoicePdfResponse->GetInboxInvoicePdfResult->Value->Data,"../Uploads/".$item->InvoiceId.".pdf");
                        $base64String = "data:application/pdf;base64,".$datap->Body->GetOutboxInvoicePdfResponse->GetOutboxInvoicePdfResult->Value->Data;
                        $uploadFile= dirname ( dirname ( __FILE__ ) ) ."/Uploads/invoices/".$row["ID"]."/".$item->InvoiceId.".pdf";
                        file_put_contents($uploadFile, file_get_contents($base64String));
                        
                        
                        $invoice=new invoices($invoiceID);
                        $invoice->pdfPath=$item->InvoiceId.".pdf";
                        $invoice->save(); 
        
        
                        $sql = "delete from invoiceDetails where invoiceID=".$invoiceID;
                        $invoice->executenonquery($sql,null,true);   

                        // Invoice Detail
                        $xmld='<?xml version="1.0" encoding="UTF-8"?>';
                        $xmld.='<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://tempuri.org/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">';
                        $xmld.='<SOAP-ENV:Body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
                        $xmld.='   <GetOutboxInvoice xmlns="http://tempuri.org/">';
                        $xmld.='   <userInfo Username="'.$row["apiUser"].'" Password="'.$row["apiPass"].'"/>';
                        $xmld.='    <invoiceId>'.$item->DocumentId.'</invoiceId>';
                        $xmld.='   </GetOutboxInvoice>';
                        $xmld.='</SOAP-ENV:Body>';
                        $xmld.='</SOAP-ENV:Envelope>';
                        //echo "<pre>".$xmlp."</pre>";

                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $row["apiUrl"]);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $xmld);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                            'Content-Type: text/xml;charset=UTF-8',
                            'SOAPAction: http://tempuri.org/IBasicIntegration/GetOutboxInvoice',
                            'Username:'.$row["apiUser"], 'Password:'.$row["apiPass"])
                        );

                        $datad = curl_exec($ch);
                        //echo var_dump(curl_error($ch));
                        //echo var_dump(curl_getinfo($ch, CURLINFO_HTTP_CODE));
                        //echo var_dump($data);
                
                        $datad = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $datad);
                        $datad = str_ireplace(['s:', ''], '', $datad);
                
                        $datad = new SimpleXMLElement($datad);
                        //echo var_dump($data->Body->GetInboxInvoiceListResponse->GetInboxInvoiceListResult);
                        $rowNumber=0;
                        foreach ($datad->Body->GetOutboxInvoiceResponse->GetOutboxInvoiceResult->Value->Invoice->InvoiceLine as $il) {
                            $rowNumber++;    

                            //if (floatval($il->Vat1)>0){
                            // KDV %1       
                            $ind = new invoiceDetails ();
                            $ind->invoiceID=$invoiceID;
                            $ind->rowNumber=$rowNumber;
                            $ind->item=ltrim($il->Item->Name);   
                            $ind->vatRate=$il->TaxTotal->TaxSubtotal->Percent;
                            $ind->vatAmount=str_replace(",",".",floatval($il->TaxTotal->TaxSubtotal->TaxAmount));
                            $ind->amount=str_replace(",",".",floatval($il->Price->PriceAmount));
                            $ind->unit=$il->InvoicedQuantity;
                            $ind->totalAmount=str_replace(",",".",floatval($il->LineExtensionAmount));
                            $ind->save();
                        }
                /*
                        if (floatval($item->Vat1)>0){
                         // KDV %1       
                         $ind = new invoiceDetails ();
                         $ind->invoiceID=$invoiceID;
                         $ind->rowNumber=1;
                         $ind->item="See Attached PDF for details ";   
                         $ind->vatRate=1;
                         $ind->vatAmount=str_replace(",",".",floatval($item->Vat1));
                         $ind->amount=str_replace(",",".",floatval($item->Vat1TaxableAmount)-floatval($item->Vat1));
                         $ind->unit=1;
                         $ind->totalAmount=str_replace(",",".",floatval($item->Vat1TaxableAmount)-floatval($item->Vat1));
                         $ind->save();
                        }    
        
                        if (floatval($item->Vat8)>0){
                            // KDV %8      
                            $ind = new invoiceDetails ();
                            $ind->invoiceID=$invoiceID;
                            $ind->rowNumber=1;
                            $ind->item="See Attached PDF for details ";   
                            $ind->vatRate=8;
                            $ind->vatAmount=str_replace(",",".",floatval($item->Vat8));
                            $ind->amount=str_replace(",",".",floatval($item->Vat8TaxableAmount)-floatval($item->Vat8));
                            $ind->unit=1;
                            $ind->totalAmount=str_replace(",",".",floatval($item->Vat8TaxableAmount)-floatval($item->Vat8));
                            $ind->save();
                        }    
        
                        if (floatval($item->Vat18)>0){
                            // KDV %18       
                            $ind = new invoiceDetails ();
                            $ind->invoiceID=$invoiceID;
                            $ind->rowNumber=1;
                            $ind->item="See Attached PDF for details ";   
                            $ind->vatRate=18;
                            $ind->vatAmount=str_replace(",",".",floatval($item->Vat18));
                            $ind->amount=str_replace(",",".",floatval($item->Vat18TaxableAmount)-floatval($item->Vat18));
                            $ind->unit=1;
                            $ind->totalAmount=str_replace(",",".",floatval($item->Vat18TaxableAmount)-floatval($item->Vat18));
                            $ind->save();
                        }    
                         
                        if (floatval($item->Vat0TaxableAmount)>0){
                            // KDV %0     
                            $ind = new invoiceDetails ();
                            $ind->invoiceID=$invoiceID;
                            $ind->rowNumber=1;
                            $ind->item="See Attached PDF for details ";   
                            $ind->vatRate=0;
                            $ind->vatAmount=0;
                            $ind->amount=str_replace(",",".",floatval($item->Vat0TaxableAmount));
                            $ind->unit=1;
                            $ind->totalAmount=str_replace(",",".",floatval($item->Vat0TaxableAmount));
                            $ind->save();
                        }    
                        */
                    }
               
            }
            echo $item->InvoiceId;
            $invoice->save();
        }
        }
        $pageIndex++;
    }
    }
    echo "Completed";

    function base64_to_jpeg($base64_string, $output_file) {
        // open the output file for writing
        $ifp = fopen( $output_file, 'wb' ); 
    
        // split the string on commas
        // $data[ 0 ] == "data:image/png;base64"
        // $data[ 1 ] == <actual base64 string>
        $data = explode( ',', $base64_string );
    
        // we could add validation here with ensuring count( $data ) > 1
        fwrite( $ifp, base64_decode( $data[ 1 ] ) );
    
        // clean up the file resource
        fclose( $ifp ); 
    
        return $output_file; 
    }

?>
