<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once dirname(dirname(__FILE__)) . "/BL/Tables/invoices.php";

header("Content-type: text/html; charset=utf-8");
date_default_timezone_set('Europe/Istanbul');
setlocale(LC_ALL, "tr_TR");

$sql = "SELECT invoices.ID,concat(chartOfAccounts.account,'.',chartOfAccounts.subAccount) as account FROM invoices 
inner join rules on rules.customerID=invoices.customerID and rules.asID=invoices.asID and rules.invoiceType=invoices.invoiceType and 
						rules.currency=invoices.invoiceCurrency
inner join chartOfAccounts on chartOfAccounts.ID=rules.accountID
where invoices.status=1000 and invoices.invoiceType=2 and ifnull(invoices.account,'')=''";
$inv = new invoices();
$result = $inv->executenonquery($sql);
while($row=mysqli_fetch_array($result)) {
    $invoice = new invoices($row["ID"]);
    $invoice->account=$row["account"];
    $invoice->isProforma=0;
    $invoice->save();
}

?>
