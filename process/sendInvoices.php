<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

header("Content-type: text/html; charset=utf-8");
date_default_timezone_set('Europe/Istanbul');
setlocale(LC_ALL, "tr_TR");

require_once dirname(dirname(__FILE__)) . "/BL/Tables/invoices.php";
require_once dirname(dirname(__FILE__)) . "/BL/Tables/invoiceDetails.php";
require_once dirname(dirname(__FILE__)) . "/BL/Tables/customers.php";
require_once dirname(dirname(__FILE__)) . "/BL/Tables/accountingSuppliers.php";
require_once dirname(dirname(__FILE__)) . "/BL/functions.php";

$invoice = new invoices();
$func = new functions();
$result = $invoice->getInvoicesWithStatus(3,0);

/* 1 = EF 2= EFCodes*/


while ( $row = mysqli_fetch_array ( $result ) ) {
    $in = new invoices($row["ID"]);
    $customer = new customers($in->customerID);
    $accountingSupplier = new accountingSuppliers($row["asID"]);
    $totalResult = $invoice->getTotals($row["ID"]);
    list ($vatTotal,$totalAmount)=mysqli_fetch_array($totalResult);
    $odf = invoiceDetails::getorderDetailFirst($row["ID"]);
    
    $customerName = split_name($customer->customer);


    $eInvoiceResult = $func->isEInvoice($customer->tckVKNNo,$row["asID"]);

        $xml='<?xml version="1.0" encoding="UTF-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://tempuri.org/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"> ';
        $xml.=' 	<SOAP-ENV:Body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"> ';
        $xml.=' 		<SendInvoice xmlns="http://tempuri.org/"> ';
        $xml.=' 			<userInfo Username="'.$accountingSupplier->apiUser.'" Password="'.$accountingSupplier->apiPass.'"/>';
        $xml.=' 			<invoices> ';
        $xml.=' 				<InvoiceInfo LocalDocumentId="'.$in->ID.'"> ';
        $xml.=' 					<Invoice> ';
        $xml.=' 						<ProfileID xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.(($in->invoiceClass==2) ? 'TEMELFATURA' : 'TICARIFATURA').'</ProfileID> ';
        $xml.=' 						<ID xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.$in->invoiceNumber.'</ID> ';
        $xml.=' 						<CopyIndicator xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">false</CopyIndicator> ';
        $xml.=' 						<IssueDate xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.date("Y-m-d",strtotime($in->invoiceDate)).'</IssueDate> ';
        $xml.=' 						<IssueTime xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.date("H:i:s+03:00",strtotime($in->invoiceDate)).'</IssueTime> ';
        $xml.=' 						<InvoiceTypeCode xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.$func->getValueFromEnums("invoiceType","invoiceType".$in->invoiceClass).'</InvoiceTypeCode> ';
        $xml.=' 						<Note xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"></Note> ';
        $xml.=' 						<Note xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.$in->notes.'</Note> ';
        $xml.=' 						<Note xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"></Note> ';
        $xml.=' 						<Note xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.$accountingSupplier->notes.'</Note> ';
        $xml.=' 						<DocumentCurrencyCode xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.$func->getValueFromEnums("currency","currency".$in->invoiceCurrency).'</DocumentCurrencyCode> ';
        $xml.=' 						<PricingCurrencyCode xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.$func->getValueFromEnums("currency","currency".$in->invoiceCurrency).'</PricingCurrencyCode> ';
        $xml.=' 						<PaymentCurrencyCode xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.$func->getValueFromEnums("currency","currency".$in->invoiceCurrency).'</PaymentCurrencyCode> ';
        $xml.=' 						<LineCountNumeric xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">2</LineCountNumeric> '; // todo
        $xml.=' 						<OrderReference xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"> ';
        $xml.=' 							<ID xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.$in->invoiceNumber.'</ID> '; //bu code order/tx numarası
        $xml.=' 							<IssueDate xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.date("Y-m-d",strtotime($in->invoiceDate)).'</IssueDate> ';
        $xml.=' 						</OrderReference> ';
        /*$xml.=' 						<DespatchDocumentReference xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"> ';
        $xml.=' 							<ID xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">IRS000000001</ID> ';
        $xml.=' 							<IssueDate xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">2017-06-08</IssueDate> ';
        $xml.=' 							<DocumentType xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">Irsaliye</DocumentType> ';
        $xml.=' 						</DespatchDocumentReference> ';
        */
        
        //Burada sıgnature eksik - zorunlu alan

        $xml.=' 						<AccountingSupplierParty xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"> ';
        $xml.=' 							<Party> ';
        $xml.=' 								<PartyIdentification> ';
        $xml.=' 									<ID schemeID="VKN" xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.str_pad($accountingSupplier->tckVKNNo, 10, "0", STR_PAD_LEFT).'</ID> ';
        $xml.=' 								</PartyIdentification> ';
        $xml.=' 								<PartyIdentification> ';
        $xml.=' 									<ID schemeID="MERSISNO" xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"></ID> ';
        $xml.=' 								</PartyIdentification> ';
        $xml.=' 								<PartyIdentification> ';
        $xml.=' 									<ID schemeID="TICARETSICILNO" xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.$accountingSupplier->commerceAuthorityNo.'</ID> ';
        $xml.=' 								</PartyIdentification> ';
        $xml.=' 								<PartyName> ';
        $xml.=' 									<Name xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.$accountingSupplier->supplier.'</Name> ';
        $xml.=' 								</PartyName> ';
        $xml.=' 								<PostalAddress> ';
        $xml.=' 									<Room xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"></Room> ';
        $xml.=' 									<StreetName xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.$accountingSupplier->address.'</StreetName> ';
        $xml.=' 									<BuildingNumber xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.$accountingSupplier->buildingNumber.'</BuildingNumber> ';
        $xml.=' 									<CitySubdivisionName xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.$accountingSupplier->district.'</CitySubdivisionName> ';
        $xml.=' 									<CityName xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.$accountingSupplier->city.'</CityName> ';
        $xml.=' 									<Country> ';
        $xml.=' 										<Name xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.$accountingSupplier->country.'</Name> ';
        $xml.=' 									</Country> ';
        $xml.=' 								</PostalAddress> ';
        $xml.=' 								<PartyTaxScheme> ';
        $xml.=' 									<TaxScheme> ';
        $xml.=' 										<Name xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.$accountingSupplier->vatAuthority.'</Name> ';
        $xml.=' 									</TaxScheme> ';
        $xml.=' 								</PartyTaxScheme> ';
        $xml.=' 							</Party> ';
        $xml.=' 						</AccountingSupplierParty> ';
        $xml.=' 						<AccountingCustomerParty xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"> ';
        $xml.=' 							<Party> ';
        $xml.=' 								<WebsiteURI xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"></WebsiteURI> ';
        $xml.=' 								<PartyIdentification> ';
        $xml.=' 									<ID schemeID="'.(($customer->customerType==1 && strlen($customer->vatNumber)<=10) ? 'VKN' : 'TCKN').'" xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.(($customer->customerType==1) ? str_pad($customer->vatNumber, 10, "0", STR_PAD_LEFT) : str_pad($customer->vatNumber, 11, "0", STR_PAD_LEFT)).'</ID> ';
        $xml.=' 								</PartyIdentification> ';
        $xml.=' 								<PartyName> ';
        $xml.=' 									<Name xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.$customer->customer.'</Name> ';
        $xml.=' 								</PartyName> ';
        $xml.=' 								<PostalAddress> ';
        $xml.=' 									<Room xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"></Room> ';
        $xml.=' 									<StreetName xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.$customer->address.'</StreetName> ';
        $xml.=' 									<BuildingNumber xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.$customer->buildingNumber.'</BuildingNumber> ';
        $xml.=' 									<CitySubdivisionName xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.$customer->district.'</CitySubdivisionName> ';
        $xml.=' 									<CityName xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.$customer->city.'</CityName> ';
        $xml.=' 									<Country> ';
        $xml.=' 										<Name xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.$customer->country.'</Name> ';
        $xml.=' 									</Country> ';
        $xml.=' 								</PostalAddress> ';
        $xml.=' 								<PartyTaxScheme> ';
        $xml.=' 									<TaxScheme> ';
        $xml.=' 										<Name xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.$customer->vatAuthority.'</Name> ';
        $xml.=' 									</TaxScheme> ';
        $xml.=' 								</PartyTaxScheme> ';
        $xml.=' 								<Contact> ';
        $xml.=' 									<Telephone xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.$customer->phone.'</Telephone> ';
        $xml.=' 									<Telefax xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"></Telefax> ';
        $xml.=' 									<ElectronicMail xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.$customer->email.'</ElectronicMail> ';
        $xml.=' 								</Contact> ';
        if ($customer->customerType==2 || strlen($customer->vatNumber)>=11) {
            $xml.='                                  <Person>';
            $xml.='                                         <FirstName xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.($customerName["first_name"].' '.$customerName["middle_name"]).'</FirstName>';
            $xml.='                                         <FamilyName xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.$customerName["last_name"].'</FamilyName>';
            $xml.='                                   </Person>';
        }
        $xml.=' 							</Party> ';
        $xml.=' 						</AccountingCustomerParty> ';   
        $xml.=' 						<TaxTotal xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"> ';
        $xml.=' 							<TaxAmount currencyID="'.$func->getValueFromEnums("currency","currency".$in->invoiceCurrency).'" xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.str_replace(",",".",round($vatTotal,2)).'</TaxAmount> ';
        $xml.=' 							<TaxSubtotal> ';
        $xml.=' 								<TaxAmount currencyID="'.$func->getValueFromEnums("currency","currency".$in->invoiceCurrency).'" xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.str_replace(",",".",round($vatTotal,2)).'</TaxAmount> '; 
        $xml.=' 								<Percent xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.$odf->vatRate.'</Percent> '; 
        $xml.=' 								<TaxCategory> ';
        if ($in->invoiceClass==4) {
            if ($in->invoiceCurrency!=1) {
                $xml.=' 										<TaxExemptionReasonCode xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">302</TaxExemptionReasonCode> ';
                $xml.=' 										<TaxExemptionReason xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">302 11 /1 - a Hizmet ihracatı</TaxExemptionReason> ';
            } else {
                $xml.=' 										<TaxExemptionReasonCode xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">350</TaxExemptionReasonCode> ';
                $xml.=' 										<TaxExemptionReason xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">302 Diğerleri</TaxExemptionReason> ';
            }   
        }
        $xml.=' 									<TaxScheme> ';
        $xml.=' 										<Name xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">KDV</Name> ';
        $xml.=' 										<TaxTypeCode xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">0015</TaxTypeCode> '; 
        $xml.=' 									</TaxScheme> ';
        $xml.=' 								</TaxCategory> ';
        $xml.=' 							</TaxSubtotal> ';
        $xml.=' 						</TaxTotal> ';
        $xml.=' 						<LegalMonetaryTotal xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"> ';
        $xml.=' 							<LineExtensionAmount currencyID="'.$func->getValueFromEnums("currency","currency".$in->invoiceCurrency).'" xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.str_replace(",",".",round($totalAmount,2)).'</LineExtensionAmount> '; // todo
        $xml.=' 							<TaxExclusiveAmount currencyID="'.$func->getValueFromEnums("currency","currency".$in->invoiceCurrency).'" xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.str_replace(",",".",round($totalAmount,2)).'</TaxExclusiveAmount> '; //todo
        $xml.=' 							<TaxInclusiveAmount currencyID="'.$func->getValueFromEnums("currency","currency".$in->invoiceCurrency).'" xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.str_replace(",",".",round(($totalAmount+$vatTotal),2)).'</TaxInclusiveAmount> '; // todo
        //$xml.=' 							<AllowanceTotalAmount currencyID="'.$func->getValueFromEnums("currency","currency".$in->invoiceCurrency).'" xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.$vatTotal.'</AllowanceTotalAmount> '; //todo
        $xml.=' 							<PayableAmount currencyID="'.$func->getValueFromEnums("currency","currency".$in->invoiceCurrency).'" xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.str_replace(",",".",round(($totalAmount+$vatTotal),2)).'</PayableAmount> '; //todo
        $xml.=' 						</LegalMonetaryTotal> ';

        $invoiceD = new invoiceDetails();
        $idResult = $invoiceD->getInvoiceDetails($in->ID);
        while ($rowd = mysqli_fetch_array($idResult)) {
            $xml.=' 						<InvoiceLine xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"> ';
            $xml.=' 							<ID xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.$rowd["ID"].'</ID> ';
            $xml.=' 							<Note xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.$rowd["item"].'</Note> ';
            $xml.=' 							<InvoicedQuantity unitCode="NIU" xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.round($rowd["unit"],0).'</InvoicedQuantity> ';
            $xml.=' 							<LineExtensionAmount currencyID="'.$func->getValueFromEnums("currency","currency".$in->invoiceCurrency).'" xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.str_replace(",",".",$rowd["totalAmount"]).'</LineExtensionAmount> ';
            //$xml.=' 							<AllowanceCharge> ';
            //$xml.=' 								<ChargeIndicator xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">false</ChargeIndicator> ';
            //$xml.=' 								<Amount currencyID="'.$func->getValueFromEnums("currency","currency".$in->invoiceCurrency).'" xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.$rowd["amount"].'</Amount> ';
            //$xml.=' 							</AllowanceCharge> ';
            $xml.=' 							<TaxTotal> ';
            $xml.=' 								<TaxAmount currencyID="'.$func->getValueFromEnums("currency","currency".$in->invoiceCurrency).'" xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.str_replace(",",".",$rowd["vatAmount"]).'</TaxAmount> ';
            $xml.=' 								<TaxSubtotal> ';
            $xml.=' 									<TaxAmount currencyID="'.$func->getValueFromEnums("currency","currency".$in->invoiceCurrency).'" xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.str_replace(",",".",$rowd["vatAmount"]).'</TaxAmount> ';
            $xml.=' 									<Percent xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.$rowd ["vatRate"].'</Percent> ';
            $xml.=' 									<TaxCategory> ';
            if ($in->invoiceClass==4) {
                if ($in->invoiceCurrency!=1) {
                    $xml.=' 										<TaxExemptionReasonCode xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">302</TaxExemptionReasonCode> ';
                    $xml.=' 										<TaxExemptionReason xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">302 11 /1 - a Hizmet ihracatı</TaxExemptionReason> ';
                } else {
                    $xml.=' 										<TaxExemptionReasonCode xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">350</TaxExemptionReasonCode> ';
                    $xml.=' 										<TaxExemptionReason xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">350 Diğerleri</TaxExemptionReason> ';
                }
            }
            $xml.=' 										<TaxScheme> ';
            $xml.=' 											<Name xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">KDV</Name> ';
            $xml.=' 											<TaxTypeCode xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">0015</TaxTypeCode> '; // todo
            $xml.=' 										</TaxScheme> ';
            $xml.=' 									</TaxCategory> ';
            $xml.=' 								</TaxSubtotal> ';
            $xml.=' 							</TaxTotal> ';
            $xml.=' 							<Item> ';
            $xml.=' 								<Description xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.$rowd["item"].'</Description> ';
            $xml.=' 								<Name xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.$rowd["item"].'</Name> ';
            $xml.=' 								<BrandName xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">EF</BrandName> ';
            $xml.=' 								<ModelName xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"></ModelName> ';
            $xml.=' 								<BuyersItemIdentification> ';
            $xml.=' 									<ID xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.$customer->ID.'</ID> ';
            $xml.=' 								</BuyersItemIdentification> ';
            $xml.=' 								<SellersItemIdentification> ';
            $xml.=' 									<ID xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"></ID> ';
            $xml.=' 								</SellersItemIdentification> ';
            $xml.=' 								<ManufacturersItemIdentification> ';
            $xml.=' 									<ID xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"></ID> ';
            $xml.=' 								</ManufacturersItemIdentification> ';
            $xml.=' 							</Item> ';
            $xml.=' 							<Price> ';
            $xml.=' 								<PriceAmount currencyID="'.$func->getValueFromEnums("currency","currency".$in->invoiceCurrency).'" xmlns="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">'.str_replace(",",".",round($rowd["amount"],2)).'</PriceAmount> ';
            $xml.=' 							</Price> ';
            $xml.=' 						</InvoiceLine> ';
        }
        $xml.=' 					</Invoice> ';
        $xml.=' 					<TargetCustomer VknTckn="'.str_pad($customer->vatNumber, 10, "0", STR_PAD_LEFT).'" Alias="'.(($customer->pbox!="") ? "urn:mail:".$customer->pbox : "defaultpk").'" Title="'.$customer->customer.'"/> ';
        $xml.=' 					<EArchiveInvoiceInfo DeliveryType="Electronic"/> ';
        $xml.=' 					<Scenario>Automated</Scenario> ';
        $xml.=' 				</InvoiceInfo> ';
        $xml.=' 			</invoices> ';
        $xml.=' 		</SendInvoice> ';
        $xml.=' 	</SOAP-ENV:Body> ';
        $xml.=' </SOAP-ENV:Envelope> ';
        echo $xml;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $accountingSupplier->apiUrl);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: text/xml;charset=UTF-8',
            'SOAPAction: http://tempuri.org/IBasicIntegration/SendInvoice'
        ));

        $data = curl_exec($ch);
        //echo var_dump(curl_error($ch));
        //echo var_dump(curl_getinfo($ch, CURLINFO_HTTP_CODE));
        echo var_dump($data);

        $data = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $data);
        $data = str_ireplace(['s:', ''], '', $data);

        $data = new SimpleXMLElement($data);
       // echo var_dump($data->Body->SendInvoiceResponse->SendInvoiceResult);
       
        if (isset($data->Body->SendInvoiceResponse->SendInvoiceResult)) {
            if ($data->Body->SendInvoiceResponse->SendInvoiceResult["IsSucceded"]=="true") {
                $in->merchantInvoiceID=$data->Body->SendInvoiceResponse->SendInvoiceResult->Value["Id"];
                $in->merchantInvoiceNumber=$data->Body->SendInvoiceResponse->SendInvoiceResult->Value["Number"];
                $in->merchantDate=date("Y-m-d H:i");
                $in->status=4;
                $in->statusDesc="";
                $in->save();
            } else {
                $in->statusDesc=$data->Body->SendInvoiceResponse->SendInvoiceResult["Message"];
                $in->status=5;
                $in->save();
            }
        }


        echo $in->ID ." has created!<br>";

}


function split_name($name) {
    $parts = array();

    while ( strlen( trim($name)) > 0 ) {
        $name = trim($name);
        $string = preg_replace('#.*\s([\w-]*)$#', '$1', $name);
        $parts[] = $string;
        $name = trim( preg_replace('#'.$string.'#', '', $name ) );
    }

    if (empty($parts)) {
        return false;
    }

    $parts = array_reverse($parts);
    $name = array();
    $name['first_name'] = $parts[0];
    $name['middle_name'] = (isset($parts[2])) ? $parts[1] : '';
    $name['last_name'] = (isset($parts[2])) ? $parts[2] : ( isset($parts[1]) ? $parts[1] : '');

    return $name;
}



?>
