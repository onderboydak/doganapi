<?php
/*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
*/
header("Content-type: text/html; charset=utf-8");
date_default_timezone_set('Europe/Istanbul');
setlocale(LC_ALL, "tr_TR");

use phpseclib\Net\SFTP;
use phpseclib\Common\Functions;
use Kingsquare\Parser\Banking\Mt940;

set_include_path ( dirname ( dirname ( __FILE__ ) ) . '/Library/phpseclib/' );

require_once dirname ( dirname ( __FILE__ ) ) . '/Library/phpseclib/Net/SFTP.php';

require_once dirname(dirname(__FILE__)) . "/BL/Consts/consts.php";

require_once dirname(dirname(__FILE__)) . "/BL/Tables/bankAccounts.php";
require_once dirname(dirname(__FILE__)) . "/BL/Tables/bankTransactions.php";

// composer autoloader
require dirname(__DIR__) . '/Library/vendor/autoload.php';
require dirname(__DIR__) . '/Library/vendor/kingsquare/php-mt940/src/Parser/Banking/Mt940/Engine/gb.php';

// instantiate the actual parser
// and parse them from a given file, this could be any file or a posted string

// Burada ilk önce dosya inecek
// Ardından bu dosyaları process edip DB yazmak lazım
// Ama gereken SFTP baglantısı ve MT940 process kutuphanelerını buraya koydum

//DB, de var olan bank accountlar için sunucuya gidip dosyaları alacak ve işleyecek

$sql = "select bankAccounts.accountID,bankAccess.ftp_server,bankAccess.ftp_port,bankAccess.ftp_user,bankAccess.ftp_pass,bankAccounts.asID from bankAccounts
inner join bankAccess on bankAccounts.bankID=bankAccess.bankID and bankAccounts.asID=bankAccess.asID
where bankAccounts.asID in (1,2)";

$btt = new bankTransactions();
$result = $btt->executenonquery($sql);
while(list($accountID, $ftp_server,$ftp_port,$ftp_username,$ftp_userpass,$asID)=mysqli_fetch_array($result)) {


/*
        $ftp_server = garantibank::ftp_server;
        $ftp_port = garantibank::ftp_port;
		$ftp_username = garantibank::ftp_username;
		$ftp_userpass = garantibank::ftp_userpass;
	*/	
		try {
			 $sftp = new Net_SFTP( $ftp_server,  $ftp_port); 
			 echo $ftp_username;
			if ( $sftp->login ( $ftp_username,$ftp_userpass )) { 
				
            // remote sunucuda dosyalar MT940/Inbox altında
            // 7886737_459_01242019_9.txt isimlerle _9 kısım o gün dosyaların numarası 1.....9
            // ilk numara hesap no, ikinci sube no, sonra tarih ve dosya numarası var


            $path = "/MT940/Inbox/";
			$list = $sftp->nlist($path);
			foreach ($list as $remoteFile) {
				if (strpos($remoteFile, $accountID)!==false) {
					echo $remoteFile."<br>";
					$localFile = dirname ( dirname ( __FILE__ ) ) . "/Uploads/statements/".$asID."/".$remoteFile;
					if ($sftp->file_exists($path.$remoteFile))
					{
						$success = $sftp->get(($path.$remoteFile),$localFile);
					}
					if ($success) {

						$parser = new Mt940();
						$engine = new Mt940\Engine\GB();
						$parsedStatements = $parser->parse(file_get_contents($localFile),$engine);

						$tc= sizeof($parsedStatements[0]->getTransactions());
						$tcr =0;
						foreach ($parsedStatements[0]->getTransactions() as $item) {
							
							//$ba = $parsedStatements[0]->getAccount();

							$ba = bankAccounts::getBankAccountsFromAccountID($accountID);

							$bt = new bankTransactions();
							$bt->accountID=$ba->ID;
							$bt->description=(str_replace("Þ","S",str_replace("Ý","I",$item->getDescription())));
							$bt->asID=$asID;
							$bt->openingBalance=str_replace(",",".",$parsedStatements[0]->getStartPrice());
							$bt->transaction=str_replace(",",".",($item->getDebitCredit()=="D") ? (-1*$item->getPrice()) : $item->getPrice()) ;
							$bt->closingBalance=str_replace(",",".",$parsedStatements[0]->getEndPrice());
							$bt->tranDate=date('Y-m-d', strtotime(gmdate("Y-m-d H:i:s",$item->getValueTimestamp()). ' + 1 days'));
							$btID = $bt->save();
						
							if ($btID>0) {
								$tcr+=1;
							}
							
						}

						if ($tcr==$tc) {
							//delet file from ftp 
							echo $localFile." has deleted<br>";
							$sftp->delete(($path.$remoteFile));
						}
					}

				}	

			}
		}
		} catch ( Exception $e ) {
			echo $e->getMessage ();
		}
}
?>