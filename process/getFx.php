<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once dirname(dirname ( __FILE__ )) . '/BL/Tables/invoices.php';
require_once dirname(dirname ( __FILE__ )) . '/Library/ganon_rev78/ganon.php';

// tcmb
$open = simplexml_load_file('http://www.tcmb.gov.tr/kurlar/today.xml');
$query = "delete from fx where refreshDate=date(now());";
$curr = new invoices();
$curr->executenonquery($query,NULL,true);


foreach ($open->Currency as $cur) {
    if ($cur[0]["CurrencyCode"] =='EUR' || $cur[0]["CurrencyCode"]=='USD' || $cur[0]["CurrencyCode"]=='GBP') {
        //echo $cur[0]["CurrencyCode"]."-".$cur->ForexBuying."<br>";
        $query = "insert into fx (name,`to`,rate,refreshdate) values ('".$cur[0]["CurrencyCode"]."','TRY',".$cur->ForexBuying.",date(now()));";
        $curr->executenonquery($query,NULL,true);
    }
    
}

$html = file_get_dom('http://www.xe.com/currencytables/?from=USD');
foreach($html('#historicalRateTbl tr') as $row) {
    $cn =0;
    $fxFrom="";
    $fxRate=0;
    foreach($row('td') as $col) {
        $cn+=1;
        if ($cn==1) {
            $fxFrom = $col->getPlainText();
        }
        if ($cn==4) {
            $fxRate = $col->getPlainText();
        }
       // echo $col->getPlainText(), ' [', $col, "] <br>\n";
    }
    if ($fxFrom=='EUR' || $fxFrom=='GBP' || $fxFrom=='TRY') {
        $query = "insert into fx (name,`to`,rate,refreshdate) values ('".$fxFrom."','USD',".$fxRate.",date(now()));";
        $curr->executenonquery($query,NULL,true);
    }
}

$html = file_get_dom('http://www.xe.com/currencytables/?from=EUR');
foreach($html('#historicalRateTbl tr') as $row) {
    $cn =0;
    $fxFrom="";
    $fxRate=0;
    foreach($row('td') as $col) {
        $cn+=1;
        if ($cn==1) {
            $fxFrom = $col->getPlainText();
        }
        if ($cn==4) {
            $fxRate = $col->getPlainText();
        }
       // echo $col->getPlainText(), ' [', $col, "] <br>\n";
    }
    if ($fxFrom=='USD' || $fxFrom=='GBP' || $fxFrom=='TRY') {
        $query = "insert into fx (name,`to`,rate,refreshdate) values ('".$fxFrom."','EUR',".$fxRate.",date(now()));";
        $curr->executenonquery($query,NULL,true);
    }
}

$html = file_get_dom('http://www.xe.com/currencytables/?from=GBP');
foreach($html('#historicalRateTbl tr') as $row) {
    $cn =0;
    $fxFrom="";
    $fxRate=0;
    foreach($row('td') as $col) {
        $cn+=1;
        if ($cn==1) {
            $fxFrom = $col->getPlainText();
        }
        if ($cn==4) {
            $fxRate = $col->getPlainText();
        }
       // echo $col->getPlainText(), ' [', $col, "] <br>\n";
    }
    if ($fxFrom=='USD' || $fxFrom=='EUR' || $fxFrom=='TRY') {
        $query = "insert into fx (name,`to`,rate,refreshdate) values ('".$fxFrom."','GBP',".$fxRate.",date(now()));";
        $curr->executenonquery($query,NULL,true);
    }
}


?>
