<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once dirname(dirname(__FILE__)) . "/BL/Tables/invoices.php";
require_once dirname(dirname(__FILE__)) . "/BL/Tables/invoiceDetails.php";
require_once dirname(dirname(__FILE__)) . "/BL/Tables/customers.php";
require_once dirname(dirname(__FILE__)) . "/BL/Tables/accountingSuppliers.php";
require_once dirname(dirname(__FILE__)) . "/BL/functions.php";

header("Content-type: text/html; charset=utf-8");
date_default_timezone_set('Europe/Istanbul');
setlocale(LC_ALL, "tr_TR");

$as = new accountingSuppliers();
$sql = "select * from accountingSuppliers order by supplier";
$result = $as->executenonquery($sql);
$pageSize=20;

while($row=mysqli_fetch_array($result)){
$pageIndex=0;
$totalPage=1;
while ($pageIndex<=($totalPage-1)) {
$xml='<?xml version="1.0" encoding="UTF-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://tempuri.org/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"> ';
$xml.=' 	<SOAP-ENV:Body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"> ';
$xml.=' 		<GetInboxInvoiceList xmlns="http://tempuri.org/"> ';
$xml.=' 			<userInfo Username="'.$row["apiUser"].'" Password="'.$row["apiPass"].'"/>';
$xml.=' <query PageIndex="'.$pageIndex.'" PageSize="'.$pageSize.'" OnlyNewestInvoices="false">';
$xml.=' <ExecutionStartDate xsi:nil="true"/>';
$xml.=' <ExecutionEndDate xsi:nil="true"/>';
$xml.=' <CreateStartDate xsi:nil="true"/>';
$xml.=' <CreateEndDate xsi:nil="true"/>';
$xml.=' <Status xsi:nil="true"/>';
$xml.=' </query>';
$xml.=' </GetInboxInvoiceList>';
$xml.=' </SOAP-ENV:Body>';
$xml.=' </SOAP-ENV:Envelope>';
//echo "<pre>".$xml."</pre>";

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $row["apiUrl"]);
curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: text/xml;charset=UTF-8',
    'SOAPAction: http://tempuri.org/IBasicIntegration/GetInboxInvoiceList',
    'Username:'.$row["apiUser"], 'Password:'.$row["apiPass"]
));

        $data = curl_exec($ch);
        //echo var_dump(curl_error($ch));
        //echo var_dump(curl_getinfo($ch, CURLINFO_HTTP_CODE));
        //echo var_dump($data);
        //exit();
        $data = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $data);
        $data = str_ireplace(['s:', ''], '', $data);
        //echo var_dump($data);
        if (!$data) {
            exit();
        }
        $data = new SimpleXMLElement($data);
       
        $totalPage=$data->Body->GetInboxInvoiceListResponse->GetInboxInvoiceListResult->Value["TotalPages"];

        foreach ($data->Body->GetInboxInvoiceListResponse->GetInboxInvoiceListResult->Value->Items as $item) {
            //echo var_dump($item);
            $invoice = invoices::getInvoiceFromMerchant($item->InvoiceId);
            if ($invoice->status!=1000) {
            $customer = customers::getCustomerFromtckVKNNo($item->TargetTcknVkn,$row["ID"]);
            $customerID=$customer->ID;
            if (!$customerID>0) {
                $customer->vatNumber=$item->TargetTcknVkn;    
                $customer->customer=$item->TargetTitle;
                $customer->customerShortName=$item->TargetTitle;
                $customer->customerType=1;
                $customer->asID=$row["ID"];
                $customer->isVendor=2;
                $customer->dueDateOptions=3;
                $customerID=$customer->save();  
            }  else {
                $customer->isVendor=3;
                $customerID=$customer->save();  
            }  
            $invoice->asID=$row["ID"];
            $invoice->invoiceNumber=$item->InvoiceId;
            $invoice->invoiceDate=date("Y-m-d",strtotime($item->ExecutionDate));
            $invoice->dueDate=date('Y-m-d', strtotime(date('Y-m-d', strtotime("next friday"))." +7 days"));
            $invoice->dueDateOptions=$customer->dueDateOptions;

            switch ($item->TypeCode) {
                case "0":
                    $invoice->invoiceClass=1;
                   break;
                default : 
                    $invoice->invoiceClass=$item->TypeCode;
            }
           
            $invoice->invoiceType=2;
            $invoice->merchantInvoiceID=$item->DocumentId;
            $invoice->customerID=$customerID;
            $invoice->tags="eInvoice";

            switch ($item->DocumentCurrencyCode) {
                case 'TRY':
                            $invoice->invoiceCurrency=1;
                            break;
                case 'USD':
                            $invoice->invoiceCurrency=2;
                            break;
                case 'EUR':
                            $invoice->invoiceCurrency=3;
                            break;
                case 'GBP':
                            $invoice->invoiceCurrency=4;
                            break;
            }   

            $invoice->status=1000;
            $invoiceID=$invoice->save();
            
            if ($invoiceID>0) {
                // download pdf
                $xmlp='<?xml version="1.0" encoding="UTF-8"?>';
                $xmlp.='<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://tempuri.org/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">';
                $xmlp.='<SOAP-ENV:Body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
                $xmlp.='   <GetInboxInvoicePdf xmlns="http://tempuri.org/">';
                $xmlp.='   <userInfo Username="'.$row["apiUser"].'" Password="'.$row["apiPass"].'"/>';
                $xmlp.='    <invoiceId>'.$item->DocumentId.'</invoiceId>';
                $xmlp.='   </GetInboxInvoicePdf>';
                $xmlp.='</SOAP-ENV:Body>';
                $xmlp.='</SOAP-ENV:Envelope>';
                //echo "<pre>".$xmlp."</pre>";

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $row["apiUrl"]);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlp);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: text/xml;charset=UTF-8',
                    'SOAPAction: http://tempuri.org/IBasicIntegration/GetInboxInvoicePdf',
                    'Username:'.$row["apiUser"], 'Password:'.$row["apiPass"])
                );

                $datap = curl_exec($ch);
                //echo var_dump(curl_error($ch));
                //echo var_dump(curl_getinfo($ch, CURLINFO_HTTP_CODE));
               

                $datap = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $datap);
                $datap = str_ireplace(['s:', ''], '', $datap);

                $datap = new SimpleXMLElement($datap);  
                //echo var_dump($datap);
                //echo var_dump($datap->Body->GetInboxInvoicePdfResponse->GetInboxInvoicePdfResult->Value->Data);

                //base64_to_jpeg($datap->Body->GetInboxInvoicePdfResponse->GetInboxInvoicePdfResult->Value->Data,"../Uploads/".$item->InvoiceId.".pdf");
                $base64String = "data:application/pdf;base64,".$datap->Body->GetInboxInvoicePdfResponse->GetInboxInvoicePdfResult->Value->Data;
                
                $uploadFile= dirname ( dirname ( __FILE__ ) ) ."/Uploads/invoices/".$row["ID"]."/".$item->InvoiceId.".pdf";
                file_put_contents($uploadFile, file_get_contents($base64String));
                
                $invoice=new invoices($invoiceID);
                $invoice->pdfPath=$item->InvoiceId.".pdf";
                $invoice->save(); 
                echo  $item->InvoiceId;

                // delete invoice details
                $sql = "delete from invoiceDetails where invoiceID=".$invoiceID;
                $invoice->executenonquery($sql,null,true);   

                // Invoice Detail
                $xmld='<?xml version="1.0" encoding="UTF-8"?>';
                $xmld.='<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://tempuri.org/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">';
                $xmld.='<SOAP-ENV:Body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
                $xmld.='   <GetInboxInvoice xmlns="http://tempuri.org/">';
                $xmld.='   <userInfo Username="'.$row["apiUser"].'" Password="'.$row["apiPass"].'"/>';
                $xmld.='    <invoiceId>'.$item->DocumentId.'</invoiceId>';
                $xmld.='   </GetInboxInvoice>';
                $xmld.='</SOAP-ENV:Body>';
                $xmld.='</SOAP-ENV:Envelope>';
                //echo "<pre>".$xmlp."</pre>";

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $row["apiUrl"]);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $xmld);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: text/xml;charset=UTF-8',
                    'SOAPAction: http://tempuri.org/IBasicIntegration/GetInboxInvoice',
                    'Username:'.$row["apiUser"], 'Password:'.$row["apiPass"])
                );

                $datad = curl_exec($ch);
                //echo var_dump(curl_error($ch));
                //echo var_dump(curl_getinfo($ch, CURLINFO_HTTP_CODE));
                //echo var_dump($data);
        
                $datad = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $datad);
                $datad = str_ireplace(['s:', ''], '', $datad);
        
                $datad = new SimpleXMLElement($datad);
                //echo var_dump($data->Body->GetInboxInvoiceListResponse->GetInboxInvoiceListResult);
                $rowNumber=0;
                foreach ($datad->Body->GetInboxInvoiceResponse->GetInboxInvoiceResult->Value->Invoice->InvoiceLine as $il) {
                $rowNumber++;    

                //if (floatval($il->Vat1)>0){
                 // KDV %1  
                 if ($datad->Body->GetInboxInvoiceResponse->GetInboxInvoiceResult->Value->TargetCustomer["VknTckn"]=="9250353261" && $rowNumber>1) {
                     break;
                 }     
                 $ind = new invoiceDetails ();
                 $ind->invoiceID=$invoiceID;
                 $ind->rowNumber=$rowNumber;
                 $ind->item=ltrim($il->Item->Name);   
                 $ind->vatRate=isset($il->TaxTotal->TaxSubtotal->Percent) ? $il->TaxTotal->TaxSubtotal->Percent : 18;
                 $ind->vatAmount=isset($il->TaxTotal->TaxSubtotal->TaxAmount) ? str_replace(",",".",floatval($il->TaxTotal->TaxSubtotal->TaxAmount)) : 0;
                 $ind->amount=str_replace(",",".",floatval($il->Price->PriceAmount));
                 $ind->unit=$il->InvoicedQuantity;
                 $ind->totalAmount=isset($il->LineExtensionAmount) ? str_replace(",",".",floatval($il->LineExtensionAmount)) : str_replace(",",".",floatval($il->Price->PriceAmount));
                 if ($datad->Body->GetInboxInvoiceResponse->GetInboxInvoiceResult->Value->TargetCustomer["VknTckn"]=="9250353261") {
                    $ind->totalAmount=str_replace(",",".",floatval($datad->Body->GetInboxInvoiceResponse->GetInboxInvoiceResult->Value->Invoice->LegalMonetaryTotal->PayableAmount));
                 };
                 $ind->save();
                
                
            }
            }
        }
    }
        $pageIndex++;
    }
    }

    echo "Completed";

    function base64_to_jpeg($base64_string, $output_file) {
        // open the output file for writing
        $ifp = fopen( $output_file, 'wb' ); 
    
        // split the string on commas
        // $data[ 0 ] == "data:image/png;base64"
        // $data[ 1 ] == <actual base64 string>
        $data = explode( ',', $base64_string );
    
        // we could add validation here with ensuring count( $data ) > 1
        fwrite( $ifp, base64_decode( $data[ 1 ] ) );
    
        // clean up the file resource
        fclose( $ifp ); 
    
        return $output_file; 
    }
?>