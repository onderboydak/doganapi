<?php 

// e fatura

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

header("Content-type: text/html; charset=utf-8");
date_default_timezone_set('Europe/Istanbul');
setlocale(LC_ALL, "tr_TR");

require_once dirname(dirname(__FILE__)) . "/BL/Tables/invoices.php";
require_once dirname(dirname(__FILE__)) . "/BL/Tables/invoiceDetails.php";
require_once dirname(dirname(__FILE__)) . "/BL/Tables/customers.php";
require_once dirname(dirname(__FILE__)) . "/BL/Tables/accountingSuppliers.php";
require_once dirname(dirname(__FILE__)) . "/BL/functions.php";

$invoice = new invoices();
$func = new functions();
$result = $invoice->getInvoicesWithStatus(3,0);

$apiUrl = "https://efaturatest.izibiz.com.tr/EFaturaOIB?wsdl";
$apiUrlArchieve = "https://efaturatest.izibiz.com.tr/EIArchiveWS/EFaturaArchive?wsdl";
$apiUser = "izibiz-test2";
$apiPass = "izi321";

$senderVKN="4840847211";
$senderUrn="urn:mail:defaultgb@izibiz.com.tr";

$customerVKN="4840847211";
$customerUrn="urn:mail:defaultpk@izibiz.com.tr";

while ( $row = mysqli_fetch_array ( $result ) ) {
    
    $in = new invoices($row["ID"]);
    $customer = new customers($in->customerID);
    $accountingSupplier = new accountingSuppliers($row["asID"]);
    $totalResult = $invoice->getTotals($row["ID"]);
    list ($vatTotal,$totalAmount)=mysqli_fetch_array($totalResult);
    $odf = invoiceDetails::getorderDetailFirst($row["ID"]);
    
    $customerName = split_name($customer->customer);


    $eInvoiceResult = $func->isEInvoice($customer->tckVKNNo,$row["asID"]);    

        $xml='<?xml version="1.0" encoding="UTF-8"?>';
        $xml.='<Invoice xmlns="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2" xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2" xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2" xmlns:ds="http://www.w3.org/2000/09/xmldsig#" xmlns:ext="urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2" xmlns:ns8="urn:oasis:names:specification:ubl:schema:xsd:ApplicationResponse-2" xmlns:xades="http://uri.etsi.org/01903/v1.3.2#" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2 UBL-Invoice-2.1.xsd">';
        $xml.=' <ext:UBLExtensions>';
        $xml.='     <ext:UBLExtension>';
        $xml.='         <ext:ExtensionContent>';                
        $xml.='         </ext:ExtensionContent>';
        $xml.='      </ext:UBLExtension>';
        $xml.=' </ext:UBLExtensions>';
        $xml.='                 <cbc:UBLVersionID>2.1</cbc:UBLVersionID>'; 
        $xml.='                <cbc:CustomizationID>TR1.2</cbc:CustomizationID>'; 
        $xml.=' 						<cbc:ProfileID>'.(($in->invoiceClass==2) ? 'TEMELFATURA' : 'TICARIFATURA').'</cbc:ProfileID> ';
        $xml.=' 						<cbc:ID>'.$in->invoiceNumber.'</cbc:ID> ';
        $xml.=' 						<cbc:CopyIndicator >false</cbc:CopyIndicator> ';
        $xml.=' 						<cbc:UUID>'.gen_uuid().'</cbc:UUID>';
        $xml.=' 						<cbc:IssueDate>'.date("Y-m-d",strtotime($in->invoiceDate)).'</cbc:IssueDate> ';
        $xml.=' 						<cbc:IssueTime>'.date("H:i:s",strtotime($in->invoiceDate)).'</cbc:IssueTime> ';
        $xml.=' 						<cbc:InvoiceTypeCode>'.$func->getValueFromEnums("invoiceType","invoiceType".$in->invoiceClass).'</cbc:InvoiceTypeCode> ';
        $xml.=' 						<cbc:Note>'.$in->notes.'</cbc:Note> ';
        $xml.=' 						<cbc:DocumentCurrencyCode  >'.$func->getValueFromEnums("currency","currency".$in->invoiceCurrency).'</cbc:DocumentCurrencyCode> ';
        $xml.=' 						<cbc:LineCountNumeric>1</cbc:LineCountNumeric>';
        $xml.=' 						<cac:AdditionalDocumentReference>';
        $xml.=' 					    	<cbc:ID>'.gen_uuid().'</cbc:ID>';
        $xml.=' 						    <cbc:IssueDate>'.date("Y-m-d",strtotime($in->invoiceDate)).'</cbc:IssueDate>';
        $xml.=' 						    <cbc:DocumentType>XSLT</cbc:DocumentType>';
        $xml.=' 						<cac:Attachment>';
        $xml.=' 						<cbc:EmbeddedDocumentBinaryObject characterSetCode="UTF-8" encodingCode="Base64" filename="izibiz.xslt" mimeCode="application/xml">';
        $data = file_get_contents(dirname(dirname(__FILE__)) ."/templates/izibiz.xslt");
        $xml.=base64_encode($data);
        $xml.='</cbc:EmbeddedDocumentBinaryObject>';
        $xml.=' 						</cac:Attachment>';
        $xml.=' 						</cac:AdditionalDocumentReference>';
        $xml.=' 						<cac:Signature>';
		$xml.=' 						<cbc:ID schemeID="VKN_TCKN">'.$senderVKN.'</cbc:ID>';
		$xml.=' 						<cac:SignatoryParty>';
		$xml.=' 							<cac:PartyIdentification>';
		$xml.=' 								<cbc:ID schemeID="VKN">'.$senderVKN.'</cbc:ID>';
		$xml.=' 							</cac:PartyIdentification>';
		$xml.=' 							<cac:PostalAddress>';
		$xml.=' 								<cbc:StreetName>'.$accountingSupplier->address.'</cbc:StreetName>';
		$xml.=' 								<cbc:BuildingName></cbc:BuildingName>';
		$xml.=' 								<cbc:BuildingNumber>'.$accountingSupplier->buildingNumber.'</cbc:BuildingNumber>';
		$xml.=' 								<cbc:CitySubdivisionName>'.$accountingSupplier->district.'</cbc:CitySubdivisionName>';
		$xml.=' 								<cbc:CityName>'.$accountingSupplier->city.'</cbc:CityName>';
		$xml.=' 								<cbc:PostalZone></cbc:PostalZone>';
		$xml.=' 								<cbc:Region>'.$accountingSupplier->city.'</cbc:Region>';
		$xml.=' 								<cac:Country>';
		$xml.=' 									<cbc:Name>'.$accountingSupplier->country.'</cbc:Name>';
		$xml.=' 								</cac:Country>';
		$xml.=' 							</cac:PostalAddress>';
		$xml.=' 						</cac:SignatoryParty>';
		$xml.=' 						<cac:DigitalSignatureAttachment>';
		$xml.=' 							<cac:ExternalReference>';
		$xml.=' 								<cbc:URI>#Signature_DMY20181231120707</cbc:URI>';
		$xml.=' 							</cac:ExternalReference>';
		$xml.=' 						</cac:DigitalSignatureAttachment>';
        $xml.=' 						</cac:Signature>';  
        $xml.=' 						<cac:AccountingSupplierParty> ';
        $xml.=' 							<cac:Party>';
        $xml.=' 								<cac:PartyIdentification>';
        $xml.=' 									<cbc:ID schemeID="VKN">'.str_pad($senderVKN, 10, "0", STR_PAD_LEFT).'</cbc:ID> ';
        $xml.=' 								</cac:PartyIdentification>';
        $xml.=' 								<cac:PartyIdentification>';
        $xml.=' 									<cbc:ID schemeID="MERSISNO"></cbc:ID> ';
        $xml.=' 								</cac:PartyIdentification>';
        $xml.=' 								<cac:PartyIdentification> ';
        $xml.=' 									<cbc:ID schemeID="TICARETSICILNO"  >'.$accountingSupplier->commerceAuthorityNo.'</cbc:ID> ';
        $xml.=' 								</cac:PartyIdentification> ';
        $xml.=' 								<cac:PartyName> ';
        $xml.=' 									<cbc:Name>'.$accountingSupplier->supplier.'</cbc:Name> ';
        $xml.=' 								</cac:PartyName> ';
        $xml.=' 								<cac:PostalAddress> ';
        $xml.=' 									<cbc:Room ></cbc:Room> ';
        $xml.=' 									<cbc:StreetName>'.$accountingSupplier->address.'</cbc:StreetName> ';
        $xml.=' 									<cbc:BuildingNumber>'.$accountingSupplier->buildingNumber.'</cbc:BuildingNumber> ';
        $xml.=' 									<cbc:CitySubdivisionName>'.$accountingSupplier->district.'</cbc:CitySubdivisionName> ';
        $xml.=' 									<cbc:CityName>'.$accountingSupplier->city.'</cbc:CityName> ';
        $xml.=' 									<cac:Country> ';
        $xml.=' 										<cbc:Name>'.$accountingSupplier->country.'</cbc:Name> ';
        $xml.=' 									</cac:Country> ';
        $xml.=' 								</cac:PostalAddress> ';
        $xml.=' 								<cac:PartyTaxScheme> ';
        $xml.=' 									<cac:TaxScheme> ';
        $xml.=' 										<cbc:Name>'.$accountingSupplier->vatAuthority.'</cbc:Name> ';
        $xml.=' 									</cac:TaxScheme> ';
        $xml.=' 								</cac:PartyTaxScheme> ';
        $xml.=' 							</cac:Party> ';
        $xml.=' 						</cac:AccountingSupplierParty> ';
        $xml.=' 						<cac:AccountingCustomerParty> ';
        $xml.=' 							<cac:Party> ';
        $xml.=' 								<cac:PartyIdentification> ';
        $xml.=' 									<cbc:ID schemeID="'.(($customer->customerType==1 && strlen($customerVKN)<=10) ? 'VKN' : 'TCKN').'"  >'.(($customer->customerType==1) ? str_pad($customerVKN, 10, "0", STR_PAD_LEFT) : str_pad($customerVKN, 11, "0", STR_PAD_LEFT)).'</cbc:ID>';
        $xml.=' 								</cac:PartyIdentification> ';
        $xml.=' 								<cac:PartyName>';
        $xml.=' 									<cbc:Name>'.$customer->customer.'</cbc:Name> ';
        $xml.=' 								</cac:PartyName> ';
        $xml.=' 								<cac:PostalAddress> ';
        $xml.=' 									<cbc:Room></cbc:Room> ';
        $xml.=' 									<cbc:StreetName>'.$customer->address.'</cbc:StreetName> ';
        $xml.=' 									<cbc:BuildingNumber>'.$customer->buildingNumber.'</cbc:BuildingNumber> ';
        $xml.=' 									<cbc:CitySubdivisionName>'.$customer->district.'</cbc:CitySubdivisionName> ';
        $xml.=' 									<cbc:CityName>'.$customer->city.'</cbc:CityName> ';
        $xml.=' 									<cac:Country> ';
        $xml.=' 										<cbc:Name>'.$customer->country.'</cbc:Name> ';
        $xml.=' 									</cac:Country> ';
        $xml.=' 								</cac:PostalAddress> ';
        $xml.=' 								<cac:PartyTaxScheme> ';
        $xml.=' 									<cac:TaxScheme> ';
        $xml.=' 										<cbc:Name>'.$customer->vatAuthority.'</cbc:Name> ';
        $xml.=' 									</cac:TaxScheme> ';
        $xml.=' 								</cac:PartyTaxScheme> ';
        $xml.=' 								<cac:Contact> ';
        $xml.=' 									<cbc:Telephone>'.$customer->phone.'</cbc:Telephone> ';
        $xml.=' 									<cbc:Telefax></cbc:Telefax> ';
        $xml.=' 									<cbc:ElectronicMail>'.$customer->email.'</cbc:ElectronicMail> ';
        $xml.=' 								</cac:Contact> ';
        if ($customer->customerType==2 || strlen($customerVKN)>=11) {
            $xml.='                                  <cac:Person>';
            $xml.='                                         <cbc:FirstName>'.($customerName["first_name"].' '.$customerName["middle_name"]).'</cbc:FirstName>';
            $xml.='                                         <cbc:FamilyName>'.$customerName["last_name"].'</cbc:FamilyName>';
            $xml.='                                   </cac:Person>';
        }
        $xml.=' 							</cac:Party> ';
        $xml.=' 						</cac:AccountingCustomerParty> ';      
        $xml.=' 						<cac:TaxTotal> ';
        $xml.=' 							<cbc:TaxAmount currencyID="'.$func->getValueFromEnums("currency","currency".$in->invoiceCurrency).'"  >'.str_replace(",",".",round($vatTotal,2)).'</cbc:TaxAmount> ';
        $xml.=' 							<cac:TaxSubtotal> ';
        $xml.=' 								<cbc:TaxableAmount currencyID="'.$func->getValueFromEnums("currency","currency".$in->invoiceCurrency).'"  >'.str_replace(",",".",round($totalAmount,2)).'</cbc:TaxableAmount> '; 
        $xml.=' 								<cbc:TaxAmount currencyID="'.$func->getValueFromEnums("currency","currency".$in->invoiceCurrency).'"  >'.str_replace(",",".",round($vatTotal,2)).'</cbc:TaxAmount> '; 
        $xml.=' 								<cbc:CalculationSequenceNumeric>1</cbc:CalculationSequenceNumeric> '; 
        $xml.=' 								<cbc:Percent>'.$odf->vatRate.'</cbc:Percent> '; 
        $xml.=' 								<cac:TaxCategory> ';
        if ($in->invoiceClass==4) {
            if ($in->invoiceCurrency!=1) {
                $xml.=' 										<cbc:TaxExemptionReasonCode>302</cbc:TaxExemptionReasonCode> ';
                $xml.=' 										<cbc:TaxExemptionReason>302 11 /1 - a Hizmet ihracatı</cbc:TaxExemptionReason> ';
            } else {
                $xml.=' 										<cbc:TaxExemptionReasonCode>350</cbc:TaxExemptionReasonCode> ';
                $xml.=' 										<cbc:TaxExemptionReason>302 Diğerleri</cbc:TaxExemptionReason> ';
            }   
        }
        $xml.=' 									<cac:TaxScheme> ';
        $xml.=' 										<cbc:Name>KDV</cbc:Name> ';
        $xml.=' 										<cbc:TaxTypeCode>0015</cbc:TaxTypeCode> '; 
        $xml.=' 									</cac:TaxScheme> ';
        $xml.=' 								</cac:TaxCategory> ';
        $xml.=' 							</cac:TaxSubtotal> ';
        $xml.=' 						</cac:TaxTotal> ';
        $xml.=' 						<cac:LegalMonetaryTotal> ';
        $xml.=' 							<cbc:LineExtensionAmount currencyID="'.$func->getValueFromEnums("currency","currency".$in->invoiceCurrency).'"  >'.str_replace(",",".",round($totalAmount,2)).'</cbc:LineExtensionAmount> '; // todo
        $xml.=' 							<cbc:TaxExclusiveAmount currencyID="'.$func->getValueFromEnums("currency","currency".$in->invoiceCurrency).'"  >'.str_replace(",",".",round($totalAmount,2)).'</cbc:TaxExclusiveAmount> '; //todo
        $xml.=' 							<cbc:TaxInclusiveAmount currencyID="'.$func->getValueFromEnums("currency","currency".$in->invoiceCurrency).'"  >'.str_replace(",",".",round(($totalAmount+$vatTotal),2)).'</cbc:TaxInclusiveAmount> '; // todo
        $xml.=' 							<cbc:AllowanceTotalAmount currencyID="'.$func->getValueFromEnums("currency","currency".$in->invoiceCurrency).'"  >'.$vatTotal.'</cbc:AllowanceTotalAmount> '; //todo
        $xml.=' 							<cbc:ChargeTotalAmount currencyID="'.$func->getValueFromEnums("currency","currency".$in->invoiceCurrency).'"  >0</cbc:ChargeTotalAmount> '; //todo
        $xml.=' 							<cbc:PayableAmount currencyID="'.$func->getValueFromEnums("currency","currency".$in->invoiceCurrency).'"  >'.str_replace(",",".",round(($totalAmount+$vatTotal),2)).'</cbc:PayableAmount> '; //todo
        $xml.=' 						</cac:LegalMonetaryTotal> ';

        $invoiceD = new invoiceDetails();
        $idResult = $invoiceD->getInvoiceDetails($in->ID);
        while ($rowd = mysqli_fetch_array($idResult)) {
            $xml.=' 						<cac:InvoiceLine> ';
            $xml.=' 							<cbc:ID>'.$rowd["ID"].'</cbc:ID> ';
            $xml.=' 							<cbc:Note>'.$rowd["item"].'</cbc:Note> ';
            $xml.=' 							<cbc:InvoicedQuantity unitCode="NIU">'.round($rowd["unit"],0).'</cbc:InvoicedQuantity> ';
            $xml.=' 							<cbc:LineExtensionAmount currencyID="'.$func->getValueFromEnums("currency","currency".$in->invoiceCurrency).'"  >'.str_replace(",",".",$rowd["totalAmount"]).'</cbc:LineExtensionAmount> ';
            $xml.=' 							<cac:TaxTotal> ';
            $xml.=' 								<cbc:TaxAmount currencyID="'.$func->getValueFromEnums("currency","currency".$in->invoiceCurrency).'"  >'.str_replace(",",".",$rowd["vatAmount"]).'</cbc:TaxAmount> ';
            $xml.=' 								<cac:TaxSubtotal> ';
            $xml.=' 									<cbc:TaxableAmount currencyID="'.$func->getValueFromEnums("currency","currency".$in->invoiceCurrency).'"  >'.str_replace(",",".",$rowd["totalAmount"]).'</cbc:TaxableAmount> ';
            $xml.=' 									<cbc:TaxAmount currencyID="'.$func->getValueFromEnums("currency","currency".$in->invoiceCurrency).'"  >'.str_replace(",",".",$rowd["vatAmount"]).'</cbc:TaxAmount> ';
            $xml.=' 									<cbc:CalculationSequenceNumeric>1</cbc:CalculationSequenceNumeric>';
            $xml.=' 									<cbc:Percent>'.$rowd ["vatRate"].'</cbc:Percent> ';
            $xml.=' 									<cac:TaxCategory> ';
            if ($in->invoiceClass==4) {
                if ($in->invoiceCurrency!=1) {
                    $xml.=' 										<cbc:TaxExemptionReasonCode  >302</cbc:TaxExemptionReasonCode> ';
                    $xml.=' 										<cbc:TaxExemptionReason  >302 11 /1 - a Hizmet ihracatı</cbc:TaxExemptionReason> ';
                } else {
                    $xml.=' 										<cbc:TaxExemptionReasonCode  >350</cbc:TaxExemptionReasonCode> ';
                    $xml.=' 										<cbc:TaxExemptionReason  >350 Diğerleri</cbc:TaxExemptionReason> ';
                }
            }
            $xml.=' 										<cac:TaxScheme> ';
            $xml.=' 											<cbc:Name  >KDV</cbc:Name> ';
            $xml.=' 											<cbc:TaxTypeCode  >0015</cbc:TaxTypeCode> '; // todo
            $xml.=' 										</cac:TaxScheme> ';
            $xml.=' 									</cac:TaxCategory> ';
            $xml.=' 								</cac:TaxSubtotal> ';
            $xml.=' 							</cac:TaxTotal> ';
            $xml.=' 							<cac:Item> ';
            $xml.=' 								<cbc:Description>'.$rowd["item"].'</cbc:Description> ';
            $xml.=' 								<cbc:Name>'.$rowd["item"].'</cbc:Name> ';
            $xml.=' 								<cbc:BrandName>EF</cbc:BrandName> ';
            $xml.=' 								<cbc:ModelName></cbc:ModelName> ';
            $xml.=' 								<cac:BuyersItemIdentification> ';
            $xml.=' 									<cbc:ID>'.$customer->ID.'</cbc:ID> ';
            $xml.=' 								</cac:BuyersItemIdentification> ';
            $xml.=' 								<cac:SellersItemIdentification> ';
            $xml.=' 									<cbc:ID></cbc:ID> ';
            $xml.=' 								</cac:SellersItemIdentification> ';
            $xml.=' 								<cac:ManufacturersItemIdentification> ';
            $xml.=' 									<cbc:ID></cbc:ID> ';
            $xml.=' 								</cac:ManufacturersItemIdentification> ';
            $xml.=' 							</cac:Item>';
            $xml.=' 							<cac:Price>';
            $xml.=' 								<cbc:PriceAmount currencyID="'.$func->getValueFromEnums("currency","currency".$in->invoiceCurrency).'"  >'.str_replace(",",".",round($rowd["amount"],2)).'</cbc:PriceAmount> ';
            $xml.=' 							</cac:Price>';
            $xml.=' 						</cac:InvoiceLine> ';
        }
       $xml.=' 			</Invoice>';
       $fp = fopen(dirname(dirname(__FILE__)) .'/templates/izibiz.xml', 'w');
       fwrite($fp, $xml);
       fclose($fp);
       
       try {
        $client = new SoapClient("https://efaturatest.izibiz.com.tr/EFaturaOIB?wsdl");
       $Req["USER_NAME"] =	$apiUser;
       $Req["PASSWORD"] = $apiPass;
       $RequestHeader["SESSION_ID"] = "-1";
       $Req["REQUEST_HEADER"] = $RequestHeader;
       $Res = $client->Login($Req);
       //$archiveClient = new SoapClient("https://efaturatest.doganedonusum.com:443/EIArchiveWS/EFaturaArchive?wsdl");
       
       //$content = file_get_contents('http://localhost/efcrmapi/process/izibiz/veriefatura.xml');
       
       $Request = array(
           "REQUEST_HEADER"	=>	array(
               "SESSION_ID"	=>	$Res->SESSION_ID,
               "COMPRESSED"	=>	"N"
           ),
                   
                   
                   "RECEIVER" =>	array(
                   "alias"=>$customerUrn,
                   "vkn" => $customerVKN
                   ),
                   "SENDER" =>	array(
                   "alias"=>$senderUrn,
                   "vkn" => $senderVKN
                   ),
                   
                   "INVOICE"	=>	array(
                       "CONTENT"	=>	$xml
                   )
                   
           
       );
       
       print_r($Request);
       $sendF = $client->SendInvoice($Request);
       print_r($sendF);
    } catch (Exception $exc) { // Hata olusursa yakala
        // Son istegi ekrana bas
        echo "Son yapilan istek asagidadir<br/><pre>";
        echo htmlentities($client->__getLastRequest());
        echo "</pre>";
       
        echo "<br/><br/><br/>";
       
        // Son istegin header kismini ekrana bas
        echo "Son yapilan istegin header kismi<br/><pre>";
        echo htmlentities($client->__getLastRequestHeaders());
        echo "</pre>";
       
        echo "<br/><br/><br/>";
       
        // Son yapilan istege sunucunun verdigi yanit
        echo "Son yapilan metod cagrisinin yaniti<br/><pre>";
        echo htmlentities($client->__getLastResponse());
        echo "</pre>";
       
        echo "<br/><br/><br/>";
       
        // Son yapilan istege sunucunun verdigi yanitin header kismi
        echo "Son yapilan metod cagrisinin yanitinin header kismi<br/><pre>";
        echo htmlentities($client->__getLastResponseHeaders());
        echo "</pre>";
        
        echo "Soap Hatasi Olustu: " . $exc->getMessage()."<br>";
        //print_r($exc);
      }
    
}


function split_name($name) {
    $parts = array();

    while ( strlen( trim($name)) > 0 ) {
        $name = trim($name);
        $string = preg_replace('#.*\s([\w-]*)$#', '$1', $name);
        $parts[] = $string;
        $name = trim( preg_replace('#'.$string.'#', '', $name ) );
    }

    if (empty($parts)) {
        return false;
    }

    $parts = array_reverse($parts);
    $name = array();
    $name['first_name'] = $parts[0];
    $name['middle_name'] = (isset($parts[2])) ? $parts[1] : '';
    $name['last_name'] = (isset($parts[2])) ? $parts[2] : ( isset($parts[1]) ? $parts[1] : '');

    return $name;
}

function gen_uuid() {
    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        // 32 bits for "time_low"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

        // 16 bits for "time_mid"
        mt_rand( 0, 0xffff ),

        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 4
        mt_rand( 0, 0x0fff ) | 0x4000,

        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
        mt_rand( 0, 0x3fff ) | 0x8000,

        // 48 bits for "node"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
    );
}



?>
