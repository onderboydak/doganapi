<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once dirname(dirname(dirname(__FILE__))) . "/BL/Tables/invoices.php";
require_once dirname(dirname(dirname(__FILE__))) . "/BL/Tables/invoiceDetails.php";
require_once dirname(dirname(dirname(__FILE__))) . "/BL/Tables/customers.php";
require_once dirname(dirname(dirname(__FILE__))) . "/BL/Tables/accountingSuppliers.php";
require_once dirname(dirname(dirname(__FILE__))) . "/BL/functions.php";

header("Content-type: text/html; charset=utf-8");
date_default_timezone_set('Europe/Istanbul');
setlocale(LC_ALL, "tr_TR");

$apiUrl = "https://efaturatest.izibiz.com.tr/EFaturaOIB?wsdl";
$apiUrlArchieve = "https://efaturatest.izibiz.com.tr/EIArchiveWS/EFaturaArchive?wsdl";
$apiUser = "izibiz-test2";
$apiPass = "izi321";

$client = new SoapClient("https://efaturatest.izibiz.com.tr/AuthenticationWS?wsdl");
$Req["USER_NAME"] =  $apiUser;
$Req["PASSWORD"] = $apiPass;
$RequestHeader["SESSION_ID"] = "-1";
$Req["REQUEST_HEADER"] = $RequestHeader;
$Res = $client->Login($Req);

$client = new SoapClient($apiUrl);

$Request = array(
  "REQUEST_HEADER"  =>  array(
    "SESSION_ID"  =>  $Res->SESSION_ID,
    "COMPRESSED"  =>  "N"
  ),
  "INVOICE_SEARCH_KEY"    =>  array(
    "LIMIT" => "5",
    "START_DATE" => "2019-09-01",
    "END_DATE" => "2019-09-30",
    "READ_INCLUDED" => "true"
  )


);
$sendF = $client->GetInvoice($Request);
$asID = 1;
//print_r($sendF);
foreach ($sendF->INVOICE as $inv) {
  $item = $inv->HEADER;
  $content = $inv->CONTENT->_;

  $content = str_ireplace(['cbc:', ''], '', $content);
  $content = str_ireplace(['cac:', ''], '', $content);

  $content = new SimpleXMLElement($content);
  echo var_dump($item);
  echo var_dump($content);
  echo "<br><br>";

  $invoice = invoices::getInvoiceFromMerchant($content->ID);
  if ($invoice->status != 1000) {
    $customer = customers::getCustomerFromtckVKNNo($item->SENDER, $asID);
    $customerID = $customer->ID;
    if (!$customerID > 0) {
      $customer->vatNumber = $item->SENDER;
      $customer->customer = $item->SUPPLIER;
      $customer->customerShortName = $item->SUPPLIER;
      $customer->customerType = 1;
      $customer->asID = $asID;
      $customer->isVendor = 2;
      $customer->dueDateOptions = 3;
      $customerID = $customer->save();
    } else {
      $customer->isVendor = 3;
      $customerID = $customer->save();
    }
    $invoice->asID = $asID;
    $invoice->invoiceNumber = $content->ID;
    $invoice->invoiceDate = date("Y-m-d", strtotime($item->ISSUE_DATE));
    $invoice->dueDate = date('Y-m-d', strtotime(date('Y-m-d', strtotime("next friday")) . " +7 days"));
    $invoice->dueDateOptions = $customer->dueDateOptions;

    switch ($item->INVOICE_TYPE_CODE) {
      case "0":
        $invoice->invoiceClass = 1;
        break;
      default:
        $invoice->invoiceClass = $item->INVOICE_TYPE_CODE;
    }

    $invoice->invoiceType = 2;
    $invoice->merchantInvoiceID = $item->ENVELOPE_IDENTIFIER;
    $invoice->customerID = $customerID;
    $invoice->tags = "eInvoice";

    switch ($item->PAYABLE_AMOUNT->currencyID) {
      case 'TRY':
        $invoice->invoiceCurrency = 1;
        break;
      case 'USD':
        $invoice->invoiceCurrency = 2;
        break;
      case 'EUR':
        $invoice->invoiceCurrency = 3;
        break;
      case 'GBP':
        $invoice->invoiceCurrency = 4;
        break;
    }

    $invoice->status = 1000;
    $invoiceID = $invoice->save();

    if ($invoiceID > 0) {
      $sql = "delete from invoiceDetails where invoiceID=" . $invoiceID;
      $invoice->executenonquery($sql, null, true);

      $rowNumber = 0;
      foreach ($content->InvoiceLine as $il) {
        $rowNumber++;
        $ind = new invoiceDetails();
        $ind->invoiceID = $invoiceID;
        $ind->rowNumber = $rowNumber;
        $ind->item = ltrim($il->Note);
        $ind->vatRate = isset($il->TaxTotal->TaxSubtotal->Percent) ? $il->TaxTotal->TaxSubtotal->Percent : 18;
        $ind->vatAmount = isset($il->TaxTotal->TaxSubtotal->TaxAmount) ? str_replace(",", ".", floatval($il->TaxTotal->TaxSubtotal->TaxAmount)) : 0;
        $ind->amount = str_replace(",", ".", floatval($il->Price->PriceAmount));
        $ind->unit = $il->InvoicedQuantity;
        $ind->totalAmount = isset($il->LineExtensionAmount) ? str_replace(",", ".", floatval($il->LineExtensionAmount)) : str_replace(",", ".", floatval($il->Price->PriceAmount));
        $ind->save();
      }
    }
  }
}
