<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

header("Content-type: text/html; charset=utf-8");
date_default_timezone_set('Europe/Istanbul');
setlocale(LC_ALL, "tr_TR");

require_once dirname(dirname(__FILE__)) . "/BL/Tables/invoices.php";
require_once dirname(dirname(__FILE__)) . "/BL/communication.php";
$invoice = new invoices();
$m = (date("m")==1) ? 12 : date("m")-1;
$y = (date("m")==1) ? date("Y")-1 : date("Y");

$result = $invoice->getBABS($y,$m);
$term = str_pad($m, 2, '0', STR_PAD_LEFT).".".$y;
$subject =$term. " Mutabakati - ";
while($row=mysqli_fetch_array($result)) {
    if ($row["email"]!="") {
        $template = file_get_contents ( "https://crmapi.efdigitalcodes.com/templates/babsmailtemplate.html" );
        $tl = str_replace ( "@customer", $row["customer"], $template );
        $tl = str_replace ( "@taxnumber", $row["vatNumber"], $tl );
        $tl = str_replace ( "@amount", $row["amount"], $tl );
        $tl = str_replace ( "@formType", $row["formType"], $tl );
        $tl = str_replace ( "@term", $term, $tl );
        $tl = str_replace ( "@invoiceCount", $row["invoiceCount"], $tl );
        $token=md5($m.":".$y.":".$row["asID"].":".$row["customerID"]);
        $tl = str_replace ( "@token", $token, $tl );
        
        $sm = new Mail('onderboydak@hotmail.com', $row["customer"],  ($subject.$row["supplier"]),$tl);
        $sm->sendMail();
        
    }
}
?>
 
