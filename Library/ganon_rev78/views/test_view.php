
<?
	list($period, $date) = split('[=]', $etc);
	if ($date == NULL){
	    $date = date("Y-m-d");
	    $period = "d";
	}
	
	$dataiTunes = createTrendGraphDataJSON($date,$period,1);
	$label = "sales";
	$tablewidth = "95%";
	$tdAlign = "center:left:center";
	$tdPerc = "5%:20%:%75";
	$tdFormat = "none:info:warning";
	$tdDataFormat = "S:S:S";
		
?>

<div id=\"Main\"><br>
	
	<h2><?=label("sales",$_SESSION['clang']);?></h2>
	<br>
	<?createTable($label, $dataiTunes,$tablewidth,$tdAlign, $tdPerc,$tdFormat,$tdDataFormat);?>
</div>