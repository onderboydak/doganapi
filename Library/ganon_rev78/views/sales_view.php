<?
	list($period, $date) = split('[=]', $etc);
	if ($date == NULL){
	    $date = date("Y-m-d");
	    $period = "d";
	}
	
	$dataiTunes = createTrendGraphDataJSON($date,$period,1);
	$dataSpotify = createTrendGraphDataJSON($date,$period,2);
	$dataDeezer = createTrendGraphDataJSON($date,$period,3);
		
?>

<div id=\"Main\"><br>
	
	<h2><?=label("sales",$_SESSION['clang']);?></h2>
	<br>
		<div class="page-control" data-role="page-control">
		        <!-- Responsive controls -->
		        <span class="menu-pull"></span> 
		        <div class="menu-pull-bar"></div>
		        <!-- Tabs -->
				<ul>
		            <li class="active"><a href="#frame1"><?=label("summary",$_SESSION['clang']);?></a></li>
		            <li><a href="#frame2">iTunes</a></li>
		            <li><a href="#frame3">Spotify</a></li>
		            <li><a href="#frame4">Deezer</a></li>
		        </ul>
		        <div class="frames">
		            <div class="frame active" id="frame1"> 
			

						<h3><?=label("downloads",$_SESSION['clang']);?></h3>
						<div id="topbarsub" style="height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;"></div>
						<br>
						<?homeMainiTunes($date,$period,$dataiTunes);?>

						<br>
						<h3><?=label("streams",$_SESSION['clang']);?></h3>
						<div id="topbarsub" style="height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;"></div><br>
	
						<?homeMainSpotify($date,$period,$dataSpotify);?>
						<?homeMainDeezer($date,$period,$dataDeezer);?>

						
					</div>
		            <div class="frame" id="frame2">
						<?
							echo "<span style=\"float:right;font-size:9px;\">".getLastUpdateTime(1)."</span>";
							getArea(200,800,10,40,40,$dataiTunes,"white",1);
						?>

						<h3><?=label("top10artist",$_SESSION['clang']);?></h3>
						
						<div id="topbarsub" style="height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;"></div><br> 
						<?getTopArtists(1,$date);?>
						
						<h3><?=label("top10title",$_SESSION['clang']);?></h3>
						<div id="topbarsub" style="height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;"></div><br> 
						<?getTopTitles(1,$date);?>
						
						<h3><?=label("top10location",$_SESSION['clang']);?></h3>
						<div id="topbarsub" style="height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;"></div><br> 
						<?getTopLoc(1,$date);?>
					</div>

					<div class="frame" id="frame3">
						<?
							echo "<span style=\"float:right;font-size:9px;\">".getLastUpdateTime(2)."</span>";
							getArea(200,800,10,40,40,$dataSpotify,"white",1);
						?>
						<h3><?=label("top10artist",$_SESSION['clang']);?></h3>
						<div id="topbarsub" style="height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;"></div><br> 
						<?getTopArtists(2,$date);?>
						
						<h3><?=label("top10title",$_SESSION['clang']);?></h3>
						<div id="topbarsub" style="height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;"></div><br> 
						<?getTopTitles(2,$date);?>
						
						<h3><?=label("top10location",$_SESSION['clang']);?></h3>
						<div id="topbarsub" style="height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;"></div><br> 
						<?getTopLoc(2,$date);?>
					</div>

					<div class="frame" id="frame4"> 
						<?
							echo "<span style=\"float:right;font-size:9px;\">".getLastUpdateTime(3)."</span>";
							getArea(200,800,10,40,40,$dataDeezer,"white",1);
						?>
						<h3><?=label("top10artist",$_SESSION['clang']);?></h3>
						<div id="topbarsub" style="height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;"></div><br>
						<?getTopArtists(3,$date);?>
						
						<h3><?=label("top10title",$_SESSION['clang']);?></h3>
						<div id="topbarsub" style="height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;"></div><br> 
						<?getTopTitles(3,$date);?>
						
						
						<h3><?=label("top10location",$_SESSION['clang']);?></h3>
						<div id="topbarsub" style="height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;"></div><br> 
						<?getTopLoc(3,$date);?>

					</div>
				</div>
			</div>
	</div>
<?callFooterAdmin();?>