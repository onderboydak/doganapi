<?php
session_start ();
$slideId = (isset ( $_GET ["id"] ) ? $_GET ["id"] : 0);

if ($slideId > 0) {
	$query = "select * from db_web.slider where id=" . $slideId;
	$result = execute ( $query );
	while ( $row = mysqli_fetch_array ( $result ) ) {
		$title = $row ["title"];
		$tags = $row ["tags"];
		$link = $row ["link"];
	}
}

?>
<link href="assets/bootstrap-tagsinput/bootstrap-tagsinput.css"
	rel="stylesheet">
<br>
<h2><?=label("Slide",$_SESSION['clang']);?></h2>
<br>
<form action="process/processWebslide.php" name="webslide" id="webslide"
	method="post" enctype="multipart/form-data">
	<br> <input type="hidden" id="action" name="action" value="1"> <input
		type="hidden" id="sliderId" name="sliderId" value="<?php echo $slideId;?>">
	<table
		style="width: 95%; text-align: right; margin-left: auto; margin-right: auto;"
		border="0" cellpadding="2" cellspacing="0">
		<tbody>
			<tr>
				<td>
					<div class="input-add text">
						<input id="title" name="title" required maxlength="100"
							type="text" value="<?=isset($title) ? $title : "";?>"
							placeholder="<?=label("title",$_SESSION['clang']);?>" />
					</div>
				</td>
			</tr>
			<tr>
				<td style="text-align: left;">
					<div class="input-add text">
						<input id="tags" name="tags" data-role="tagsinput" type="text"
							value="<?=isset($tags) ? $tags : "";?>"
							placeholder="<?=label("tags",$_SESSION['clang']);?>" />
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div class="input-add text">
						<input id="link" name="link" required maxlength="100" type="text"
							value="<?=isset($link) ? $link : "";?>"
							placeholder="<?=label("link",$_SESSION['clang']);?>" />
					</div>
				</td>
			</tr>
			<tr>
				<td style="text-align: left;">
					<a id='slideFile' href='#' class="slideFile"
					onclick="javascript:$('#_slideFile').trigger('click');"><span
					id='slideFileName' style='margin-bottom: 10px;'>Select Slide Picture<i
						class="icon-attachment"></i></span></a> <input type='file'
					id='_slideFile' name='_slideFile' style='display: none'
					accept="text/xml" />
				
				</td>
			</tr>
		</tbody>
	</table>
	<div style="text-align: right; padding-right: 25px;">
		<input id="savePost" name="savePost" type="submit"
			value="<?=label("save",$_SESSION['clang']);?>" />
	</div>
</form>
<script src="assets/ckeditor/ckeditor.js"></script>
<script src="assets/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
<script>
$(function() {
	$('#startDate').datepicker({ dateFormat: 'yy-mm-dd' });
	$('#endDate').datepicker({ dateFormat: 'yy-mm-dd' });
});

$('#tags').tagsinput({
	  tagClass: 'small'
	});

$("#_slideFile").change(function() {
	  var xmlfilename = $(this).val();
	  $("#slideFileName").html(xmlfilename);
});
</script>
