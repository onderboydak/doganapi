<?php
require (dirname ( dirname ( __FILE__ ) ) . '/controllers/php_functions.php');
$scopeDate = (isset ( $_GET ["scopeDate"] ) ? $_GET ["scopeDate"] : "");
$labelUserId = (isset ( $_GET ["labelUserId"] ) ? $_GET ["labelUserId"] : 0);
$label = (isset ( $_GET ["label"] ) ? $_GET ["label"] : 0);

$query = "call db_royalty2.proyaltyDetails('" . $scopeDate . "'," . $labelUserId . ")";
$export = execute ( $query );

$row = mysqli_fetch_assoc ( $export );
$line = "";
$comma = "";
foreach ( $row as $name => $value ) {
	$line .= $comma . '"' . str_replace ( '"', '""', $name ) . '"';
	$comma = ";";
}
$line .= "\n";
$out = $line;
mysqli_data_seek ( $export, 0 );
while ( $row = mysqli_fetch_assoc ( $export ) ) {
	$line = "";
	$comma = "";
	foreach ( $row as $value ) {
		$line .= $comma . '"' . str_replace ( '"', '""', $value ) . '"';
		$comma = ";";
	}
	$line .= "\n";
	$out .= $line;
}

$out = mb_convert_encoding($out, 'UTF-16LE', 'UTF-8');
$out = "\xFF\xFE" . $out;

header('Content-Encoding: UTF-8');
header('Content-Type: text/csv; charset=utf-8'); 
header("Content-Disposition: attachment; filename=" . str_replace ( ' ', '', $label ) . "_" . date ( "Ym", strtotime ( $scopeDate ) ) . ".csv");
header("Pragma: no-cache");
header("Expires: 0");
echo $out;

?>