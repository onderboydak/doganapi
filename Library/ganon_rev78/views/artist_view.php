<?php 
require_once dirname ( dirname ( __FILE__ ) ) .'/controllers/mysql_connect.php';

function getmmID($url){
	//	echo $url;
	$content = url_get_contents($url);
	$jsonde = json_decode($content,true);

	//echo $jsonde["response"]["id"];
	$id = $jsonde["response"]["id"];
	$summary = truncate_html($jsonde["response"]["summary"]["overview"],250);
	$image = $jsonde["response"]["images"][0]["url"];
	return array($id, $summary,$image);
}

?>

<div id="Main">
	<br>
	<h2><?=label("artisttracker",$_SESSION['clang']);?></h2>
	<div id="topbarsub"
		style="height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;"></div>
	<br>
	<form action="?artist" method="POST">
		<div style="width: 100%; text-align: center;">
			<div class="input-add text">
				<input id="query" type="text" placeholder="ArtistName"
					value="<?php echo $_POST['query']; ?>" maxlength="100" size="30"
					required="" name="query">
			</div>
			<br>
			<div class="action" align="center">
				<input type="submit"
					value="<?php echo label("search",$_SESSION['clang']);?>">
			</div>
		</div>
	</form>
	<br> <br>

<?php 
$artist = isset($_POST['query']) ? $_POST['query'] : NULL;
if ($artist <> NULL){	
	
	$artistname = $artist;
	$token = "284f03866a1d48af933cd113ea4441df";
	$url = "http://www.musicbrainz.org/ws/2/artist/?query=".$artistname;
		
	$xml = simplexml_load_file($url);
	if (!$xml) {
		echo "Seems like we have an issue with MusicBrainz Service! Please retry";
	}else{
		$json = json_encode($xml);
		$jsonde = json_decode($json,true);	
		//Fetch musicBrianz XML based on Artist Name	
		echo '<h3>"'.strtoupper($artistname).'"</h3>';
		echo '<div id="topbarsub" style="height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;"></div><br>';
		echo "<table class=\"hovered\" style='width: 95%; text-align: left; margin-left: auto; margin-right: auto;' border='0' cellpadding='2' cellspacing='0'>";
		
		foreach ($jsonde["artist-list"]["artist"] as $artist) {
				
				$musicBrainzID = $artist["@attributes"]["id"];
				//Get musicBrainzID and fetch that to musicMetrics to get summary info, image and musicMetrics ID
				list($musicMetricID,$mmsummary,$image) = getmmID("http://api.semetric.com/entity/musicbrainz:".$musicBrainzID."?token=$token");
				
				$artistName = $artist["name"];
				list($number, $ffid) = checkArtist($artistName);
				
				if($ffid <> NULL){
					list($beingtracked) = checkMusicMetrics($ffid);
					if ($beingtracked == 0){
						$link = "<a href='?artistAction&id=".$musicMetricID."&name=".$artistName."&mbid=".$musicBrainzID."&ffid=".$ffid."'><i class='icon-plus-2'></i></a>";
					}else{
						$link = "<i class='icon-checkmark'></i>";
					}
					$beingtracked = 0;
				}else{
					$link = "<a href='?artistAction&id=".$musicMetricID."&name=".$artistName."&mbid=".$musicBrainzID."&ffid=".$ffid."'><i class='icon-plus-2'></i></a>";
				}

				if($image == '') {$image="/assets/images/no-image.jpg"; }else{}
					
				echo "<tr>";
					echo "<td width='5%'>";
						echo "<span class='jewelImage'><img src='".$image."' width='64px'/></span>";
					echo "</td>";
					echo "<td>";
						echo "<div>".$artistName."</div>";
						echo "<div class=\"tertiary-secondary-text\">".$mmsummary."</div>";
					echo "</td>";
					echo "<td width='5%'>";
					echo $link;
					echo "</td>";
				echo "</tr>";			
		}
	echo "</table>";
	echo '<div id="topbarsub" style="height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;"></div>';
	echo "<span class=\"tertiary-secondary-text\" style=\"float:right;\">by MusicBrainz</span>";
	
	}
}	
?>
</div>
