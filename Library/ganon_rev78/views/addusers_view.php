<?
	parse_str($etc);
	$rd = "?addusers&me&".(isset($_POST['etc']) ? $_POST['etc'] : "") ;
	
//	echo "post[etc]:".$_POST['etc']."<br>etc:".$etc."<br>id:".$id."<br>".$rd."<br>";
	$id = isset($id) ? $id : 0;
	if ($id == 0){
		# No userid specified if admin, new user, if not, yourself edit (addusers&me)
		if ($_SESSION["role"] == 2 || $_SESSION["role"] == 13){
			$new = 1;
		}else{
//			echo "id NULL; not admin";
			redirect($rd);
		}	
	}else if ($etc == "me"){
		$new = 0;
		list($username, $fullname, $password, $email,$type,$userRole,$vendorName,$enableBooklet,$paymentCurrency,$dealTermId,$parentUserId,$enableMusicVideo,$enableYoutube,$cuId) = getUserSettings($_SESSION['userid']);
		list ($contentTypes) = getuserContentTypes ($_SESSION['userid']);
	}else if (substr_count($etc, 'id') == TRUE){
		$new = 0;
		
		if ($_SESSION["role"] == 2 || $_SESSION["role"] == 13){
				$new = 0;
				list($username, $fullname, $password, $email,$type,$userRole,$vendorName,$enableBooklet,$paymentCurrency,$dealTermId,$parentUserId,$enableMusicVideo,$enableYoutube,$cuId) = getUserSettings($id);
				list ($contentTypes) = getuserContentTypes ($id);
			}else{
//				echo "id =x; not admin";
				redirect($rd);
			}
		}
		
		$userId = $_SESSION["userid"];
		if ($userId!=4)
		{
			$addDisabled = " disabled ";
		} else {
			$addDisabled = "";
		}
		
		$addDisabled = (($id==0)  ? "" : $addDisabled);
?> 
		<div id=\"Main\"><br>
			<h2><?=label("settings",$_SESSION['clang']);?></h2>
			<div id="topbarsub" style="height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;"></div>
			<br>
			<div class="page-control" data-role="page-control">
			        <!-- Responsive controls -->
			        <span class="menu-pull"></span> 
			        <div class="menu-pull-bar"></div>
			        <!-- Tabs -->
					<ul>
			            <li class="active"><a href="#frame1"><?=label("adduser",$_SESSION['clang']);?></a></li>
			        </ul>
			        <div class="frames">
			            <div class="frame active" id="frame1"> 
							<div class="inner">    
							 	<div class="feature_explanation">
									<form action="process/processUser.php" name="edit_user" class="edit_user" id="edit_user" method="post">
										<br>
										<table style="width: 95%; text-align: right; margin-left: auto; margin-right: auto;" border="0" cellpadding="2" cellspacing="0">
								 		 <tbody>
											<tr>
												<td>
													<?echo label("fullname",$_SESSION['clang']);?>
												</td>
												<td> 
													<div class="input-add text">
											    		<input id="user_name" name="fullname" required size="30" maxlength="100" type="text" value="<?=isset($fullname) ? $fullname : "";?>" placeholder="<?=label("fullname",$_SESSION['clang']);?>" />
													</div>
												</td>
											</tr>
											<tr>
												<td>
													<?echo label("email",$_SESSION['clang']);?>
												</td>
												<td> 
													<div class="input-add text">
														<input id="user_email_address" name="email" required size="30" maxlength="100" type="text" value="<?=isset($email) ? $email : "";?>" placeholder="<?=label("email",$_SESSION['clang']);?>"/>
													</div>
												</td>
											</tr>
											<tr style="display:none;">
												<td>
													<?echo label("username",$_SESSION['clang']);?>
												</td>
												<td> 
													<div class="input-add text">
												        <input class="user_name" id="user_username" name="username" size="30" maxlength="100" type="text" value="<?=isset($username) ? $username : "";?>"  placeholder="<?=label("username",$_SESSION['clang']);?>" <?if($new==1){echo "";}else{echo "disabled";}?> />
													</div>
												</td>
											</tr>
											<tr>
												<td>
													<?echo label("login_pass_msg",$_SESSION['clang']);?>
												</td>
												<td> 
													<div class="input-add text">
														<input id="user_password" name="password" size="30" type="text" value="<?=isset($password) ? $password : "";?>" placeholder="<?=label("password",$_SESSION['clang']);?>" />
													</div>
												</td>
											</tr>
											<tr>
												<td>
													<?echo label("retypepassword",$_SESSION['clang']);?>
												</td>
												<td> 
													<div class="input-add text">
														<input id="user_password_confirmation" name="password_confirm" size="30" type="text" value="<?=isset($password) ? $password:"";?>" placeholder="<?=label("retypepassword",$_SESSION['clang']);?>" />
													</div>
												</td>
											</tr>
											<tr>
												<td>
													<?echo label("label",$_SESSION['clang']);?>
												</td>
												<td> 
													<div class="input-add text">
											    		<input id="vendorName" name="vendorName" required size="30" maxlength="100" type="text" value="<?=isset($vendorName) ? $vendorName : "";?>" placeholder="<?=label("label",$_SESSION['clang']);?>" />
													</div>
												</td>
											</tr>
											<tr>
												<td>
													<?echo label("Role",$_SESSION['clang']);?>
												</td>
												<td> 
													<input type="hidden" id="userType" name="userType" value="1">
														<div class="input-add select">
													    <select name="role" id="role" placeholder="Pick Role">
													    <?php 
													    	$query = "select roles.Id,concat(roleTypes.roleType,'-',roles.role) as role from ".$_SESSION['db'].".roles 
													    				inner join ".$_SESSION['db'].".roleTypes on roles.roleType = roleTypes.ID where roles.Id<>12 order by roleTypes.ID";
													    	$result = execute($query);
													    	$userRole= (isset($userRole) ? $userRole : 0);
													    	while (list($roleID,$role)= mysqli_fetch_array($result))
																{	
																	
													    ?>
														  			<option value="<?php echo $roleID;?>" <?php echo (($roleID==$userRole) ? "selected" : "");?>><?php echo $role;?></option>
														  			<?php }?>
													    </select>
													    </div>
												</td>
											</tr>	
											<tr>
												<td>
												
													<?php 
													echo label("ContentType",$_SESSION['clang']);?>
												</td>
												<td  style="text-align:left;padding-top:10px;">
													<label class="input-control checkbox">
														<input type="checkbox" id="contentTypeAlbum" name="contentTypeAlbum" <?php echo (strpos((isset($contentTypes) ? $contentTypes : ""), '1')!==false) ? "checked" : ""; ?>/>
														<span class="helper">
															<?php echo label("Music",$_SESSION['clang']);?>
														</span>
														</label>
													<label class="input-control checkbox">
														<input type="checkbox" id="contentTypeMovie" name="contentTypeMovie" <?php echo  (strpos((isset($contentTypes) ? $contentTypes : ""), '2')!==false) ? "checked" : ""; ?>/>
														<span class="helper">
															<?php echo label("Movie",$_SESSION['clang']);?>
														</span>
														</label>
													<label class="input-control checkbox">
														<input type="checkbox" id="contentTypeBooks" name="contentTypeBooks" <?php echo (strpos((isset($contentTypes) ? $contentTypes : ""), '3')!==false) ? "checked" : ""; ?>/>
														<span class="helper">
															<?php echo label("Book",$_SESSION['clang']);?>
														</span>
														</label>
													<label class="input-control checkbox">
														<input type="checkbox" id="contentTypeVideos" name="contentTypeVideos" <?php echo (strpos((isset($contentTypes) ? $contentTypes : ""), '4')!==false) ? "checked" : ""; ?>/>
														<span class="helper">
															<?php echo label("video",$_SESSION['clang']);?>
														</span>
														</label>
													<label class="input-control checkbox">
														<input type="checkbox" id="contentTypeMusicVideo" name="contentTypeMusicVideo" <?php echo (strpos((isset($contentTypes) ? $contentTypes : ""), '5')!==false) ? "checked" : ""; ?>/>
														<span class="helper">
															<?php echo label("MusicVideo",$_SESSION['clang']);?>
														</span>
														</label>
														
												</td>
											</tr>
											<tr>
												<td>
													<?echo label("PaymentCurrency",$_SESSION['clang']);?>
												</td>
												<td> 
														<div class="input-add select">
													    <select name="pcurrency" id="pcurrency" placeholder="Pick Payment Currency">
													    <?php 
													    	$query = "select currencyId,currency from ".$_SESSION['db'].".currency order by currencyId; ";
													    	$result = execute($query);
													    	$userPaymentCurrency= (isset($paymentCurrency) ? $paymentCurrency : 0);
													    	while (list($currencyId,$currency)= mysqli_fetch_array($result))
																{	
													    ?>
														  			<option value="<?php echo $currencyId;?>" <?php echo (($currencyId==$userPaymentCurrency) ? "selected" : "");?>><?php echo $currency;?></option>
														  			<?php }?>
													    </select>
													    </div>
												</td>
											</tr>	
											<tr>
												<td>
													<?php echo label("enable",$_SESSION['clang']);?>
												</td>
												<td  style="text-align:left;padding-top:10px;">
													<label class="input-control checkbox">
														<input type="checkbox" id="enableBooklet" name="enableBooklet" <?php echo ((isset($enableBooklet) ? $enableBooklet : 0) ==1) ? "checked" : ""; ?>/>
														<span class="helper">
															<?php echo label("booklet",$_SESSION['clang']);?>
														</span>
														</label>
														<label class="input-control checkbox">
														<input type="checkbox" id="enableMusicVideo" name="enableMusicVideo" <?php echo ((isset($enableMusicVideo) ? $enableMusicVideo : 0) ==1) ? "checked" : ""; ?>/>
														<span class="helper">
															<?php echo label("musicvideo",$_SESSION['clang']);?>
														</span>
														</label>
															<label class="input-control checkbox">
														<input type="checkbox" id="enableYoutube" name="enableYoutube" <?php echo ((isset($enableYoutube) ? $enableYoutube : 0) ==1) ? "checked" : ""; ?>/>
														<span class="helper">
															<?php echo label("youtube",$_SESSION['clang']);?>
														</span>
														</label>
												</td>
											</tr>
											<tr>
												<td>
													<?echo label("Rate",$_SESSION['clang']);?>
												</td>
												<td> 
														<input type="hidden" value="<?php echo $dealTermId?>" id="dealTermId2" name="dealTermId2">
														<div class="input-add select">
													    <select name="dealTermId" id="dealTermId" placeholder="<?echo label("Rate",$_SESSION['clang']);?>" <?php echo $addDisabled;?>>
													    <?php 
													    	$query = "select id,concat(channel,' (',round(percent*100,2),'%)')  from db_royalty2.dealTerms where dealTerms.isStandart=1 order by channel; ";
													    	$result = execute($query);
													    	$userDealTermId= $dealTermId;
													    	while (list($dealTermId,$dealTerm)= mysqli_fetch_array($result))
																{	
													    ?>
														  			<option value="<?php echo $dealTermId;?>" <?php echo (($dealTermId==$userDealTermId) ? "selected" : "");?>><?php echo $dealTerm;?></option>
														  			<?php }?>
													    </select>
													    </div>
												</td>
											</tr>	
											<tr>
												<td>
													<?echo label("Commission",$_SESSION['clang']);?>
												</td>
												<td> 
														<input type="hidden" value="<?php echo $cuId?>" id="commissionUserId2" name="commissionUserId2">
														<div class="input-add select">
													    <select name="commissionUserId" id="commissionUserId" placeholder="<?echo label("Commission",$_SESSION['clang']);?>" <?php echo $addDisabled;?>>
													    	<option value="0"><?php echo label("Commission",$_SESSION["clang"]);?></option>
													    <?php 
													    	$query = "call db_royalty2.pCommissions()";
													    	//echo $query;
													    	$result = execute($query);
													    	$userCommissionId= $cuId;
													    	while (list($commissionUserId,$commissionUser)= mysqli_fetch_array($result))
																{	
													    ?>			
														  			<option value="<?php echo $commissionUserId;?>" <?php echo (($userCommissionId==$commissionUserId) ? "selected" : "");?>><?php echo $commissionUser;?></option>
														  			<?php }?>
													    </select>
													    </div>
												</td>
											</tr>	
										</tbody>
										</table>
										<br>
										<div class="action" align="right">
											<input type="hidden" name="new" value="<?if ($new == 1){echo "1";}else {echo "0";}?>">
											<input type="hidden" name="etc" value="<?=$etc2;?>">
											<input type="hidden" name="id" value="<?if ($new == 0){echo $id;}else {echo "0";} if($etc == "me"){echo $_SESSION['userid'];}?>">
											<input type="hidden" name="parentUserId" value="4">
									     	 <input id="submit" name="commit" type="submit" value="Save"> 
											<div id="output-div"></div>
									</form>
								</div>
							<div>	
							</div>
						</div>
					</div>
				</div>
	    </div>
	<?callFooterAdmin();?>
	<script>
		$("#role").change(function () {
			var selectedID = $(this).val();
			checkRole (selectedID);
		});

		function checkRole (selectedID) {
			if (selectedID==2 || selectedID==8 || selectedID==9 || selectedID==10 || selectedID==3 || selectedID==4 || selectedID==5 || selectedID==7 || selectedID==11 ) {
				$("#contentTypeAlbum").prop('disabled', true);
				$("#contentTypeMovie").prop('disabled', true);
				$("#contentTypeBooks").prop('disabled', true);
				$("#contentTypeVideos").prop('disabled', true);
				$("#contentTypeMusicVideo").prop('disabled', true);
				
				$("#pcurrency").prop('disabled', true);
				$("#enableBooklet").prop('disabled', true);
				//$("#dealTermId").prop('disabled', true);
			} else {
				$("#contentTypeAlbum").prop('disabled', false);
				$("#contentTypeMovie").prop('disabled', false);
				$("#contentTypeBooks").prop('disabled', false);
				$("#contentTypeVideos").prop('disabled', false);
				$("#contentTypeMusicVideo").prop('disabled', false);
				
				$("#pcurrency").prop('disabled', false);
				$("#enableBooklet").prop('disabled', false);
				//$("#dealTermId").prop('disabled', false);
			}
		}
		
		$( document ).ready(function() {
			var selectedID = <?php echo (isset($userRole) ? $userRole : 0);?>;
			checkRole (selectedID);
		});

	</script>
