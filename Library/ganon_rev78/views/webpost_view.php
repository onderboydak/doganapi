<?php

$query = "select * from db_web.posts order by postDate";
$result = execute($query);



?>
<div id=\"webposts\">
	<br>
	<h2><?=label("webpost",$_SESSION['clang']);?></h2>
	<br>
	<div class="page-control" data-role="page-control">
		<!-- Responsive controls -->
		<span class="menu-pull"></span>
		<div class="menu-pull-bar"></div>
		<table class="hovered" width="95%">
		<tr><td colspan='7' align='right'><a href="?addpost&<?php echo $etc;?>"><span class='modernlabel modernlabel-info'><i class='icon-plus-2' style='font-size: 11px;'></i></span></a></td></tr>
			<?php 
			while($row=mysqli_fetch_array($result)) {
			?>
			<tr id="row_<?php echo $row["id"];?>">
				<td>
					<?php echo $row["title"];?>
				</td>
				<td>
					<?php echo $row["tags"];?>
				</td>
				<td>
					<?php echo dately($row["postDate"]);?>
				</td>
				<td>
					<span id="status_<?php echo  $row["id"];?>" class='modernlabel modernlabel-info'><?php 
						$st = "READY TO GO LIVE";
						switch ($row["status"]) {
							case 0 :
									$st = "READY TO GO LIVE";
									break;
							case 1:
									$st = "LIVE";
									break;
							case 2:
								$st = "ARCHIEVED";
								break;

						}
					echo label($st,$_SESSION['clang']);
					
					?></span>
				</td>
				<td id="statusIcon_<?php echo $row["id"];?>">
					<?php if ($row["status"]==0) {?>
						<a href="javascript:status(<?php echo $row["id"];?>,1)"><i class="icon-checkmark"></i></a>
					<?php } else {?>
						<a href="javascript:status(<?php echo $row["id"];?>,0)"><i class="icon-cancel-2"></i></a>
					<?php }?>
				</td>
				<td width='5%' align='right'>
					<a href='?addpost&id=<?php echo $row["id"];?>'><i class="icon-pencil"></i></a>
				</td>
				<td width='5%' align='right'>
					<a href='javascript:removePost(<?php echo $row["id"];?>);'><i class="icon-remove"></i></a>
				</td>
			</tr>
			<?php 
			}
			?>
		</table>

	</div>
</div>
<script>
	function status (postId,stat) {
		$.post("process/processWebpost.php",{action:4,postId:postId,status:stat},function(data) {
			var rowStatus = '#status_'+postId;
			var statusIcon = '#statusIcon_'+postId;
			if (stat==1) {
				$(rowStatus).html("<?php echo label("LIVE",$_SESSION['clang']);?>");
				$(statusIcon).children().remove();
				$(statusIcon).append ("<a href='javascript:status("+postId+",0)'><i class='icon-cancel-2'></i></a>");
			} else {
				$(rowStatus).html("<?php echo label("READY TO GO LIVE",$_SESSION['clang']);?>");
				$(statusIcon).children().remove();
				$(statusIcon).append ("<a href='javascript:status("+postId+",1)'><i class='icon-checkmark'></i></a>");
			}
			//var rowId = "#row_"+postId;
			//$(rowId).remove();
		});
	}
	function removePost(postId) {

		$.post("process/processWebpost.php",{action:3,postId:postId},function(data) {
			var rowId = "#row_"+postId;
			$(rowId).remove();
		});
		
	}

</script>


