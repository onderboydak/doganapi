<?php
	list($period, $id) = split('[=]', $etc);
	
		$mine = checkOwnership($id,$_SESSION['userid']);
		if ($mine == NULL){
			redirect("?error&id=1");
		}
	
	if ($id <> NULL){
		list($title,$languageId,$artistId,$genreId,$copyright,$releaseDate,$prereleased,$pricing,$upc,$art,$user) = albumInfo($id);
		list($stat,$statid) = contentStatus($id);
		$ostat = label($stat,$_SESSION['clang']);
		
		$te = $title."&nbsp;&nbsp;<span class=\"label\">".$ostat."</span>";
        $appleId = getAdamId($upc);
        if ($appleId == NULL){
//            echo "upc:".$upc."/adam: NULL - No record of Sale";
            $arrow = "";
        }else{
//            echo "upc:".$upc."/adam:".$appleId;
            $arrow = "<span style='float:right;'><a href='?research&id=".$appleId."&".$etc2."'><img src='/images/magglass.png' width='10' length='12'></a></span>";
        }
		//$te = $title."&nbsp;&nbsp;<span class=\"label\">".label(contentStatus($id),$_SESSION['clang'])."</span>";
		$form = "<form method='POST' action='?showcontent&$etc2'>";
		$remove = "<span style='float:right;'><a href='?remove_album&id=".$id."'><i class='icon-remove'></i></a></span>";


        if ($art<>NULL){
			$artfile = "src=\"uploads/".$_SESSION['userid']."/".$id."/".$art."\"";
			$artfiletext = label("changeArt",$_SESSION['clang']);
		}else{
			$artfile = "src=\"uploads/album.jpg\"";
			$artfiletext = label("newArt",$_SESSION['clang']);

		}
	}else{

        echo "opppps";
	}
	echo "<div id=\"Main\">";
	//topSectionContent($page,$period,$date);//Creates the second layer menu and top heading 

	?>	

	<?=$form;?>
	<h2><?=$te;?><?=$arrow;?></h2>
	<div id="topbarsub" style="height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;"></div>
	<div class="inner">    
	 	<div class="feature_explanation">
			<h3><?=label("info",$_SESSION['clang']);?></h3>			
			<table style="width: 100%; text-align: left; margin-left: auto; margin-right: auto;" border="0" cellpadding="2" cellspacing="0">
	 		 <tbody>
				<tr>
					<td rowspan= "6" width='10%' valign='top' align="middle">
						<div id="myModal" class="modal hide fade in" style="display: none;color:#000;">
						  <div class="modal-header">
						    <h3><?=$title;?></h3>

						  </div>
						  <div class="modal-body" >
							<img <?=$artfile;?> width="350px">
						  </div>
						  <div class="modal-footer">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
						  </div>
						</div>
						
						<a href="#myModal" data-toggle="modal" data-target="#myModal"><span class='jewelImage'><img <?=$artfile;?> width='180px'></span></a>
					</td>
					<td width='45%'>
					     <div class="input-add text">
					    <input type="text" placeholder="<?=label("title",$_SESSION['clang']);?>" name="title" value="<?=$title;?>" id="txtTile" disabled/>
					    </div>
					</td>
					<td width='45%'>
						<div class="input-add select">
					    <select name="languageId" disabled>
						  <option><?=label("lang",$_SESSION['clang']);?></option>
						<? languages($languageId);?>
					    </select>
					    </div>
					</td>
				</tr>
				<tr>
					<td>
						 <div class="input-add text">
					    <input type="text" placeholder="UPC" name="upc" value="<?=$upc;?>" id="upc" maxlength="13" disabled/>
					    </div>
					</td>
					<td>
						<div class="input-add select">
					    <select name="genreId" disabled>
						    <option><?=label("genre",$_SESSION['clang']);?></option>
							<? genres($genreId);?>
					    </select>
					    </div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="input-add text">
					    <input type="text" placeholder="<?=label("copyright",$_SESSION['clang']);?>" name="copyright" value="<?=$copyright;?>" id="copyright" disabled/>
					    </div>
					</td>
					<td>
						<div class="input-add select">
					    <select name="price" id="txtPrice" disabled>
                        <option value="21"><?=label("defaultpricing",$_SESSION['clang']);?></option>
						<? prices($tierId,2);?>
					    </select>
					    </div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="input-add select">
					    <select name="released" id='released' disabled>
					    <option selected ><?=label("released",$_SESSION['clang']);?></option>
						<?
							if ($prereleased == 1){
								echo "<option value = \"1\" selected>Yes</option>";
								echo "<option value = \"0\">No</option>";
							}else{
								echo "<option value = \"1\">Yes</option>";
								echo "<option value = \"0\" selected>No</option>";
							}
						?>
					    </select>
					    </div>
					</td>
					
					<td>
						 <div class="input-add text">
					    <input type="text" placeholder="<?=label("releasedate",$_SESSION['clang']);?>" name="releasedate" id="releasedate" value="<?=date ("Y-m-d", strtotime($releaseDate));?>" disabled/>
					    </div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="input-add select">
					    <select name="preorder" id='txtPreorder' disabled>
						<?
							if ($preordered == ''){
								echo "<option value = \"\" selected>".label("preorderd",$_SESSION['clang'])."</option>";
								echo "<option value = \"1\" >".label("preorderYes",$_SESSION['clang'])."</option>";
								echo "<option value = \"0\">".label("preorderNo",$_SESSION['clang'])."</option>";
							}
							elseif ($preordered == 1){
								echo "<option value = \"\">".label("preorderd",$_SESSION['clang'])."</option>";
								echo "<option value = \"1\" selected>".label("preorderYes",$_SESSION['clang'])."</option>";
								echo "<option value = \"0\">".label("preorderNo",$_SESSION['clang'])."</option>";
							}elseif ($preordered == 0){
								echo "<option value = \"\">".label("preorderd",$_SESSION['clang'])."</option>";
								echo "<option value = \"1\">".label("preorderYes",$_SESSION['clang'])."</option>";
								echo "<option value = \"0\" selected>".label("preorderNo",$_SESSION['clang'])."</option>";
							}
						?>
					    </select>
					    </div>
					</td>
					<td>
						 <div class="input-add text">
					    <input type="text" placeholder="<?=label("salesstart",$_SESSION['clang']);?>" name="salesstartdate" id="txtSdate" value="<?if ($salesDate == ''){}else{ echo date ("Y-m-d", strtotime($salesDate));}?>" disabled/>
						<input id="locale" type="hidden" name="locale" value="tr"><!--THIS WILL SET THE DATEPICKER LANGUAGE-->
						</div>
					</td>
				</tr>

				<tr>
					<td>

					</td>
					<td>
						
					</td>
				</tr>
			</tbody>
			</table>
		</div>
	</div>		
	<div id="topbarsub" style="height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;"></div>
	<?
	if ($id <> NULL){
		getArtistsNoEdit($id);
	}else{?>
		<div class="inner">    
		 	<div class="feature_explanation">
				<h3><?=label("artists",$_SESSION['clang']);?></h3>
				<?=label("noartist",$_SESSION['clang']);?>
			</div>
		</div>
	<?}?>
	<div id="topbarsub" style="height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;"></div>
	<?
	if ($id <> NULL){
		getTrackswPlayerNoEdit($id,$etc2);
	}else{?>
		<div class="inner">    
		 	<div class="feature_explanation">
				<h3><?=label("tracks",$_SESSION['clang']);?></h3>
			</div>
		</div>
	<?}?>
	<div id="topbarsub" style="height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;"></div>
	<div>
		<br>
		<table>
			<tr>
				<td width="50%" align="left">
					<?
						$errCount = getReviewErrorCount($id);
						if ($errCount > 0){
					?>
						<a href="contentErrors.php?id=<?=$id;?>" class="first"><i class="icon-warning" style="color: red;"></i><span style="background: white; color: red;"><?echo label('reviewerr',$_SESSION['clang'])." <span class=\"label\">".getReviewErrorCount($id);?></span></span></a>
					<?
						}
					?>
				</td>
				<td width="50%" align="right">
					<?
						$stat = contentStatus($id);
						if($stat == 'err'){
					?>
							<a href="processFixed.php?id=<?=$id;?>&<?=$etc2;?>"><?=label("fixed",$_SESSION['clang']);?></a>
					<?  }elseif($stat == 'created'){
							echo "<a href='publish.php?id=$id&$etc2' class='first'>".label("publish",$_SESSION['clang'])."</a>";
							echo "&nbsp;".label("or",$_SESSION['clang']);
							echo "<a href='?showcontent&$etc2'>".label("backtocontent",$_SESSION['clang'])."</a>";
							
						}else{
					?>		
							<a href="?contents&<?=$etc2;?>"><?=label("backtocontent",$_SESSION['clang']);?></a>
					<?	
						}
					?>
				</td>
			</tr>
		</table>
	</div>	
	</form>
</div>
</div>

