<?
	
	$arr = array();
	$arr = split('[?,&,&]',  $_SERVER['REQUEST_URI']);
	$pre = (isset($arr[0]) ? $arr[0] : "");
	$page = (isset($arr[1]) ? $arr[1] : "");
	$etc = (isset($arr[2]) ? $arr[2] : "");
	$etc2 = (isset($arr[3]) ? $arr[3] : "");

	//list($pre, $page, $etc, $etc2) = split('[?,&,&]', $_SERVER['REQUEST_URI']); 
	
	list($period, $id) = split('[=]', $etc);
//	echo $id;

$isFlexible = 1;//$_SESSION['isFlexible'];
$offset = 0;

if($isFlexible == 1){
	$offset = 2;
}
$errorcount = getReviewErrorCount($id,$limit = 500);
?>

<div class="nav-bar bg-color-white" style="margin-bottom:1px;">
	<br>
	
	<h2 style="float:left;"><?=label("content",$_SESSION['clang']);?></h2>
</div>
<div class="page-control" data-role="page-control">
	<!-- Responsive controls -->
	<span class="menu-pull"></span> 
	<div class="menu-pull-bar"></div>
	<!-- Tabs -->
	<ul>
	    <li class="active"><a href="#frame1"><?=label("album",$_SESSION['clang']);?></a></li>
		<?if($isFlexible == 1){//If provider has access to territory and sales date administration?>	
	    <li><a href="#frame2"><?=label("territory",$_SESSION['clang']);?></a></li>
	    <!--  <li><a href="#frame3"><?=label("saless",$_SESSION['clang']);?></a></li>-->
		<?}else{}?>
	    <li><a href="#frame<?=(3+$offset);?>"><?=label("history",$_SESSION['clang']);?></a></li>
	    <li><a href="#frame<?=(2+$offset);?>"><?=label("reviewerr",$_SESSION['clang']);?></a><?if($errorcount <> 0){?><span class="noti_bubble"><?=$errorcount;?></span><?}else{}?></li>
		<li><a href="#frame<?=(4+$offset);?>"><?=label("status",$_SESSION['clang']);?></a></li>
	</ul>
	<!-- Tabs content -->
	<div class="frames">
	    <div class="frame active" id="frame1">
		<?
				include('newcontent_view.php');
		?>
		</div>
		<?if($isFlexible == 1){//If provider has access to territory and sales date administration?>	
		<div class="frame" id="frame2">  
			<h2><?=label("territory",$_SESSION['clang']);?></h2>
			<div id="topbarsub" style="height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;"></div>
			<br>
			<? include('territory_view.php');?>

		</div>
	    <div class="frame" id="frame3"> 
			<h2><?=label("saless",$_SESSION['clang']);?></h2>
			<div id="topbarsub" style="height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;"></div>
			<br>
			<? include('publish_view.php');?>
		</div>
		<?}else{}?>

		<div class="frame" id="frame<?=(2+$offset);?>"> 
			<h2><?=label("reviewerr",$_SESSION['clang']);?></h2>
			<div id="topbarsub" style="height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;"></div>
			<br>
				<? getReviewErrors($id,500);?>
		</div>
		<div class="frame" id="frame<? echo(3+$offset);?>"> 
			<h2><?=label("history",$_SESSION['clang']);?></h2>
			<div id="topbarsub" style="height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;"></div>
			<br>
				
				<? contentHistory($id);?>
		</div>
		<div class="frame" id="frame<? echo(4+$offset);?>"> 
			<h2><?=label("Status",$_SESSION['clang']);?></h2>
			<div id="topbarsub" style="height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;"></div>
			<br>
				
				<? contentStatusLists($id);?>
		</div>
	</div>
</div>
<?php callFooter(0);?>