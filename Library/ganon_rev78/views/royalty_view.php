<?php
/*
$pageUrl=$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].$_SERVER['QUERY_STRING'];
echo $pageUrl;
$frag = (parse_url($pageUrl));
echo var_dump($frag);
*/
$selectedFrame = (isset($_POST['selectedFrame']) ? $_POST['selectedFrame'] : '#frame1');
if ($selectedFrame=="") {
	$selectedFrame="#frame1";
}
?>
<div id=\"Main\"><br>
	<h2><?=label("Royalties",$_SESSION['clang']);?></h2>
	<br>
	<form method='post' id='frminvoice'>
<input type="hidden" id="selectedFrame" name="selectedFrame" value="<?php echo $selectedFrame;?>"/>	
<input type="hidden" id="monthId" name="monthId" />

	<div class="page-control" data-role="page-control">
	        <!-- Responsive controls -->
	        <span class="menu-pull"></span> 
	        <div class="menu-pull-bar"></div>
	        <!-- Tabs -->
			<ul>
	            <li <?php echo (($selectedFrame=="#frame1") ? " class='active' " : ""); ?>><a href="#frame1"><?=label("Dashboard",$_SESSION['clang']);?></a></li>
	            <li <?php echo (($selectedFrame=="#frame2") ? " class='active' " : ""); ?>><a href="#frame2"><?=label("Invoices",$_SESSION['clang']);?></a></li>
	             <li <?php echo (($selectedFrame=="#frame3") ? " class='active' " : ""); ?>><a href="#frame3"><?=label("Exceptions",$_SESSION['clang']);?></a></li>
	             <li <?php echo (($selectedFrame=="#frame4") ? " class='active' " : ""); ?>><a href="#frame4"><?=label("Received",$_SESSION['clang']);?></a></li>
	        </ul>
	        <div class="frames">
	            <div class="frame <?php echo (($selectedFrame=="#frame1") ? " active " : ""); ?>" id="frame1"> 
	            	<a style="float:right;padding-top:15px;" data-target="#myModalCreateInvoice" data-toggle="modal" href="#myModalCreateInvoice"><?php echo label('createinvoice',$_SESSION['clang']);?></a>
	            	<h3 style="border-bottom: 1px solid #d5d5d5;padding-bottom:10px;"><?php echo label("Invoices",$_SESSION["clang"]);?></h3>
	            	<table class='hovered'>
	            		<?php 
	            			$query = "select 
										scopeShortDate,
										max(case when currency='TRY' then concat (paid,' ') else '' end) as TRY,
										max(case when currency='EUR' then concat (paid,' ') else '' end) as EUR,
										max(case when currency='USD' then concat (paid,' ') else '' end) as USD,
										status,
										statusId,
										scopeDate,
										scopeMonth,
										warn,
										ssd,
										max(case when currency='TRY' then concat (EFShare,' ') else '' end) as TRYS,
										max(case when currency='EUR' then concat (EFShare,' ') else '' end) as EURS,
										max(case when currency='USD' then concat (EFShare,' ') else '' end) as USDS
								from (
								select 
									concat(year(invoiceStatuses.scopeDate),' / ',month(invoiceStatuses.scopeDate)) as scopeShortDate,
									sum(accountPaid) as paid,
									accountCurrency as currency,
									(ifnull(invoiceStatuses.status,0)+1) as status,
									ifnull(invoiceStatuses.invoiceStatusId,0) statusId,
									invoiceStatuses.scopeDate,
									month(invoiceStatuses.scopeDate) scopeMonth,
									db_royalty2.fcheckRoyaltyWarning(invoiceStatuses.scopeDate) as warn,
									invoices.scopeShortDate as ssd,
									sum(ifnull(EFShare,0)) as EFShare
								from db_royalty2.invoices 
													left outer join db_royalty2.invoiceStatuses on  invoices.invoiceStatusId=invoiceStatuses.invoiceStatusId
											group by invoices.invoiceStatusId,accountCurrency
											order by invoiceStatuses.scopeDate) der
								group by 
										statusId
								order by 
										scopeDate
									";
	            			$result = execute($query);
	            			$lastDate ="";
	            			while(list($period,$TRY,$EUR,$USD,$status,$invoiceStatusId,$endDate,$month,$warn,$scopeShortDate,$TRYS,$EURS,$USDS)=mysqli_fetch_array($result))
	            			{
	            				$efshareTRY +=$TRYS;
	            				$efshareEUR +=$EURS;
	            				$efshareUSD +=$USDS;
	            				
	            				$amountTRY += $TRY;
	            				$amountEUR += $EUR;
	            				$amountUSD += $USD;
	            				 
	            				$lastDate=$endDate;
	            				echo "<tr>";
	            				echo "<td style='font-size:small;'>".$period."</td>";
	            				echo "<td style='font-size:small;width:5%;text-align:right'><span class='efshareTRY'>".$TRYS."</span></td>";
	            				echo "<td style='text-align:right;font-size:small;width:13%;border-right:1px solid #ccc;'><span class='summaryTRY'>".$TRY."</span>".(($TRY!='') ? "<span class='modernlabel modernlabel-success'>TRY</span>" : "" )."</td>";
	            				echo "<td style='font-size:small;'><span class='efshareEUR'>".$EURS."</span></td>";
	            				echo "<td style='text-align:right;font-size:small;width:13%;border-right:1px solid #ccc;'><span class='summaryEUR'>".$EUR."</span>".(($EUR!='') ? "<span class='modernlabel modernlabel-success'>EUR</span>" : "" )."</td>";
	            				echo "<td style='font-size:small;'><span class='efshareUSD'>".$USDS."</span></td>";
	            				echo "<td style='text-align:right;font-size:small;width:13%;'><span class='summaryUSD'>".$USD."</span>".(($USD!='') ? "<span class='modernlabel modernlabel-success'>USD</span>" : "" )."</td>";
	            				echo "<td style='text-align:right;font-size:small;'>".(($warn>0) ? "<a href='javascript:goexception(\"".$scopeShortDate."\");'><i style='color:red;' class='icon-warning'></i></a>" : "")."</td>";
	            				echo "<td style='width:12%;text-align:right;'><span class='modernlabel modernlabel-info' id='invoiceResult'>".(($status==1) ? label("waitingapprove",$_SESSION['clang']) : label("approved",$_SESSION['clang']))."</span></td>";
	            				echo "<td style='width:3%;text-align:right;'><a href='javascript:invoiceProcess(".$status.",\"".$period."\",".$invoiceStatusId.");'><i style='font-size:10px;' class='".(($status==2) ? "icon-cancel-2" : "icon-checkmark")."'></i></a></td>";
	            				echo "<td style='width:3%;text-align:right;'>".(($status==1) ? "<a href='javascript:removeInvoice(\"".$invoiceStatusId."\");'><i style='font-size:10px;' class='icon-remove'></i></a>" : "")."</td>";
	            				echo "<td style='width:3%;'><a href=\"javascript:goinvoice('".$scopeShortDate."')\"><img width='10' length='12' src='assets/images/magglass.png'></a></td>";
	            				echo "</tr>";
	            				 
	            			}
	            		?>
	            		<tr>
	            			<td>
	            				<?php echo label("Total",$_SESSION["clang"]);?>
	            			</td>
	            			<td>
	            				<b><span id="efshareTotalTRY"><?php echo $efshareTRY;?></span></b>
	            			</td>
							<td style="text-align: right;border-right:1px solid #ccc;">
								<b><span id="sumTotalTRY"></span><?php echo $amountTRY;?></b>&nbsp;<span class='modernlabel modernlabel-success'>TRY</span>
							</td>
							<td>
								<b><span id="efshareTotalEUR"><?php echo $efshareEUR;?></span></b>
							</td>
							<td  style="text-align: right;border-right:1px solid #ccc;">
								<b><span id="sumTotalEUR"></span><?php echo $amountEUR;?></b>&nbsp;<span class='modernlabel modernlabel-success'>EUR</span>
							</td>
							<td>
								<b><span id="efshareTotalUSD"><?php echo $efshareUSD;?></span></b>
							</td>
							<td  style="text-align: right;">
								<b><span id="sumTotalUSD"></span><?php echo $amountUSD;?></b>&nbsp;<span class='modernlabel modernlabel-success'>USD</span>
							</td>
						</tr>
	            	</table>
				</div>
	            <div class="frame <?php echo (($selectedFrame=="#frame2") ? " active " : ""); ?>" id="frame2"> 
	            	<?php include "views/royalties.php";?>
				</div>
				 <div class="frame <?php echo (($selectedFrame=="#frame3") ? " active " : ""); ?>" id="frame3"> 
				 	<?php include "views/royaltyExceptions.php";?>
				 </div>
				  <div class="frame <?php echo (($selectedFrame=="#frame4") ? " active " : ""); ?>" id="frame4"> 
				  	&nbsp;
				 	<?php include "views/received.php";?>
				 </div>
			</div>
		</div>
		</form>
   </div>
    <div id="myModalCreateInvoice" class="modal hide fade in" style="display: none;color:#000;">
 	<div class="modal-header">
	   <h3><?echo label('createinvoice',$_SESSION['clang']);?></h3>
	</div>
	<div class="modal-body" style='overflow-y:visible; ' >
	
		<div class="input-add select">
			    <select  id="year" name="year">
				    <option value="<?php echo date("Y")-1;?>"><?php echo date("Y")-1;?></option>
				    <option value="<?php echo date("Y");?>"><?php echo date("Y");?></option>
				    <option value="<?php echo date("Y")+1;?>"><?php echo date("Y")+1;?></option>
				</select>
				<select  id="month" name="month">
				    <option value="1"><?php echo label("january",$_SESSION["clang"]);?></option>
				     <option value="2"><?php echo label("february",$_SESSION["clang"]);?></option>
				    <option value="3"><?php echo label("march",$_SESSION["clang"]);?></option>
				     <option value="4"><?php echo label("april",$_SESSION["clang"]);?></option>
				     <option value="5"><?php echo label("may",$_SESSION["clang"]);?></option>
				    <option value="6"><?php echo label("june",$_SESSION["clang"]);?></option>
			   	   <option value="7"><?php echo label("july",$_SESSION["clang"]);?></option>
		      	 	 <option value="8"><?php echo label("august",$_SESSION["clang"]);?></option>
	       		   <option value="9"><?php echo label("september",$_SESSION["clang"]);?></option>
         		   <option value="10"><?php echo label("october",$_SESSION["clang"]);?></option>
            	  <option value="11"><?php echo label("november",$_SESSION["clang"]);?></option>
             	   <option value="12"><?php echo label("december",$_SESSION["clang"]);?></option>
				</select>
    	</div>
    	<br>
    	<h3><?php echo label("Currencies",$_SESSION["clang"]);?></h3>
    	 <div class="input-add text">
	    	<input type="text" placeholder="TRY-USD" title='TRY-USD' name="TRY-USD" value="" id="TRY-USD" maxlength="10" />
	    </div>
	     <div class="input-add text">
	    	<input type="text" placeholder="TRY-EUR" name="TRY-EUR" title="TRY-EUR" value="" id="TRY-EUR" maxlength="10" />
	    </div>
	     <div class="input-add text">
	    	<input type="text" placeholder="USD-TRY" name="USD-TRY" title="USD-TRY" value="" id="USD-TRY" maxlength="10" />
	    </div>
	     <div class="input-add text">
	    	<input type="text" placeholder="USD-EUR" name="USD-EUR" title="USD-EUR" value="" id="USD-EUR" maxlength="10" />
	    </div>
	     <div class="input-add text">
	    	<input type="text" placeholder="EUR-TRY" name="EUR-TRY" title="EUR-TRY" value="" id="EUR-TRY" maxlength="10" />
	    </div>
	     <div class="input-add text">
	    	<input type="text" placeholder="EUR-USD" name="EUR-USD" title="EUR-USD" value="" id="EUR-USD" maxlength="10" />
	    </div>
		<!--  
		<div class="input-add text">
								    <input type="text" class='dates' <?php echo (($lastDate!='') ? " disabled " : "");?> placeholder="<?=label("startdate",$_SESSION['clang']);?>" name="invoicestartdate" title ="<?=label("startdate",$_SESSION['clang']);?>" id="invoicestartdate" value="<?php echo (($lastDate!='') ? date("Y-m-d",strtotime($lastDate)) : date("Y-m-d"));?>"/>
									<input id="locale" type="hidden" name="locale" value="tr">
									</div>
									<div class="input-add text">
								    <input type="text" class='dates' placeholder="<?=label("enddate",$_SESSION['clang']);?>" name="invoiceenddate" title ="<?=label("enddate",$_SESSION['clang']);?>" id="invoiceenddate" value=""/>
									<input id="locale" type="hidden" name="locale" value="tr">
									</div> 
	 -->
	</div>
	<div class="modal-footer">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><?echo label('close',$_SESSION['clang']);?></button>
		<button type="button" class="button bg-color-blue fg-color-white Copy" data-dismiss="modal" onclick="javascript:createInvoice();" aria-hidden="true"><?echo label('Create',$_SESSION['clang']);?></button>
	</div>
</div>
<script>
	            		/*
	$(function() {
		$("#invoicestartdate").datepicker({ dateFormat: 'yy-mm-dd' });
		$("#invoiceenddate").datepicker({ dateFormat: 'yy-mm-dd',minDate:-2 });
	});
*/

	$(document).ready(function() {
		var mo = $("#month").val();
		var ye = $("#year").val();
		
		setCurrencies(ye,mo);
	})
	
	$("#year").change(function() {
		var mo = $("#month").val();
		var ye = $(this).val();
		
		setCurrencies(ye,mo);
    })
    
    $("#month").change(function() {
    	var mo = $(this).val();
		var ye = $("#year").val();
		setCurrencies(ye,mo);
    })
	
	function setCurrencies (year,month) {
		$.post("process/processInvoice.php?status=9",{cyear:year,cmonth:month}).done(function(data) {
			console.log(data);
			if (data!="null") {
				$("#TRY-USD").val("");
				$("#TRY-EUR").val("");
				$("#USD-TRY").val("");
				$("#USD-EUR").val("");
				$("#EUR-TRY").val("");
				$("#EUR-USD").val("");
				
    			$.each(jQuery.parseJSON(data),function(index,data) {
    				$("#"+data.name).val(data.rate);
    			});
			} else {
				$("#TRY-USD").val("");
				$("#TRY-EUR").val("");
				$("#USD-TRY").val("");
				$("#USD-EUR").val("");
				$("#EUR-TRY").val("");
				$("#EUR-USD").val("");
			}
		});
	}
	
	function createInvoice()
	{
		var startDate = $("#year").val()+"-"+$("#month").val()+"-01";
		var TRYUSD = $("#TRY-USD").val();
		var TRYEUR = $("#TRY-EUR").val();
		var USDTRY = $("#USD-TRY").val();
		var USDEUR = $("#USD-EUR").val();
		var EURTRY = $("#EUR-TRY").val();
		var EURUSD = $("#EUR-USD").val();
		
		$.post("process/processCurrency.php",{invoiceDate:startDate,"TRY-USD":TRYUSD,"TRY-EUR":TRYEUR,"USD-TRY":USDTRY,"USD-EUR":USDEUR,"EUR-TRY":EURTRY,"EUR-USD":EURUSD}).done (function() {
			$.post("process/processInvoice.php",{startDate:startDate}).done(function() {
				alert('<?php echo label("Completed!",$_SESSION['clang']);?>');
				$("#frminvoice").submit();
			});
		});
	}

	function summary()
	{
		var res=0;
		var tt=0;
		$(".summaryTRY").each(function() {
		  if (!isNaN(parseFloat($(this).text())))  {
	  		tt=tt+parseFloat($(this).text());
		  }
		})
		
		res = round(tt,2);
		
		$("#sumTotalTRY").text(res);

		tt=0;
		res = 0;
		$(".summaryEUR").each(function() {
			 if (!isNaN(parseFloat($(this).text())))  {
			  		tt=tt+parseFloat($(this).text());
				  }
			});	
		
		res = round(tt,2);
		$("#sumTotalEUR").text(res);
		
		tt=0;
		res = 0;
		$(".summaryUSD").each(function() {
			 if (!isNaN(parseFloat($(this).text())))  {
			  		tt=tt+parseFloat($(this).text());
				  }
			});
		
		res = round(tt,2);
		$("#sumTotalUSD").text(res);
					
		
	}

	function removeInvoice (invoiceStatusId) {
		$.post("process/processinvoice.php?status=3",{invoiceStatusId:invoiceStatusId}).done (function() {
			$("#frminvoice").submit();
		});	
	}

	function invoiceProcess(status,processDate,invoiceStatusId)
	{
		$.post("process/processinvoice.php?status="+status,{processDate:processDate,invoiceStatusId:invoiceStatusId}).done (function() {
			$("#frminvoice").submit();
		});	
	}

	function goinvoice (monthId) {
		   $("#selectedFrame").val("#frame2");
		   $("#monthId").val(monthId);
		   $("#frminvoice").submit();
	}
	function goexception(monthId) {
		 $("#selectedFrame").val("#frame3");
		   $("#monthId").val(monthId);
		   $("#frminvoice").submit();
	}
	

</script>