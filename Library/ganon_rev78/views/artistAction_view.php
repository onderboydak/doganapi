<div id="Main">
<br>
	<h2><?=label("artistProcessed",$_SESSION['clang']);?></h2>
	<div id="topbarsub" style="height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;"></div>
	<br>
<?php 
require_once dirname(dirname(__FILE__)) . '/BL/Tables/artists.php';
require_once dirname(dirname(__FILE__)) . '/BL/Tables/metrics.php';

$token = "284f03866a1d48af933cd113ea4441df";

//	http://api.semetric.com/artist/3a2b13e5a28e47a8afdf7eeb8dedbbad/downloads/total?variant=cumulative&granularity=week&country=TR&token=284f03866a1d48af933cd113ea4441df
//	http://api.semetric.com/artist/3a2b13e5a28e47a8afdf7eeb8dedbbad/fans/twitter?variant=cumulative&granularity=week&country=ALL&token=284f03866a1d48af933cd113ea4441df
//  http://api.semetric.com/artist/3a2b13e5a28e47a8afdf7eeb8dedbbad/plays/vevo?variant=cumulative&granularity=week&country=ALL&token=284f03866a1d48af933cd113ea4441df
//	http://api.semetric.com/artist/3a2b13e5a28e47a8afdf7eeb8dedbbad/views/total?variant=cumulative&granularity=week&country=ALL&token=284f03866a1d48af933cd113ea4441df

function getPlays($id, $artistId, $platform, $period='week', $country='ALL'){
	//vevo, lastfm, soundcloud
	echo "processing Plays for $platform<br>";
	$url = "http://api.semetric.com/artist/".$artistId."/plays/".$platform."?variant=cumulative&granularity=".$period."&country=".$country."&token=284f03866a1d48af933cd113ea4441df";
	$content = url_get_contents($url);
	$jsonde = json_decode($content,true);
	
	$success = $jsonde["success"];
	
	if ($success == 'true'){	
		$epoch = $jsonde["response"]["start_time"];
		$dt = new DateTime("@$epoch");
		$dFt = $dt->format('Y-m-d');
		
		$i = 0;
		foreach($jsonde["response"]["data"] as $item){
			
			$date = strtotime(date("Y-m-d", strtotime($dFt)) . " +".$i." week");
			$d = new DateTime("@$date");
			$dA = $d->format('Y-m-d');
		
			$metric = new metrics();
			$metric->period = $dA;
			$metric->metric = $item;
			$metric->artistId = $id;
			switch ($platform) {
				case "lastfm":
					$metric->metricType = 8;
					break;
				case "soundcloud":
					$metric->metricType = 9;
					break;
			}
			$metric->save();
			$i = $i+1;
		}
	}else{
		echo "Error processing plays (getPlays:$platform".$jsonde["error"]["msg"].")<br>";
	}	
}
function getViews($id, $artistId, $platform, $period='week', $country='ALL'){
	//wikipedia
	echo "processing Views for $platform<br>";
	
	$url = "http://api.semetric.com/artist/".$artistId."/views/".$platform."?variant=cumulative&granularity=".$period."&country=".$country."&token=284f03866a1d48af933cd113ea4441df";
	$content = url_get_contents($url);
	$jsonde = json_decode($content,true);
	
	$success = $jsonde["success"];
	
	if ($success == 'true'){
		$epoch = $jsonde["response"]["start_time"];
		$dt = new DateTime("@$epoch");
		$dFt = $dt->format('Y-m-d');
	
		$i = 0;
		foreach($jsonde["response"]["data"] as $item){
			$date = strtotime(date("Y-m-d", strtotime($dFt)) . " +".$i." week");
			$d = new DateTime("@$date");
			$dA = $d->format('Y-m-d');
	
			//echo $dA." - ".$item."<br>";
	
			$metric = new metrics();
			$metric->period = $dA;
			$metric->metric = $item;
			$metric->artistId = $id;
			$metric->metricType = 10;
			$metric->save();
			$i = $i+1;
				
		}
	}else{
		echo "Error processing views (getViews:$platform".$jsonde["error"]["msg"].")<br>";
			}
}
function getDownloads($id, $artistId, $platform, $period='week', $country='ALL'){
	//bittorent
	echo "processing Downloads for $platform<br>";
	
	$url = "http://api.semetric.com/artist/".$artistId."/downloads/".$platform."?variant=cumulative&granularity=".$period."&country=".$country."&token=284f03866a1d48af933cd113ea4441df";
	$content = url_get_contents($url);
	$jsonde = json_decode($content,true);
	
	$success = $jsonde["success"];
	
	if ($success == 'true'){
		$epoch = $jsonde["response"]["start_time"];
		$dt = new DateTime("@$epoch");
		$dFt = $dt->format('Y-m-d');
			
		$i = 0;
		foreach($jsonde["response"]["data"] as $item){
			$date = strtotime(date("Y-m-d", strtotime($dFt)) . " +".$i." week");
			$d = new DateTime("@$date");
			$dA = $d->format('Y-m-d');
			
			//echo $dA." - ".$item."<br>";
			
			$metric = new metrics();
			$metric->period = $dA;
			$metric->metric = $item;
			$metric->artistId = $id;
			$metric->metricType = 11;
			$metric->save();		
			$i = $i+1;
				
		}
	}else{
		echo "Error processing downloads (getDownloads:$platform".$jsonde["error"]["msg"].")<br>";
			}
}
function getFans($id, $artistId, $platform, $period='week', $country='ALL'){
	//facebook, lastfm, twitter, soundcloud, instagram
	echo "processing Fans for $platform<br>";
	
	$url = "http://api.semetric.com/artist/".$artistId."/fans/".$platform."?variant=cumulative&granularity=".$period."&country=".$country."&token=284f03866a1d48af933cd113ea4441df";
	$content = url_get_contents($url);
	$jsonde = json_decode($content,true);
	
	$success = $jsonde["success"];
	
	if ($success == 'true'){
		$epoch = $jsonde["response"]["start_time"];
		$dt = new DateTime("@$epoch");
		$dFt = $dt->format('Y-m-d');
		
		$i = 0;
		foreach($jsonde["response"]["data"] as $item){
			$date = strtotime(date("Y-m-d", strtotime($dFt)) . " +".$i." week");
			$d = new DateTime("@$date");
			$dA = $d->format('Y-m-d');
		
			$metric = new metrics();
			$metric->period = $dA;
			$metric->metric = $item;
			$metric->artistId = $id;
			switch ($platform) {
				case "facebook":
					$metric->metricType = 1;
					break;
				case "twitter":
					$metric->metricType = 2;
					break;
				case "lastfm":
					$metric->metricType = 5;
					break;
				case "soundcloud":
					$metric->metricType = 6;
					break;
			}
			$metric->save();
			$i = $i+1;
				
		}	
	}else{
		echo "Error processing fans (getFans:$platform".$jsonde["error"]["msg"].")<br>";
			}

}
function insertSocialArtist($artistId, $artistName, $musicBrainzId, $musicMetricId){

	$artist = new artists();
	$artist->artistId=$artistId;
	$artist->artistName=$artistName;
	$artist->musicBrainzId=$musicBrainzId;
	$artist->musicMetricId=$musicMetricId;
	$rowNum = $artist->save();
	return $rowNum;
	//returns the new ID
}
function checkArtistId($musicMetricId){
	$artist = new artists();
	$result = $artist->checkArtist($musicMetricId);
	return $result;
	//returns ID or 0
	
}
function checkArtistIsTracked($factoryFloorId){
	
	$metric = new metrics();
	$result = $metric->checkArtist($factoryFloorId);
	return $result;
	//returns 1 or 0

}
//Format includes musicBrainz, musicMetric and our user table IDs
//http://admin/?artistAction&id=3f04f5a6ea1e486194b0b8159744599d&name=Glennis%20Grace&mbid=26f6fba4-7714-418b-be30-385f01163a63&ffid=611

	$artistName = $_GET['name'];
	$musicMetricId = isset($_GET['id']) ? $_GET['id'] : NULL;
	$musicBrainzId = isset($_GET['mbid']) ? $_GET['mbid'] : NULL;
	$factoryFloorId = isset($_GET['ffid']) ? $_GET['ffid'] : NULL;
	
	if ($musicMetricId <> NULL){

		
		$haveUser = checkArtistId($musicMetricId);
				
		if ($haveUser <> 0){
			echo "We already have this user in DB:".$haveUser."<br>";
			$metricDBid = $haveUser;
		}else{
			$metricDBid = insertSocialArtist($factoryFloorId, $artistName, $musicBrainzId, $musicMetricId);
			echo "We didnt have this user in DB, we just added:".$metricDBid."<br>";
		}

		$exists = checkArtistIsTracked($metricDBid);
		if ($exists == 1){
			echo "We already are tracking this user";
			//Go with schedule
		}else{
			//Dump first and schedule
			echo "OK, we are not tracking this user yet.<br>";
			echo "Lets get the datadump first, then we will start tracking<br>";
			
			getFans($metricDBid, $musicMetricId, "facebook","week","ALL");
			getFans($metricDBid, $musicMetricId, "twitter","week","ALL");			
			getFans($metricDBid, $musicMetricId, "lastfm","week","ALL");
			getFans($metricDBid, $musicMetricId, "soundcloud","week","ALL");
				
			getViews($metricDBid, $musicMetricId, "total","week","ALL");
				
			getPlays($metricDBid, $musicMetricId, "lastfm","week","ALL");
			getPlays($metricDBid, $musicMetricId, "soundcloud","week","ALL");

			getDownloads($metricDBid, $musicMetricId, "total","week","ALL");
				
		}
	
	}else{
		redirect('?artist');
	}

?>
</div>	
