
<div id=\"Main\"><br>
	<h2><?=label("reviews",$_SESSION['clang']);?></h2>
	<br>
	<div class="page-control" data-role="page-control">
	        <!-- Responsive controls -->
	        <span class="menu-pull"></span> 
	        <div class="menu-pull-bar"></div>
	        <!-- Tabs -->
			<ul>
	            <li class="active"><a href="#frame1"><?=label("inreview",$_SESSION['clang']);?></a></li>
	            <li><a href="#frame2"><?=label("fixed",$_SESSION['clang']);?></a></li>
	        </ul>
	        <div class="frames">
	            <div class="frame active" id="frame1"> 
					<?reviews("review");?>
				</div>
	            <div class="frame" id="frame2"> 
					<?reviews("fixed");?>
				</div>
			</div>
		</div>
   </div>
<?
	callFooterAdmin();

?>