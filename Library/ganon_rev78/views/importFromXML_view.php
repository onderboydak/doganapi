<?php
require_once dirname(dirname ( __FILE__ )) . '/controllers/php_functions.php';
?>
<br>
		<h3>Import from MUYAP</h3>
		<form method="post" action="process/processXML.php"
			enctype="multipart/form-data">
			<div>
				<br>
				<p>
					Before to the progress... <br>
					<br> 1. Create user for Albums <br> 2. Copy cover files to
					bulk/Cover folder of server <br> 3. Copy audio files to bulk/Audio
					of server <br>

				</p>
				<br> <a id='xmlFile' href='#' class="file"
					onclick="javascript:$('#_xmlFile').trigger('click');"><span
					id='xmlFileName' style='margin-bottom: 10px;'>Select XML File<i
						class="icon-attachment"></i></span></a> <input type='file'
					id='_xmlFile' name='_xmlFile' style='display: none'
					accept="text/xml" />
			</div>
			<br>
			<div class="input-add text">
				<input type="text" class="folder" name="musicFolder"
					id="musicFolder" value="bulk/"
					placeholder="Type Server Music Folder" disabled>
			</div>
			<br>
			<div class="input-add text">
				<input type="text" class="folder" name="coverFolder"
					id="coverFolder" value="bulk/"
					placeholder="Type Server Cover Folder" disabled>
			</div>
			<br>
			<div class="input-add select">
				<select name="userId" id="userId">
					<option value="0">Select User</option>
					<?php
					$query = "select id,concat(username,' (',fullname,')') as user from db.user order by username";
					$result = execute ( $query );
					while ( list ( $id, $user ) = mysqli_fetch_array ( $result ) ) {
						echo " <option value='" . $id . "'>" . $user . "</option>";
					}
					?>
    </select>
			</div>
			<br>
			<input type="submit" class="button bg-color-ramsBlue fg-color-white"
				value="Submit">
	
	</form>

<script>
  		function importXML() {
  			//var fd = new FormData();
  			//fd.append("_xmlFile", document.getElementById('_xmlFile').files[0]);	
  			
  			$.ajax({
  			    url: "importFromXML.php",
  			    data: document.getElementById('_xmlFile').files[0], 
  			    type: 'POST',
  			    contentType: "text/xml",
  			    dataType: "text",
  			    error : function (xhr, ajaxOptions, thrownError){  
  			        alert(xhr.status);          
  			        alert(thrownError);
  			    } 
  			});
  		}
  		
		$("#_xmlFile").change(function() {
			  var xmlfilename = $(this).val();
			  $("#xmlFileName").html(xmlfilename);
		});

  </script>
