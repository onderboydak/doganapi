<div id="Main">
<br>
	<?php
		$albumId = getAlbumIdbyContentId($_GET['id']);
		$upc = getUPC($_GET['id']);
		list($batchFolder,$packed) = getBatchLocation($albumId);
		detailedDeliveries($_GET['id'], $packed);//List the content in context
		$iid = $_GET['id'];
	?>
	<div class="page-control" data-role="page-control">
	        <!-- Responsive controls -->
	        <span class="menu-pull"></span> 
	        <div class="menu-pull-bar"></div>
	        <!-- Tabs -->
	        <ul>
	            <li class="active"><a href="#frame1">iTunes</a></li>
	            <li><a href="#frame2">Spotify</a></li>
	            <li><a href="#frame3">Deezer</a></li>
	            <li><a href="#frame4">Fuga</a></li>
	            <li><a href="#frame5">Google</a></li>
	            
	        </ul>
	        <!-- Tabs content -->
	        <div class="frames">
	            <div class="frame active" id="frame1"> 
					<table>	
						<tr>
						<td width="70%" class="pull left">
							<?
								list($text,$success) = getDeliveryStatus($iid, 1);
								echo $text;
							?>
						</td>	
						<td width="20%" class="pull left">
							<?if($success==0){?>
							<div class="input-control text">
								<select name="reasons" id="reasons<?=$iid."1";?>" class="clsreason" iid="<? echo($iid); ?>" pl="1">
								<? reasonsDeclinedDeliveries($rId);?>
							    </select>
							</div>
							<?}?>
						</td>
						<td width="10%">
							<?if($success==0){?>
							<p class="tertiary-text">
							<? 
								echo "<a id='l1_$iid' href='process/processDelivery.php?id=$iid&res=1&rea=0&pl=1'><i class='icon-checkmark'></i></a>&nbsp;";
								echo "<a id='l2_$iid' href='process/deliveryErrors.php?in=$iid&res=2&rea=5&pl=1' class='first'><i class='icon-cancel-2'></i></a>";
							?>
							<p>
							<?}?>	
						</td>
						</tr>
						
					</table>
					<h3>BatchFolder <?=$batchFolder;?></h3>
					<div id="topbarsub" style="height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;"></div><br>	
					
					<?
					if($batchFolder ==''){
						echo "<i class=\"icon-warning\" style=\"color: red;\"></i><span class=\"red\">".label("missingfolder",$_SESSION['clang'])."</span>";
						
					}else{
						listDir($batchFolder,2,1);
					}
					echo "<h3>metadata.xml</h3>";
					echo "<div id=\"topbarsub\" style=\"height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;\"></div><br>";
				    echo readXML($batchFolder,1,$upc);
					?>
					<br>
		 		</div>
	            <div class="frame" id="frame2"> 
					<table>	
						<tr>
						<td width="70%" class="pull left">
							<?
								list($text,$success) = getDeliveryStatus($iid, 2);
								echo $text;
							?>						</td>	
						<td width="20%" class="pull left">
							<?if($success==0){?>
							
							<div class="input-control text">
								<select name="reasons" id="reasons<?=$iid."2";?>" class="clsreason" iid="<? echo($iid); ?>" pl="2" <?if($success<>0){echo "disabled";}?>>
								<? reasonsDeclinedDeliveries($rId);?>
							    </select>
							</div>
							<?}?>	
							
						</td>
						<td width="10%">
							<?if($success==0){?>
							<p class="tertiary-text">
							<? 
								echo "<a id='l3_$iid' href='process/processDelivery.php?id=$iid&res=1&rea=0&pl=2'><i class='icon-checkmark'></i></a>&nbsp;";
								echo "<a id='l4_$iid' href='process/deliveryErrors.php?in=$iid&res=2&rea=5&pl=2' class='first'><i class='icon-cancel-2'></i></a>";
							?>
							<p>
							<?}?>
						</td>
						</tr>
					</table>
					<h3>BatchFolder <?=$batchFolder;?></h3>
					<div id="topbarsub" style="height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;"></div><br>	
					<?
					if($batchFolder ==''){
						echo "<i class=\"icon-warning\" style=\"color: red;\"></i><span class=\"red\">".label("missingfolder",$_SESSION['clang'])."</span>";
						
					}else{
						listDir($batchFolder,2,2);
					}				
					echo "<br><h3>Manifest.txt</h3>";
					echo "<div id=\"topbarsub\" style=\"height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;\"></div><br>";
				    echo readManifest($batchFolder,2);

					echo "<br><h3>metadata.xml</h3>";
					echo "<div id=\"topbarsub\" style=\"height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;\"></div><br>";
				    echo readXML($batchFolder,2,$upc);
					?>
					<br>
				</div>
				<div class="frame" id="frame3">  
					<table>	
						<tr>
						<td width="70%" class="pull left">
							<?
								list($text,$success) = getDeliveryStatus($iid, 3);
								echo $text;
							?>						</td>	
						<td width="20%" class="pull left">
							<?if($success==0){?>
							
							<div class="input-control text">
								<select name="reasons" id="reasons<?=$iid."3";?>" class="clsreason" iid="<? echo($iid); ?>" pl="3" <?if($success<>0){echo "disabled";}?>>
								<? reasonsDeclinedDeliveries($rId);?>
							    </select>
							</div>
							<?}?>	
							
						</td>
						<td width="10%">
							<?if($success==0){?>
							<p class="tertiary-text">
							<? 
								echo "<a id='l5_$iid' href='process/processDelivery.php?id=$iid&res=1&rea=0&pl=3'><i class='icon-checkmark'></i></a>&nbsp;";
								echo "<a id='l6_$iid' href='process/deliveryErrors.php?in=$iid&res=2&rea=5&pl=3' class='first'><i class='icon-cancel-2'></i></a>";
							?>
							<p>
							<?}?>
						</td>
						</tr>
						
					</table>
					<h3>BatchFolder <?=$batchFolder;?></h3>
					<div id="topbarsub" style="height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;"></div><br>	

					<?

					if($batchFolder ==''){
						echo "<i class=\"icon-warning\" style=\"color: red;\"></i><span class=\"red\">".label("missingfolder",$_SESSION['clang'])."</span>";
						
					}else{
						listDir($batchFolder,2,3);
					}				
					echo "<br><h3>metadata.xml</h3>";
					echo "<div id=\"topbarsub\" style=\"height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;\"></div><br>";
				    echo readXML($batchFolder,3,$upc);


					?>
				<br>
				</div>
				
				<div class="frame" id="frame4">  
					<table>	
						<tr>
						<td width="70%" class="pull left">
							<?
								list($text,$success) = getDeliveryStatus($iid, 4);
								echo $text;
							?>
						</td>	
						<td width="20%" class="pull left">
							<?if($success==0){?>
							
							<div class="input-control text">
								<select name="reasons" id="reasons<?=$iid."5";?>" class="clsreason" iid="<? echo($iid); ?>" pl="4" <?if($success<>0){echo "disabled";}?>>
								<? reasonsDeclinedDeliveries($rId);?>
							    </select>
							</div>
							<?}?>	
							
						</td>
						<td width="10%">
							<?if($success==0){?>
							<p class="tertiary-text">
							<? 
								echo "<a id='l7_$iid' href='process/processDelivery.php?id=$iid&res=1&rea=0&pl=4'><i class='icon-checkmark'></i></a>&nbsp;";
								echo "<a id='l8_$iid' href='process/deliveryErrors.php?in=$iid&res=2&rea=5&pl=4' class='first'><i class='icon-cancel-2'></i></a>";
							?>
							<p>
							<?}?>
						</td>
						</tr>
						
					</table>
					<h3>BatchFolder <?=$batchFolder;?></h3>
					<div id="topbarsub" style="height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;"></div><br>	

					<?

					if($batchFolder ==''){
						echo "<i class=\"icon-warning\" style=\"color: red;\"></i><span class=\"red\">".label("missingfolder",$_SESSION['clang'])."</span>";
						
					}else{
						listDir($batchFolder,2,4);
					}				
					echo "<br><h3>metadata.xml</h3>";
					echo "<div id=\"topbarsub\" style=\"height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;\"></div><br>";
				    echo readXML($batchFolder,4,$upc);
					?>
				<br>
				</div>
				
				<div class="frame" id="frame5">  
					<table>	
						<tr>
						<td width="70%" class="pull left">
							<?
								list($text,$success) = getDeliveryStatus($iid, 4);
								echo $text;
							?>
						</td>	
						<td width="20%" class="pull left">
							<?if($success==0){?>
							
							<div class="input-control text">
								<select name="reasons" id="reasons<?=$iid."5";?>" class="clsreason" iid="<? echo($iid); ?>" pl="5" <?if($success<>0){echo "disabled";}?>>
								<? reasonsDeclinedDeliveries($rId);?>
							    </select>
							</div>
							<?}?>	
							
						</td>
						<td width="10%">
							<?if($success==0){?>
							<p class="tertiary-text">
							<? 
								echo "<a id='l9_$iid' href='process/processDelivery.php?id=$iid&res=1&rea=0&pl=5'><i class='icon-checkmark'></i></a>&nbsp;";
								echo "<a id='l10_$iid' href='process/deliveryErrors.php?in=$iid&res=2&rea=5&pl=5' class='first'><i class='icon-cancel-2'></i></a>";
							?>
							<p>
							<?}?>
						</td>
						</tr>
						
					</table>
					<h3>BatchFolder <?=$batchFolder;?></h3>
					<div id="topbarsub" style="height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;"></div><br>	

					<?

					if($batchFolder ==''){
						echo "<i class=\"icon-warning\" style=\"color: red;\"></i><span class=\"red\">".label("missingfolder",$_SESSION['clang'])."</span>";
						
					}else{
						listDir($batchFolder,2,5);
					}				
					echo "<br><h3>metadata.xml</h3>";
					echo "<div id=\"topbarsub\" style=\"height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;\"></div><br>";
				    echo readXML($batchFolder,5,$upc);
					?>
				<br>
				</div>				
	        </div>
	    </div>
	<script>
		$(document).ready(function(){				
				$(".clsreason").change(function(){
					var iid = $(".clsreason").attr("iid");
					var pl = $(".clsreason").attr("pl");
					var cmbId = $(this).attr("id");
					var plId = $(this).attr("pl");
					var reasoniid =  $(this).attr("iid");
					var reasonValue =  $("#" + cmbId + " option:selected").val();
					
					var lnk1 = $("#l1_"+ reasoniid);
					var lnk2 = $("#l2_"+ reasoniid);
					var lnk3 = $("#l3_"+ reasoniid);
					var lnk4 = $("#l4_"+ reasoniid);
					var lnk5 = $("#l5_"+ reasoniid);
					var lnk6 = $("#l6_"+ reasoniid);
					var lnk7 = $("#l7_"+ reasoniid);
					var lnk8 = $("#l8_"+ reasoniid);
					var lnk9 = $("#l9_"+ reasoniid);
					var lnk10= $("#l10_"+ reasoniid);
					
					lnk1.removeAttr("href");
					lnk1.attr("href", "process/processDelivery.php?in="+iid+"&res=1&rea="+reasonValue+"&pl="+plId);
					lnk2.removeAttr("href");
					lnk2.attr("href", "process/deliveryErrors.php?in="+iid+"&res=2&rea="+reasonValue+"&pl="+plId);
					lnk3.removeAttr("href");
					lnk3.attr("href", "process/processDelivery.php?in="+iid+"&res=1&rea="+reasonValue+"&pl="+plId);
					lnk4.removeAttr("href");
					lnk4.attr("href", "process/deliveryErrors.php?in="+iid+"&res=2&rea="+reasonValue+"&pl="+plId);
					lnk5.removeAttr("href");
					lnk5.attr("href", "process/processDelivery.php?in="+iid+"&res=1&rea="+reasonValue+"&pl="+plId);
					lnk6.removeAttr("href");
					lnk6.attr("href", "process/deliveryErrors.php?in="+iid+"&res=2&rea="+reasonValue+"&pl="+plId);
					lnk7.removeAttr("href");
					lnk7.attr("href", "process/deliveryErrors.php?in="+iid+"&res=2&rea="+reasonValue+"&pl="+plId);
					lnk8.removeAttr("href");
					lnk8.attr("href", "process/deliveryErrors.php?in="+iid+"&res=2&rea="+reasonValue+"&pl="+plId);
					lnk9.removeAttr("href");
					lnk9.attr("href", "process/deliveryErrors.php?in="+iid+"&res=2&rea="+reasonValue+"&pl="+plId);
					lnk10.removeAttr("href");
					lnk10.attr("href", "process/deliveryErrors.php?in="+iid+"&res=2&rea="+reasonValue+"&pl="+plId);
				});
		});
	</script>
<br>
</div>	
<?	callFooterAdmin();?>