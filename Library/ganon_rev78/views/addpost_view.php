<?php
session_start ();
$postId = (isset ( $_GET ["id"] ) ? $_GET ["id"] : 0);
$content = "";
if ($postId > 0) {
	$query = "select * from db_web.posts where id=" . $postId;
	$result = execute ( $query );
	while ( $row = mysqli_fetch_array ( $result ) ) {
		$title = $row ["title"];
		$tags = $row ["tags"];
		$startDate = date ( "Y-m-d", strtotime ( $row ["startDate"] ) );
		$endDate = date ( "Y-m-d", strtotime ( $row ["endDate"] ) );
		$newRelease = $row ["new"];
		$content = $row ["content"];
		$facebook = $row ["facebook"];
		$twitter = $row ["twitter"];
		$instagram = $row ["instagram"];
		$web = $row ["web"];
		$dontShowOnMain= $row ["dontShowOnMain"];
	}
}

?>
<link href="assets/bootstrap-tagsinput/bootstrap-tagsinput.css"
	rel="stylesheet">
<br>
<h2><?=label("Post",$_SESSION['clang']);?></h2>
<br>
<form action="process/processWebpost.php" name="webpost" id="webpost"
	method="post">
	<br> <input type="hidden" id="action" name="action" value="1"> <input
		type="hidden" id="postId" name="postId" value="<?php echo $postId;?>">
	<table
		style="width: 95%; text-align: right; margin-left: auto; margin-right: auto;"
		border="0" cellpadding="2" cellspacing="0">
		<tbody>
			<tr>
				<td colspan="2">
					<div class="input-add text">
						<input id="title" name="title" required maxlength="100"
							type="text" value="<?=isset($title) ? $title : "";?>"
							placeholder="<?=label("title",$_SESSION['clang']);?>" />
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="text-align: left;">
					<div class="input-add text">
						<input id="tags" name="tags" data-role="tagsinput" type="text"
							value="<?=isset($tags) ? $tags : "";?>"
							placeholder="<?=label("tags",$_SESSION['clang']);?>" />
					</div>
				</td>
			</tr>
			<tr>
				<td style="width: 50%;">
					<div class="input-add text">
						<input id="startDate" name="startDate" required type="text"
							value="<?=isset($startDate) ? $startDate : "";?>"
							placeholder="<?=label("startDate",$_SESSION['clang']);?>" />
					</div>
				</td>
				<td style="width: 50%;">
					<div class="input-add text">
						<input id="endDate" name="endDate" required type="text"
							value="<?=isset($endDate) ? $endDate : "";?>"
							placeholder="<?=label("endDate",$_SESSION['clang']);?>" />
					</div>
				</td>
			</tr>
			<tr>
				<td style="text-align: left;"><label
					style="margin-top: 5px;" class="input-control checkbox"> <input
						type="checkbox" id="newRelease" name="newRelease"
						<?php echo (strpos((isset($newRelease) ? $newRelease : ""), '1')!==false) ? "checked" : ""; ?> />
						<span class="helper">
															<?php echo label("New",$_SESSION['clang']);?>
														</span>
				</label></td>
				<td style="text-align: left;"><label
					style="margin-top: 5px;" class="input-control checkbox"> <input
						type="checkbox" id="dontShowOnMain" name="dontShowOnMain"
						<?php echo (strpos((isset($dontShowOnMain) ? $dontShowOnMain : ""), '1')!==false) ? "checked" : ""; ?> />
						<span class="helper">
															<?php echo label("dontShowOnMain",$_SESSION['clang']);?>
														</span>
				</label></td>
			</tr>
			<tr>
				<td colspan="2" style="text-align:left;">
					<h3 style="text-align: left;border-bottom:1px solid #bababa;"><?=label("Publish",$_SESSION['clang']);?></h3>
					<label style="margin-top: 5px;" class="input-control checkbox"> <input
						type="checkbox" id="web" name="web"
						<?php echo ((strpos((isset($web) ? $web : ""), '1')!==false) ? "checked" :  ((strpos((isset($web) ? $web : ""), '0')!==false) ? "" : "checked")); ?> />
						<span class="helper">Web</span>
					</label>
					<label style="margin-top: 5px;" class="input-control checkbox"> <input
						type="checkbox" id="Facebook" name="Facebook"
						<?php echo (strpos((isset($facebook) ? $facebook : ""), '1')!==false) ? "checked" : ""; ?> />
						<span class="helper">Facebook</span>
					</label>
					<label style="margin-top: 5px;" class="input-control checkbox"> <input
						type="checkbox" id="Twitter" name="Twitter"
						<?php echo (strpos((isset($twitter) ? $twitter : ""), '1')!==false) ? "checked" : ""; ?> />
						<span class="helper">Twitter</span>
					</label>
					<label style="margin-top: 5px;" class="input-control checkbox"> <input
						type="checkbox" id="Instagram" name="Instagram"
						<?php echo (strpos((isset($instagram) ? $instagram : ""), '1')!==false) ? "checked" : ""; ?> />
						<span class="helper">Instagram</span>
					</label>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<h3 style="text-align: left;"><?=label("Content",$_SESSION['clang']);?></h3>
					<textarea class="ckeditor" cols="80" id="content" name="content"
						rows="15"><?php echo $content;?></textarea>
				</td>
			</tr>
		</tbody>
	</table>
	<div style="text-align: right; padding-right: 25px;">
		<input id="savePost" name="savePost" type="submit"
			value="<?=label("save",$_SESSION['clang']);?>" />
	</div>
</form>
<script src="assets/ckeditor/ckeditor.js"></script>
<script src="assets/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
<script>
$(function() {
	$('#startDate').datepicker({ dateFormat: 'yy-mm-dd' });
	$('#endDate').datepicker({ dateFormat: 'yy-mm-dd' });
});

$('#tags').tagsinput({
	  tagClass: 'small'
	});
</script>
