<?php 
$month = isset($_POST['monthId']) ? $_POST['monthId'] : '0';
if ($month=='') {
	$month=0;
}
?>
<div style="border-bottom: 1px solid #d5d5d5;padding-bottom:20px;">
<span class="input-add select" >
<select id="rinvoicePeriod" name="rinvoicePeriod" style="width:20%;float:right;">
<option value='0'><?php echo label("allperiods",$_SESSION['clang']);?></option>
    <?php 
    	$query = "select concat(year(invoiceStatuses.scopeDate),' / ',month(invoiceStatuses.scopeDate)),
					concat(year(invoiceStatuses.scopeDate),case when length(month(invoiceStatuses.scopeDate))=1 then concat('0',month(invoiceStatuses.scopeDate)) else month(invoiceStatuses.scopeDate) end)  as m 
					from db_royalty2.invoiceStatuses 
					group by invoiceStatuses.scopeDate ";
    	$result = execute($query);
    	while (list($processDate,$allMonths) = mysqli_fetch_array($result)) {
			echo "<option value='".$allMonths."' ".(($allMonths==$month) ? "selected" : "").">".$processDate."</option>";
		}
    ?>
	</select>
    </span>
<a href="javascript:addReceive();"><?php echo label('addNew',$_SESSION['clang']);?></a>
<br>
</div>
<table class="hovered" id="receivedTable">
<?php 
$query = "select received.Id,concat(year(received.scopeDate),' / ',month(received.scopeDate)) as scopeDate,
				stores.storeName,
				received.amount,
				received.currency,
				date(received.receivedDate)
				from db_royalty2.received 
					inner join db.stores on stores.ID=received.platformId where
		('".$month."'='0' or concat(year(received.scopeDate),case when length(month(received.scopeDate))=1 then concat('0',month(received.scopeDate)) else month(received.scopeDate) end)='".$month."') 
			order by scopeDate";
//echo $query;
$rTRY =0;
$rEUR =0;
$rUSD=0;
$result = execute($query);
while (list($rId,$rScopeDate,$rStoreName,$rAmount,$rCurrency,$rReceived)=mysqli_fetch_array($result)) {
switch ($rCurrency) {
	case "TRY":
		$rTRY +=$rAmount;
		break;
	case "EUR":
		$rEUR +=$rAmount;
		break;
	case "USD":
		$rUSD +=$rAmount;
		break;
}
	?>
		<tr id ="rrow_<?php echo $rId;?>">
			<td><?php echo $rScopeDate;?></td>
			<td><?php echo $rStoreName;?></td>
			<td style="text-align:right;"><?php echo $rAmount;?></td>
			<td style="text-align:left;"><span class='modernlabel modernlabel-success'><?php echo $rCurrency;?></span></td>
			<td><?php echo $rReceived;?></td>
			<td style="width:3%;"><a href="javascript:editReceive(<?php echo $rId;?>);"><i style='font-size:10px;' class='icon-pencil'></i></a></td>
			<td style="width:3%;"><a data-target="#myModalremoveInvoice<?php echo $rId;?>" data-toggle="modal" href="#myModalremoveInvoice<?php echo $rId;?>"><i style='font-size:10px;' class='icon-remove'></i></a></td>
			 <div id="myModalremoveInvoice<?php echo $rId;?>" class="modal hide fade in" style="display: none;color:#000;">
			 	<div class="modal-header">
				   <h3><?echo label('warning',$_SESSION['clang']);?></h3>
				</div>
				<div class="modal-body" style='overflow-y:visible; ' >
						<span><?php echo label("areyousure",$_SESSION["clang"]);?></span>
				</div>
				<div class="modal-footer">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><?echo label('No',$_SESSION['clang']);?></button>
					<button type="button" class="button bg-color-blue fg-color-white Copy" data-dismiss="modal" onclick="javascript:removeReceive(<?php echo $rId;?>);" aria-hidden="true"><?echo label('Yes',$_SESSION['clang']);?></button>
				</div>
			    	</div>
		</tr>
<?php 	
}
if ($rEUR>0 || $rTRY>0 || $rUSD>0) {
?>
	<tr>
		<td>
			<?php echo label("Total",$_SESSION["clang"]);?>
		</td>
		<td colspan="3" style="text-align: right;padding-right:2px;">
			<b><span id="total">
				<?php 
						echo  $rTRY."&nbsp;<span class='modernlabel modernlabel-success'>TRY</span>&nbsp;&nbsp;" ;
						echo  $rEUR."&nbsp;<span class='modernlabel modernlabel-success'>EUR</span>&nbsp;&nbsp;" ;
						echo  $rUSD."&nbsp;<span class='modernlabel modernlabel-success'>USD</span>&nbsp;&nbsp;" ;
				
				?></span></b>
		</td>
	</tr>
	<?php }?>
</table>
 <div id="myModaladdNewInvoice" class="modal hide fade in" style="display: none;color:#000;">
 	<div class="modal-header">
	   <h3><?echo label('received',$_SESSION['clang']);?></h3>
	</div>
	<div class="modal-body" style='overflow-y:visible;line-height:40px; ' >
	<input type="hidden" id="receiveId" name="receiveId">
			<div class="input-add select" >
				<select id="receivePeriod" name="receivePeriod">
					<option value='0'><?php echo label("SelectPeriot",$_SESSION['clang']);?></option>
						<?php 
						$query = "select concat(year(invoiceStatuses.scopeDate),' / ',month(invoiceStatuses.scopeDate)),
								date(scopeDate)  as m 
								from db_royalty2.invoiceStatuses 
								group by invoiceStatuses.scopeDate ";
					    	$result = execute($query);
					    	while (list($processDate,$allMonths) = mysqli_fetch_array($result)) {
					    		echo "<option value='".$allMonths."'".">".$processDate."</option>";
					    	}
						?>
				</select>
			</div>
			 <div class="input-add text">
		   		 <input type="text" placeholder="<?=label("invoicedate",$_SESSION['clang']);?>" name="invoicedate" id="invoicedate" value=""/>
			</div>	
			<div class="input-add select" >
			<select id="platform" name="platform">
					<?php 
						$query = "select ID,storeName from db.stores where isVisible=1 order by sortOrder;";
						$result = execute($query);
				    	while (list($storeId,$storeName) = mysqli_fetch_array($result)) {
				    		echo "<option value='".$storeId."'".">".$storeName."</option>";
				    	}
					?>
				</select>
			</div>
			 <div class="input-add text">
					<input type="text" placeholder="<?php echo label("amount",$_SESSION["clang"]);?>" name="amount" value="" maxlength="10" id="amount" required/>
             </div>
             <div class="input-add select" >
			<select id="exchange" name="exchange">
					<?php 
						$query = "select currencyId,currency from db.currency;";
						$result = execute($query);
				    	while (list($currencyId,$currency) = mysqli_fetch_array($result)) {
				    		echo "<option value='".$currency."'".">".$currency."</option>";
				    	}
					?>
				</select>
			</div>
	</div>
	<div class="modal-footer">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><?echo label('close',$_SESSION['clang']);?></button>
		<button type="button" class="button bg-color-blue fg-color-white Copy" data-dismiss="modal" onclick="javascript:saveReceive();" aria-hidden="true"><?echo label('Save',$_SESSION['clang']);?></button>
	</div>
    	</div>
   
   
<script>
function addReceive () {
	$("receivedId").val(0);
	$("#receivePeriod").val('');
	$("#invoicedate").val('');
	$("#platform").val(0);
	$("#amount").val('');
	$("#exchange").val('');
	$('#myModaladdNewInvoice').modal("show");
}

					
function editReceive(receiveId) {
	$.post("process/processinvoice.php?status=8",{receiveId:receiveId},function(data) {
		var obj = jQuery.parseJSON( data );
		$("#receivePeriod").val(obj[0].scopeDate);
		$("#invoicedate").val(obj[0].receivedDate);
		$("#platform").val(obj[0].platformId);
		$("#amount").val(obj[0].amount);
		$("#exchange").val(obj[0].currency);
		$("#receiveId").val(receiveId)
		$('#myModaladdNewInvoice').modal("show");
		//$("#selectedFrame").val("#frame4");
		//$("#frminvoice").submit();
	});
}

function removeReceive(receiveId) {
	$.post("process/processinvoice.php?status=7",{receiveId:receiveId},function(data) {
		var rrow = "#rrow_"+receiveId;
		$(rrow).remove();
	});
}

$(function() {	
	$.datepicker.setDefaults($.extend({showMonthAfterYear: false}, $.datepicker.regional['']));
	var date = $('#invoicedate').datepicker({ dateFormat: 'yy-mm-dd' }).val();
	$("#invoicedate").datepicker($.datepicker.regional['tr']);
});
function saveReceive() {
	var receiveId = $("#receiveId").val();
	var receivePeriod = $("#receivePeriod").val();
	var invoicedate = $("#invoicedate").val();
	var platform = $("#platform").val();
	var amount = $("#amount").val();
	var exchange = $("#exchange").val();
	$.post("process/processinvoice.php?status=6",{receiveId:receiveId,receivePeriod:receivePeriod,invoicedate:invoicedate,platform:platform,amount:amount,exchange:exchange},function(data) {
		$("#selectedFrame").val("#frame4");
		$("#frminvoice").submit();
	});
}
$(document).ready(function() {
	$("#rinvoicePeriod").change(function(){
		 $("#monthId").val($(this).val());
		$("#frminvoice").submit();
		});
	});
</script>