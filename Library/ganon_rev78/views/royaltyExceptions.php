<?php 
	session_start();
	$searchText = isset($_POST['searchlabelException']) ? ltrim($_POST['searchlabelException']) : "";
	$month = isset($_POST['monthId']) ? $_POST['monthId'] : '0';
	if ($month=='') {
		$month=0;
	}
?>
<div style="border-bottom: 1px solid #d5d5d5;padding-bottom:10px;">
	<span class="input-add text" >
		<input type="text" id="searchlabelException" style="width:50%;" name="searchlabelException" placeholder="<?php echo label("Title", $_SESSION['clang']);?>" value="<?php echo $searchText;?>"></input>
		<a style='margin-left:-20px;' href='#' onclick='javascript:EXcancelFilter();'><i class="icon-cancel-2"></i></a>
	</span>
		<span class="input-add select" >
		    <select id="exceptionPeriod" name="exceptionPeriod" style="width:20%;float:right;">
		    <option value='0'><?php echo label("allperiods",$_SESSION['clang']);?></option>
		    <?php 
		    	$query = "select concat(year(invoiceStatuses.scopeDate),' / ',month(invoiceStatuses.scopeDate)),
													concat(year(invoiceStatuses.scopeDate),case when length(month(invoiceStatuses.scopeDate))=1 then concat('0',month(invoiceStatuses.scopeDate)) else month(invoiceStatuses.scopeDate) end)  as m 
													from db_royalty2.invoiceStatuses 
													group by invoiceStatuses.scopeDate ";
		    	$result = execute($query);
		    	while (list($processDate,$allMonths) = mysqli_fetch_array($result)) {
					echo "<option value='".$allMonths."' ".(($allMonths==$month) ? "selected" : "").">".$processDate."</option>";
				}
		    ?>
			</select>
		    </span>
</div>
<br>
<table class="hovered">
	<?php 
		$query = "SELECT 'MissingLabel' as exType, royalty.Title,
					concat(year(royalty.scopeDate),' / ',month(royalty.scopeDate)) as periot,
					royalty.dsp, royalty.amountDue,royalty.amountCurrency
					FROM db_royalty2.royalty 
					where ('".$searchText."'='' or royalty.Title='".$searchText."') and 
					('".$month."'='0' or royalty.scopeShortDate='".$month."') and
					(ifnull(EFShare,0)>0 or amountDue>0)  and 
					ifnull(royalty.labelUserId,0)=0
					order by scopeDate,dsp,title";
		
		$result = execute($query);
		$x=0;
		$eTRY =0;
		$eEUR =0;
		$eUSD=0;
		while(list($exType,$title,$period,$dsp,$amountDue,$currency)=mysqli_fetch_array($result))
		{
			switch ($currency) {
				case "TRY":
					$eTRY +=$amountDue;
					break;
				case "EUR":
					$eEUR +=$amountDue;
					break;
				case "USD":
					$eUSD +=$amountDue;
					break;
			}
			
			$x+=1;
			echo "<tr id='row_".$x."'>";
			echo "<td><span class='modernlabel modernlabel-info'>",label($exType,$_SESSION["clang"]),"</span></td>";
			echo "<td style='font-size:small;'>",$title,"</td>";
			echo "<td style='width:10%;font-size:small;'>",$period,"</td>";
			echo "<td><span class='modernlabel modernlabel-info'>",$dsp,"</span></td>";
			echo "<td style='text-align:right;font-size:small;'><span class='EXaccountPaid".$currency."'>",$amountDue,"</span></td>";
			echo "<td><span class='modernlabel modernlabel-success'>",$currency,"</span></td>";
			echo "</tr>";
			
		}
	?>
	<tr>
		<td>
			<?php echo label("Total",$_SESSION["clang"]);?>
		</td>
		<td colspan="5" style="text-align: right;padding-right:2px;">
			<b><span id="exceptionTotal">
			<?php 
						echo  $eTRY."&nbsp;<span class='modernlabel modernlabel-success'>TRY</span>&nbsp;&nbsp;" ;
						echo  $eEUR."&nbsp;<span class='modernlabel modernlabel-success'>EUR</span>&nbsp;&nbsp;" ;
						echo  $eUSD."&nbsp;<span class='modernlabel modernlabel-success'>USD</span>&nbsp;&nbsp;" ;
				
				?></span></b>
		</td>
	</tr>
</table>
<script type="text/javascript">
var options, a;
jQuery(function(){
   options = { 
   		serviceUrl:'autocompleteRoyaltyTitle.php',
   		onSelect : function(value) {
      					 $("#monthId").val($("#exceptionPeriod").val());
						$("#frminvoice").submit();
   					}
   };
   a = $('#searchlabelException').autocomplete(options);
});

function round(value, exp) {
	  if (typeof exp === 'undefined' || +exp === 0)
	    return Math.round(value);

	  value = +value;
	  exp  = +exp;

	  if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
	    return NaN;

	  // Shift
	  value = value.toString().split('e');
	  value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));

	  // Shift back
	  value = value.toString().split('e');
	  return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
	}
	
function totalPaid()
{
	var tt=0;
	$(".EXaccountPaidTRY").each(function() {
		 if (!isNaN(parseFloat($(this).text())))  {
			  tt=tt+parseFloat($(this).text());
		 }
	});
	if (tt>0) {
		$("#exceptionTotal").text($("#exceptionTotal").text()+round(tt,2)+" TRY ");
	}

	 tt=0;
	$(".EXaccountPaidEUR").each(function() {
		 if (!isNaN(parseFloat($(this).text())))  {
			  tt=tt+parseFloat($(this).text());
		 }
	});
	if (tt>0) {
		$("#exceptionTotal").text($("#exceptionTotal").text()+", "+round(tt,2)+ " EUR ");
	}

	 tt=0;
		$(".EXaccountPaidUSD").each(function() {
			 if (!isNaN(parseFloat($(this).text())))  {
				  tt=tt+parseFloat($(this).text());
			 }
		});
		if (tt>0) {
			$("#exceptionTotal").text($("#exceptionTotal").text()+", "+round(tt,2)+" USD ");
		}

		if ($("#exceptionTotal").text().substring(0, 1)==',') {
			$("#exceptionTotal").text($("#exceptionTotal").text().substring(1, $("#exceptionTotal").text().length-1));
		};
}
function EXcancelFilter()
{
	$("#selectedFrame").val("#frame3");
	$("#searchlabelException").text("");
}

$(document).ready(function() {
$("#exceptionPeriod").change(function(){
	 $("#monthId").val($(this).val());
		$("#frminvoice").submit();
	});
	//totalPaid();
});
</script>
