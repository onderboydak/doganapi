<?php 
$sql = "select country from ".$_SESSION['db'].".publishCountries inner join ".$_SESSION['db'].".albums on albums.albumId=publishCountries.albumId where albums.contentId=".$id;
$result = execute($sql);
list($country)=mysqli_fetch_array($result);

$albumId=getAlbumIdbyContentId($id);

?>
<div id="territories_tab" style="display:">
        
	<input id="acquisition_id" name="acquisition[id]" type="hidden" value="65224335" />
	<!-- always LICENSE because we want to show all territories -->
	<input id="acquisition_acquisition_type_license" name="acquisition[acquisition_type]" type="hidden" value="LICENSE" />
	<div id="territory_selector" style="display: 'block'">
                
		<div class="form_row" style="opacity: 0.99999"><input id="acquisition_territory_WORLD" name="acquisition[territory_WORLD]" onclick="toggleWorld(this.checked)" type="checkbox" value="true" />
		<input id="acquisition_territory_WORLD" name="acquisition[territory_WORLD]" type="hidden" value="false" />
	  
		<strong>World</strong>
		<p></p>
		</div>

		<script>
  jsonObj = [];
  var territories = new Array();
  territories['AFRICA'] = new Array();
  territories['AFRICA'].push('DZ');territories['AFRICA'].push('AO');territories['AFRICA'].push('BJ');territories['AFRICA'].push('BW');territories['AFRICA'].push('BF');territories['AFRICA'].push('BI');territories['AFRICA'].push('CM');territories['AFRICA'].push('CV');territories['AFRICA'].push('CF');territories['AFRICA'].push('TD');territories['AFRICA'].push('KM');territories['AFRICA'].push('CG');territories['AFRICA'].push('CD');territories['AFRICA'].push('CI');territories['AFRICA'].push('DJ');territories['AFRICA'].push('EG');territories['AFRICA'].push('GQ');territories['AFRICA'].push('ER');territories['AFRICA'].push('ET');territories['AFRICA'].push('GA');territories['AFRICA'].push('GM');territories['AFRICA'].push('GH');territories['AFRICA'].push('GN');territories['AFRICA'].push('GW');territories['AFRICA'].push('KE');territories['AFRICA'].push('LS');territories['AFRICA'].push('LR');territories['AFRICA'].push('LY');territories['AFRICA'].push('MG');territories['AFRICA'].push('MW');territories['AFRICA'].push('ML');territories['AFRICA'].push('MR');territories['AFRICA'].push('MU');territories['AFRICA'].push('YT');territories['AFRICA'].push('MA');territories['AFRICA'].push('MZ');territories['AFRICA'].push('NA');territories['AFRICA'].push('NE');territories['AFRICA'].push('NG');territories['AFRICA'].push('RE');territories['AFRICA'].push('RW');territories['AFRICA'].push('SH');territories['AFRICA'].push('ST');territories['AFRICA'].push('SN');territories['AFRICA'].push('SC');territories['AFRICA'].push('SL');territories['AFRICA'].push('SO');territories['AFRICA'].push('ZA');territories['AFRICA'].push('SS');territories['AFRICA'].push('SD');territories['AFRICA'].push('SZ');territories['AFRICA'].push('TG');territories['AFRICA'].push('TN');territories['AFRICA'].push('UG');territories['AFRICA'].push('TZ');territories['AFRICA'].push('EH');territories['AFRICA'].push('ZM');territories['AFRICA'].push('ZW');  territories['AMERICAS'] = new Array();
  territories['AMERICAS'].push('AI');territories['AMERICAS'].push('AG');territories['AMERICAS'].push('AR');territories['AMERICAS'].push('AW');territories['AMERICAS'].push('BS');territories['AMERICAS'].push('BB');territories['AMERICAS'].push('BZ');territories['AMERICAS'].push('BM');territories['AMERICAS'].push('BO');territories['AMERICAS'].push('BQ');territories['AMERICAS'].push('BR');territories['AMERICAS'].push('VG');territories['AMERICAS'].push('CA');territories['AMERICAS'].push('KY');territories['AMERICAS'].push('CL');territories['AMERICAS'].push('CO');territories['AMERICAS'].push('CR');territories['AMERICAS'].push('CU');territories['AMERICAS'].push('CW');territories['AMERICAS'].push('DM');territories['AMERICAS'].push('DO');territories['AMERICAS'].push('EC');territories['AMERICAS'].push('SV');territories['AMERICAS'].push('FK');territories['AMERICAS'].push('GF');territories['AMERICAS'].push('GL');territories['AMERICAS'].push('GD');territories['AMERICAS'].push('GP');territories['AMERICAS'].push('GT');territories['AMERICAS'].push('GY');territories['AMERICAS'].push('HT');territories['AMERICAS'].push('HN');territories['AMERICAS'].push('JM');territories['AMERICAS'].push('MQ');territories['AMERICAS'].push('MX');territories['AMERICAS'].push('MS');territories['AMERICAS'].push('NI');territories['AMERICAS'].push('PA');territories['AMERICAS'].push('PY');territories['AMERICAS'].push('PE');territories['AMERICAS'].push('PR');territories['AMERICAS'].push('BL');territories['AMERICAS'].push('KN');territories['AMERICAS'].push('LC');territories['AMERICAS'].push('MF');territories['AMERICAS'].push('PM');territories['AMERICAS'].push('VC');territories['AMERICAS'].push('SX');territories['AMERICAS'].push('SR');territories['AMERICAS'].push('TT');territories['AMERICAS'].push('TC');territories['AMERICAS'].push('VI');territories['AMERICAS'].push('US');territories['AMERICAS'].push('UY');territories['AMERICAS'].push('VE');  territories['ASIA'] = new Array();
  territories['ASIA'].push('AF');territories['ASIA'].push('AM');territories['ASIA'].push('AZ');territories['ASIA'].push('BH');territories['ASIA'].push('BD');territories['ASIA'].push('BT');territories['ASIA'].push('BN');territories['ASIA'].push('KH');territories['ASIA'].push('CN');territories['ASIA'].push('GE');territories['ASIA'].push('HK');territories['ASIA'].push('IN');territories['ASIA'].push('ID');territories['ASIA'].push('IR');territories['ASIA'].push('IQ');territories['ASIA'].push('IL');territories['ASIA'].push('JP');territories['ASIA'].push('JO');territories['ASIA'].push('KZ');territories['ASIA'].push('KP');territories['ASIA'].push('KW');territories['ASIA'].push('KG');territories['ASIA'].push('LA');territories['ASIA'].push('LB');territories['ASIA'].push('MO');territories['ASIA'].push('MY');territories['ASIA'].push('MV');territories['ASIA'].push('MN');territories['ASIA'].push('MM');territories['ASIA'].push('NP');territories['ASIA'].push('PS');territories['ASIA'].push('OM');territories['ASIA'].push('PK');territories['ASIA'].push('PH');territories['ASIA'].push('QA');territories['ASIA'].push('KR');territories['ASIA'].push('SA');territories['ASIA'].push('SG');territories['ASIA'].push('LK');territories['ASIA'].push('SY');territories['ASIA'].push('TW');territories['ASIA'].push('TJ');territories['ASIA'].push('TH');territories['ASIA'].push('TL');territories['ASIA'].push('TR');territories['ASIA'].push('TM');territories['ASIA'].push('AE');territories['ASIA'].push('UZ');territories['ASIA'].push('VN');territories['ASIA'].push('YE');  territories['OCEANIA'] = new Array();
  territories['OCEANIA'].push('AS');territories['OCEANIA'].push('AQ');territories['OCEANIA'].push('AU');territories['OCEANIA'].push('BV');territories['OCEANIA'].push('IO');territories['OCEANIA'].push('CX');territories['OCEANIA'].push('CC');territories['OCEANIA'].push('CK');territories['OCEANIA'].push('FJ');territories['OCEANIA'].push('PF');territories['OCEANIA'].push('TF');territories['OCEANIA'].push('GU');territories['OCEANIA'].push('HM');territories['OCEANIA'].push('KI');territories['OCEANIA'].push('MH');territories['OCEANIA'].push('FM');territories['OCEANIA'].push('NR');territories['OCEANIA'].push('NC');territories['OCEANIA'].push('NZ');territories['OCEANIA'].push('NU');territories['OCEANIA'].push('NF');territories['OCEANIA'].push('MP');territories['OCEANIA'].push('PW');territories['OCEANIA'].push('PG');territories['OCEANIA'].push('PN');territories['OCEANIA'].push('WS');territories['OCEANIA'].push('SB');territories['OCEANIA'].push('GS');territories['OCEANIA'].push('TK');territories['OCEANIA'].push('TO');territories['OCEANIA'].push('TV');territories['OCEANIA'].push('UM');territories['OCEANIA'].push('VU');territories['OCEANIA'].push('WF');  territories['EUROPE'] = new Array();
  territories['EUROPE'].push('AX');territories['EUROPE'].push('AL');territories['EUROPE'].push('AD');territories['EUROPE'].push('AT');territories['EUROPE'].push('BY');territories['EUROPE'].push('BE');territories['EUROPE'].push('BA');territories['EUROPE'].push('BG');territories['EUROPE'].push('HR');territories['EUROPE'].push('CY');territories['EUROPE'].push('CZ');territories['EUROPE'].push('DK');territories['EUROPE'].push('EE');territories['EUROPE'].push('FO');territories['EUROPE'].push('FI');territories['EUROPE'].push('FR');territories['EUROPE'].push('DE');territories['EUROPE'].push('GI');territories['EUROPE'].push('GR');territories['EUROPE'].push('GG');territories['EUROPE'].push('VA');territories['EUROPE'].push('HU');territories['EUROPE'].push('IS');territories['EUROPE'].push('IE');territories['EUROPE'].push('IM');territories['EUROPE'].push('IT');territories['EUROPE'].push('JE');territories['EUROPE'].push('LV');territories['EUROPE'].push('LI');territories['EUROPE'].push('LT');territories['EUROPE'].push('LU');territories['EUROPE'].push('MK');territories['EUROPE'].push('MT');territories['EUROPE'].push('MC');territories['EUROPE'].push('ME');territories['EUROPE'].push('NL');territories['EUROPE'].push('NO');territories['EUROPE'].push('PL');territories['EUROPE'].push('PT');territories['EUROPE'].push('MD');territories['EUROPE'].push('RO');territories['EUROPE'].push('RU');territories['EUROPE'].push('SM');territories['EUROPE'].push('RS');territories['EUROPE'].push('SK');territories['EUROPE'].push('SI');territories['EUROPE'].push('ES');territories['EUROPE'].push('SJ');territories['EUROPE'].push('SE');territories['EUROPE'].push('CH');territories['EUROPE'].push('UA');territories['EUROPE'].push('GB');

  function checkTerritory(id){
      var box = jQuery('#acquisition_territory_' + id + '[type="checkbox"]');
      if(box && !box.is(':disabled')) {
          box.attr('checked',true );
          var strs = box.attr('id').split("_");
          item = {};
          item ["action"] = 'insert';
          item ["country"] = strs[2];
          jsonObj.push(item);
          
		  return true;
		}

		return false;
  }

  function uncheckTerritory(id){
      var box = jQuery('#acquisition_territory_' + id + '[type="checkbox"]');
      if(box && !box.is(':disabled')) {
          box.attr('checked',false );
          var strs = box.attr('id').split("_");
          item = {};
          item ["action"] = 'remove';
          item ["country"] = strs[2];
          jsonObj.push(item);
			return true;
		}
		
		return false;
  }

  function checkTerritories(id){
    checkTerritory(id);
    for(i=0; i < territories[id].length; i++){
      checkTerritory(territories[id][i]);
    }
  }

  function uncheckTerritories(id){
    uncheckTerritory(id);
    for(i=0; i < territories[id].length; i++){
      uncheckTerritory(territories[id][i]);
    }
  }

  function toggleParent(checked,thisId, id){
    if(allCountriesChecked(id)){
      checkTerritory(id);
      if(allContinentsChecked()){
        checkTerritory('WORLD');
      }
    } else {
      uncheckTerritory(id);
      uncheckTerritory('WORLD');
    }

    if($('#' + thisId + '[type="checkbox"]').is(':checked')){
        
        var strs = thisId.split("_");
        item = {};
        item ["action"] = 'insert';
        item ["country"] = strs[2];
        jsonObj.push(item);
	} else {
		 var strs = thisId.split("_");
	        item = {};
	        item ["action"] = 'remove';
	        item ["country"] = strs[2];
	        jsonObj.push(item);
	}
	    
  }

  function toggleContinent(checked, id){
    if(checked){
      checkTerritories(id);
      if(allContinentsChecked()){
        checkTerritory('WORLD');
      }
    } else {
      uncheckTerritories(id);
      uncheckTerritory('WORLD');
    }
  }

  function toggleWorld(checked){
    if(checked){
              checkTerritories('AFRICA');
              checkTerritories('AMERICAS');
              checkTerritories('ASIA');
              checkTerritories('OCEANIA');
              checkTerritories('EUROPE');
              checkTerritories('WORLD');
              
      
    } else {
              uncheckTerritories('AFRICA');
              uncheckTerritories('AMERICAS');
              uncheckTerritories('ASIA');
              uncheckTerritories('OCEANIA');
              uncheckTerritories('EUROPE');
              uncheckTerritories('WORLD');
      
    }
  }

  function allCountriesChecked(id){
    for(i=0; i < territories[id].length; i++){
      if(!jQuery('#acquisition_territory_' + territories[id][i]+  '[type="checkbox"]').is(':checked')){
        return false;
      }
    }
    return true;
  }

  function allContinentsChecked(){
          if(!jQuery('#acquisition_territory_AFRICA[type="checkbox"]').is(':checked')){
        return false;
      }
      for(i=0; i < territories['AFRICA'].length; i++){
          if(!jQuery('#acquisition_territory_'+territories['AFRICA'][i]+'[type="checkbox"]').is(':checked')){
          return false;
        }
      }
          if(!jQuery('#acquisition_territory_AMERICAS[type="checkbox"]').is(':checked')){
        return false;
      }
      for(i=0; i < territories['AMERICAS'].length; i++){
          if(!jQuery('#acquisition_territory_'+territories['AMERICAS'][i]+'[type="checkbox"]').is(':checked')){
          return false;
        }
      }
          if(!jQuery('#acquisition_territory_ASIA[type="checkbox"]').is(':checked')){
        return false;
      }
      for(i=0; i < territories['ASIA'].length; i++){
          if(!jQuery('#acquisition_territory_'+territories['ASIA'][i]+'[type="checkbox"]').is(':checked')){
          return false;
        }
      }
          if(!jQuery('#acquisition_territory_OCEANIA[type="checkbox"]').is(':checked')){
        return false;
      }
      for(i=0; i < territories['OCEANIA'].length; i++){
          if(!jQuery('#acquisition_territory_'+territories['OCEANIA'][i]+'[type="checkbox"]').is(':checked')){
          return false;
        }
      }
          if(!jQuery('#acquisition_territory_EUROPE[type="checkbox"]').is(':checked')){
        return false;
      }
      for(i=0; i < territories['EUROPE'].length; i++){
          if(!jQuery('#acquisition_territory_'+territories['EUROPE'][i]+'[type="checkbox"]').is(':checked')){
          return false;
        }
      }
    
    return true;
  }

</script>

		<table id="territories_table" class="territory">
		    <tr>
		        <td width="20%">
		          <span class="">
					<input id="acquisition_territory_AFRICA" name="acquisition[territory_AFRICA]" type="hidden" value="false" />
		            <input id="acquisition_territory_AFRICA" name="acquisition[territory_AFRICA]" onclick="toggleContinent(this.checked, 'AFRICA')" type="checkbox" value="AFRICA" />
		            <strong>Africa</strong><p></p>
		          </span>
		        </td>
      
		        <td width="20%" >
		          <span class="">

					<input id="acquisition_territory_AMERICAS" name="acquisition[territory_AMERICAS]" type="hidden" value="false" />
		            <input id="acquisition_territory_AMERICAS" name="acquisition[territory_AMERICAS]" onclick="toggleContinent(this.checked, 'AMERICAS')" type="checkbox" value="AMERICAS" />
		            <strong>Americas</strong><p></p>
					
		          </span>
		        </td>
      
		        <td width="20%" >
		          <span class="">

								<input id="acquisition_territory_ASIA" name="acquisition[territory_ASIA]" type="hidden" value="false" />
		            <input id="acquisition_territory_ASIA" name="acquisition[territory_ASIA]" onclick="toggleContinent(this.checked, 'ASIA')" type="checkbox" value="ASIA" />
		            <strong>Asia</strong><p></p>
					
		          </span>
		        </td>
      
		        <td width="20%" >
		          <span class="">

								<input id="acquisition_territory_OCEANIA" name="acquisition[territory_OCEANIA]" type="hidden" value="false" />
		            <input id="acquisition_territory_OCEANIA" name="acquisition[territory_OCEANIA]" onclick="toggleContinent(this.checked, 'OCEANIA')" type="checkbox" value="OCEANIA" />
		            <strong>Oceania</strong><p></p>
					
		          </span>
		        </td>
      
		        <td width="20%" class="last">
		          <span class="">

								<input id="acquisition_territory_EUROPE" name="acquisition[territory_EUROPE]" type="hidden" value="false" />
		            <input id="acquisition_territory_EUROPE" name="acquisition[territory_EUROPE]" onclick="toggleContinent(this.checked, 'EUROPE')" type="checkbox" value="EUROPE" />
		            <strong>Europe</strong><p></p>
					
		          </span>
		        </td>
      
		    </tr>
		<tbody>
			<tr class="odd">
				<td class="territory">
					<div id="territory_DZ">
						<div class="c1">
							<input id="acquisition_territory_DZ" name="acquisition[territory_DZ]" type="hidden" value="false" />
							<input id="acquisition_territory_DZ" name="acquisition[territory_DZ]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="DZ" />
							Algeria
						</div>
					</div>
				</td>  
			<td class="territory">    <div id="territory_AI">
			<div class="c1">

			<input id="acquisition_territory_AI" name="acquisition[territory_AI]" type="hidden" value="false" />
			<input id="acquisition_territory_AI" name="acquisition[territory_AI]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="AI" />
			Anguilla</div>
			</div>
			</td>  <td class="territory">    <div id="territory_AF">
			<div class="c1">

			<input id="acquisition_territory_AF" name="acquisition[territory_AF]" type="hidden" value="false" />
			<input id="acquisition_territory_AF" name="acquisition[territory_AF]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="AF" />
			Afghanistan</div>
			</div>
			</td>  <td class="territory">    <div id="territory_AS">
			<div class="c1">

			<input id="acquisition_territory_AS" name="acquisition[territory_AS]" type="hidden" value="false" />
			<input id="acquisition_territory_AS" name="acquisition[territory_AS]" onclick="toggleParent(this.checked,this.id, 'OCEANIA')" type="checkbox" value="AS" />
			American Samoa</div>
			</div>
			</td>  <td class="territory">    <div id="territory_AX">
			<div class="c1">

			<input id="acquisition_territory_AX" name="acquisition[territory_AX]" type="hidden" value="false" />
			<input id="acquisition_territory_AX" name="acquisition[territory_AX]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="AX" />
			Aland Islands</div>
			</div>
			</td></tr>
		  <tr class="even">
		    <td class="territory">    <div id="territory_AO">
		      <div class="c1">

		          <input id="acquisition_territory_AO" name="acquisition[territory_AO]" type="hidden" value="false" />
		<input id="acquisition_territory_AO" name="acquisition[territory_AO]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="AO" />
		             Angola</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_AG">
		      <div class="c1">

		          <input id="acquisition_territory_AG" name="acquisition[territory_AG]" type="hidden" value="false" />
		<input id="acquisition_territory_AG" name="acquisition[territory_AG]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="AG" />
		             Antigua and Barbuda</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_AM">
		      <div class="c1">

		          <input id="acquisition_territory_AM" name="acquisition[territory_AM]" type="hidden" value="false" />
		<input id="acquisition_territory_AM" name="acquisition[territory_AM]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="AM" />
		             Armenia</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_AQ">
		      <div class="c1">

		          <input id="acquisition_territory_AQ" name="acquisition[territory_AQ]" type="hidden" value="false" />
		<input id="acquisition_territory_AQ" name="acquisition[territory_AQ]" onclick="toggleParent(this.checked,this.id, 'OCEANIA')" type="checkbox" value="AQ" />
		             Antarctica</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_AL">
		      <div class="c1">

		          <input id="acquisition_territory_AL" name="acquisition[territory_AL]" type="hidden" value="false" />
		<input id="acquisition_territory_AL" name="acquisition[territory_AL]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="AL" />
		             Albania</div>
		    </div>
		      </td></tr>
		  <tr class="odd">
		    <td class="territory">    <div id="territory_BJ">
		      <div class="c1">

		          <input id="acquisition_territory_BJ" name="acquisition[territory_BJ]" type="hidden" value="false" />
		<input id="acquisition_territory_BJ" name="acquisition[territory_BJ]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="BJ" />
		             Benin</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_AR">
		      <div class="c1">

		          <input id="acquisition_territory_AR" name="acquisition[territory_AR]" type="hidden" value="false" />
		<input id="acquisition_territory_AR" name="acquisition[territory_AR]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="AR" />
		             Argentina</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_AZ">
		      <div class="c1">

		          <input id="acquisition_territory_AZ" name="acquisition[territory_AZ]" type="hidden" value="false" />
		<input id="acquisition_territory_AZ" name="acquisition[territory_AZ]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="AZ" />
		             Azerbaijan</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_AU">
		      <div class="c1">

		          <input id="acquisition_territory_AU" name="acquisition[territory_AU]" type="hidden" value="false" />
		<input id="acquisition_territory_AU" name="acquisition[territory_AU]" onclick="toggleParent(this.checked,this.id, 'OCEANIA')" type="checkbox" value="AU" />
		             Australia</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_AD">
		      <div class="c1">

		          <input id="acquisition_territory_AD" name="acquisition[territory_AD]" type="hidden" value="false" />
		<input id="acquisition_territory_AD" name="acquisition[territory_AD]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="AD" />
		             Andorra</div>
		    </div>
		      </td></tr>
		  <tr class="even">
		    <td class="territory">    <div id="territory_BW">
		      <div class="c1">

		          <input id="acquisition_territory_BW" name="acquisition[territory_BW]" type="hidden" value="false" />
		<input id="acquisition_territory_BW" name="acquisition[territory_BW]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="BW" />
		             Botswana</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_AW">
		      <div class="c1">

		          <input id="acquisition_territory_AW" name="acquisition[territory_AW]" type="hidden" value="false" />
		<input id="acquisition_territory_AW" name="acquisition[territory_AW]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="AW" />
		             Aruba</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_BH">
		      <div class="c1">

		          <input id="acquisition_territory_BH" name="acquisition[territory_BH]" type="hidden" value="false" />
		<input id="acquisition_territory_BH" name="acquisition[territory_BH]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="BH" />
		             Bahrain</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_BV">
		      <div class="c1">

		          <input id="acquisition_territory_BV" name="acquisition[territory_BV]" type="hidden" value="false" />
		<input id="acquisition_territory_BV" name="acquisition[territory_BV]" onclick="toggleParent(this.checked,this.id, 'OCEANIA')" type="checkbox" value="BV" />
		             Bouvet Island</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_AT">
		      <div class="c1">

		          <input id="acquisition_territory_AT" name="acquisition[territory_AT]" type="hidden" value="false" />
		<input id="acquisition_territory_AT" name="acquisition[territory_AT]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="AT" />
		             Austria</div>
		    </div>
		      </td></tr>
		  <tr class="odd">
		    <td class="territory">    <div id="territory_BF">
		      <div class="c1">

		          <input id="acquisition_territory_BF" name="acquisition[territory_BF]" type="hidden" value="false" />
		<input id="acquisition_territory_BF" name="acquisition[territory_BF]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="BF" />
		             Burkina Faso</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_BS">
		      <div class="c1">

		          <input id="acquisition_territory_BS" name="acquisition[territory_BS]" type="hidden" value="false" />
		<input id="acquisition_territory_BS" name="acquisition[territory_BS]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="BS" />
		             Bahamas</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_BD">
		      <div class="c1">

		          <input id="acquisition_territory_BD" name="acquisition[territory_BD]" type="hidden" value="false" />
		<input id="acquisition_territory_BD" name="acquisition[territory_BD]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="BD" />
		             Bangladesh</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_IO">
		      <div class="c1">

		          <input id="acquisition_territory_IO" name="acquisition[territory_IO]" type="hidden" value="false" />
		<input id="acquisition_territory_IO" name="acquisition[territory_IO]" onclick="toggleParent(this.checked,this.id, 'OCEANIA')" type="checkbox" value="IO" />
		             British Indian Ocean Territory</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_BY">
		      <div class="c1">

		          <input id="acquisition_territory_BY" name="acquisition[territory_BY]" type="hidden" value="false" />
		<input id="acquisition_territory_BY" name="acquisition[territory_BY]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="BY" />
		             Belarus</div>
		    </div>
		      </td></tr>
		  <tr class="even">
		    <td class="territory">    <div id="territory_BI">
		      <div class="c1">

		          <input id="acquisition_territory_BI" name="acquisition[territory_BI]" type="hidden" value="false" />
		<input id="acquisition_territory_BI" name="acquisition[territory_BI]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="BI" />
		             Burundi</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_BB">
		      <div class="c1">

		          <input id="acquisition_territory_BB" name="acquisition[territory_BB]" type="hidden" value="false" />
		<input id="acquisition_territory_BB" name="acquisition[territory_BB]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="BB" />
		             Barbados</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_BT">
		      <div class="c1">

		          <input id="acquisition_territory_BT" name="acquisition[territory_BT]" type="hidden" value="false" />
		<input id="acquisition_territory_BT" name="acquisition[territory_BT]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="BT" />
		             Bhutan</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_CX">
		      <div class="c1">

		          <input id="acquisition_territory_CX" name="acquisition[territory_CX]" type="hidden" value="false" />
		<input id="acquisition_territory_CX" name="acquisition[territory_CX]" onclick="toggleParent(this.checked,this.id, 'OCEANIA')" type="checkbox" value="CX" />
		             Christmas Island</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_BE">
		      <div class="c1">

		          <input id="acquisition_territory_BE" name="acquisition[territory_BE]" type="hidden" value="false" />
		<input id="acquisition_territory_BE" name="acquisition[territory_BE]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="BE" />
		             Belgium</div>
		    </div>
		      </td></tr>
		  <tr class="odd">
		    <td class="territory">    <div id="territory_CM">
		      <div class="c1">

		          <input id="acquisition_territory_CM" name="acquisition[territory_CM]" type="hidden" value="false" />
		<input id="acquisition_territory_CM" name="acquisition[territory_CM]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="CM" />
		             Cameroon</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_BZ">
		      <div class="c1">

		          <input id="acquisition_territory_BZ" name="acquisition[territory_BZ]" type="hidden" value="false" />
		<input id="acquisition_territory_BZ" name="acquisition[territory_BZ]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="BZ" />
		             Belize</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_BN">
		      <div class="c1">

		          <input id="acquisition_territory_BN" name="acquisition[territory_BN]" type="hidden" value="false" />
		<input id="acquisition_territory_BN" name="acquisition[territory_BN]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="BN" />
		             Brunei Darussalam</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_CC">
		      <div class="c1">

		          <input id="acquisition_territory_CC" name="acquisition[territory_CC]" type="hidden" value="false" />
		<input id="acquisition_territory_CC" name="acquisition[territory_CC]" onclick="toggleParent(this.checked,this.id, 'OCEANIA')" type="checkbox" value="CC" />
		             Cocos (Keeling) Islands</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_BA">
		      <div class="c1">

		          <input id="acquisition_territory_BA" name="acquisition[territory_BA]" type="hidden" value="false" />
		<input id="acquisition_territory_BA" name="acquisition[territory_BA]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="BA" />
		             Bosnia and Herzegovina</div>
		    </div>
		      </td></tr>
		  <tr class="even">
		    <td class="territory">    <div id="territory_CV">
		      <div class="c1">

		          <input id="acquisition_territory_CV" name="acquisition[territory_CV]" type="hidden" value="false" />
		<input id="acquisition_territory_CV" name="acquisition[territory_CV]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="CV" />
		             Cape Verde</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_BM">
		      <div class="c1">

		          <input id="acquisition_territory_BM" name="acquisition[territory_BM]" type="hidden" value="false" />
		<input id="acquisition_territory_BM" name="acquisition[territory_BM]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="BM" />
		             Bermuda</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_KH">
		      <div class="c1">

		          <input id="acquisition_territory_KH" name="acquisition[territory_KH]" type="hidden" value="false" />
		<input id="acquisition_territory_KH" name="acquisition[territory_KH]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="KH" />
		             Cambodia</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_CK">
		      <div class="c1">

		          <input id="acquisition_territory_CK" name="acquisition[territory_CK]" type="hidden" value="false" />
		<input id="acquisition_territory_CK" name="acquisition[territory_CK]" onclick="toggleParent(this.checked,this.id, 'OCEANIA')" type="checkbox" value="CK" />
		             Cook Islands</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_BG">
		      <div class="c1">

		          <input id="acquisition_territory_BG" name="acquisition[territory_BG]" type="hidden" value="false" />
		<input id="acquisition_territory_BG" name="acquisition[territory_BG]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="BG" />
		             Bulgaria</div>
		    </div>
		      </td></tr>
		  <tr class="odd">
		    <td class="territory">    <div id="territory_CF">
		      <div class="c1">

		          <input id="acquisition_territory_CF" name="acquisition[territory_CF]" type="hidden" value="false" />
		<input id="acquisition_territory_CF" name="acquisition[territory_CF]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="CF" />
		             Central African Republic</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_BO">
		      <div class="c1">

		          <input id="acquisition_territory_BO" name="acquisition[territory_BO]" type="hidden" value="false" />
		<input id="acquisition_territory_BO" name="acquisition[territory_BO]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="BO" />
		             Bolivia</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_CN">
		      <div class="c1">

		          <input id="acquisition_territory_CN" name="acquisition[territory_CN]" type="hidden" value="false" />
		<input id="acquisition_territory_CN" name="acquisition[territory_CN]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="CN" />
		             China</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_FJ">
		      <div class="c1">

		          <input id="acquisition_territory_FJ" name="acquisition[territory_FJ]" type="hidden" value="false" />
		<input id="acquisition_territory_FJ" name="acquisition[territory_FJ]" onclick="toggleParent(this.checked,this.id, 'OCEANIA')" type="checkbox" value="FJ" />
		             Fiji</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_HR">
		      <div class="c1">

		          <input id="acquisition_territory_HR" name="acquisition[territory_HR]" type="hidden" value="false" />
		<input id="acquisition_territory_HR" name="acquisition[territory_HR]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="HR" />
		             Croatia</div>
		    </div>
		      </td></tr>
		  <tr class="even">
		    <td class="territory">    <div id="territory_TD">
		      <div class="c1">

		          <input id="acquisition_territory_TD" name="acquisition[territory_TD]" type="hidden" value="false" />
		<input id="acquisition_territory_TD" name="acquisition[territory_TD]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="TD" />
		             Chad</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_BQ">
		      <div class="c1">

		          <input id="acquisition_territory_BQ" name="acquisition[territory_BQ]" type="hidden" value="false" />
		<input id="acquisition_territory_BQ" name="acquisition[territory_BQ]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="BQ" />
		             Bonaire, Saint Eustatius and Saba</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_GE">
		      <div class="c1">

		          <input id="acquisition_territory_GE" name="acquisition[territory_GE]" type="hidden" value="false" />
		<input id="acquisition_territory_GE" name="acquisition[territory_GE]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="GE" />
		             Georgia</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_PF">
		      <div class="c1">

		          <input id="acquisition_territory_PF" name="acquisition[territory_PF]" type="hidden" value="false" />
		<input id="acquisition_territory_PF" name="acquisition[territory_PF]" onclick="toggleParent(this.checked,this.id, 'OCEANIA')" type="checkbox" value="PF" />
		             French Polynesia</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_CY">
		      <div class="c1">

		          <input id="acquisition_territory_CY" name="acquisition[territory_CY]" type="hidden" value="false" />
		<input id="acquisition_territory_CY" name="acquisition[territory_CY]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="CY" />
		             Cyprus</div>
		    </div>
		      </td></tr>
		  <tr class="odd">
		    <td class="territory">    <div id="territory_KM">
		      <div class="c1">

		          <input id="acquisition_territory_KM" name="acquisition[territory_KM]" type="hidden" value="false" />
		<input id="acquisition_territory_KM" name="acquisition[territory_KM]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="KM" />
		             Comoros</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_BR">
		      <div class="c1">

		          <input id="acquisition_territory_BR" name="acquisition[territory_BR]" type="hidden" value="false" />
		<input id="acquisition_territory_BR" name="acquisition[territory_BR]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="BR" />
		             Brazil</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_HK">
		      <div class="c1">

		          <input id="acquisition_territory_HK" name="acquisition[territory_HK]" type="hidden" value="false" />
		<input id="acquisition_territory_HK" name="acquisition[territory_HK]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="HK" />
		             Hong Kong</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_TF">
		      <div class="c1">

		          <input id="acquisition_territory_TF" name="acquisition[territory_TF]" type="hidden" value="false" />
		<input id="acquisition_territory_TF" name="acquisition[territory_TF]" onclick="toggleParent(this.checked,this.id, 'OCEANIA')" type="checkbox" value="TF" />
		             French Southern Territories</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_CZ">
		      <div class="c1">

		          <input id="acquisition_territory_CZ" name="acquisition[territory_CZ]" type="hidden" value="false" />
		<input id="acquisition_territory_CZ" name="acquisition[territory_CZ]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="CZ" />
		             Czech Republic</div>
		    </div>
		      </td></tr>
		  <tr class="even">
		    <td class="territory">    <div id="territory_CG">
		      <div class="c1">

		          <input id="acquisition_territory_CG" name="acquisition[territory_CG]" type="hidden" value="false" />
		<input id="acquisition_territory_CG" name="acquisition[territory_CG]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="CG" />
		             Congo</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_VG">
		      <div class="c1">

		          <input id="acquisition_territory_VG" name="acquisition[territory_VG]" type="hidden" value="false" />
		<input id="acquisition_territory_VG" name="acquisition[territory_VG]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="VG" />
		             British Virgin Islands</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_IN">
		      <div class="c1">

		          <input id="acquisition_territory_IN" name="acquisition[territory_IN]" type="hidden" value="false" />
		<input id="acquisition_territory_IN" name="acquisition[territory_IN]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="IN" />
		             India</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_GU">
		      <div class="c1">

		          <input id="acquisition_territory_GU" name="acquisition[territory_GU]" type="hidden" value="false" />
		<input id="acquisition_territory_GU" name="acquisition[territory_GU]" onclick="toggleParent(this.checked,this.id, 'OCEANIA')" type="checkbox" value="GU" />
		             Guam</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_DK">
		      <div class="c1">

		          <input id="acquisition_territory_DK" name="acquisition[territory_DK]" type="hidden" value="false" />
		<input id="acquisition_territory_DK" name="acquisition[territory_DK]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="DK" />
		             Denmark</div>
		    </div>
		      </td></tr>
		  <tr class="odd">
		    <td class="territory">    <div id="territory_CD">
		      <div class="c1">

		          <input id="acquisition_territory_CD" name="acquisition[territory_CD]" type="hidden" value="false" />
		<input id="acquisition_territory_CD" name="acquisition[territory_CD]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="CD" />
		             Congo (DR)</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_CA">
		      <div class="c1">

		          <input id="acquisition_territory_CA" name="acquisition[territory_CA]" type="hidden" value="false" />
		<input id="acquisition_territory_CA" name="acquisition[territory_CA]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="CA" />
		             Canada</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_ID">
		      <div class="c1">

		          <input id="acquisition_territory_ID" name="acquisition[territory_ID]" type="hidden" value="false" />
		<input id="acquisition_territory_ID" name="acquisition[territory_ID]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="ID" />
		             Indonesia</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_HM">
		      <div class="c1">

		          <input id="acquisition_territory_HM" name="acquisition[territory_HM]" type="hidden" value="false" />
		<input id="acquisition_territory_HM" name="acquisition[territory_HM]" onclick="toggleParent(this.checked,this.id, 'OCEANIA')" type="checkbox" value="HM" />
		             Heard and McDonald Islands</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_EE">
		      <div class="c1">

		          <input id="acquisition_territory_EE" name="acquisition[territory_EE]" type="hidden" value="false" />
		<input id="acquisition_territory_EE" name="acquisition[territory_EE]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="EE" />
		             Estonia</div>
		    </div>
		      </td></tr>
		  <tr class="even">
		    <td class="territory">    <div id="territory_CI">
		      <div class="c1">

		          <input id="acquisition_territory_CI" name="acquisition[territory_CI]" type="hidden" value="false" />
		<input id="acquisition_territory_CI" name="acquisition[territory_CI]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="CI" />
		             Cote d'Ivoire</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_KY">
		      <div class="c1">

		          <input id="acquisition_territory_KY" name="acquisition[territory_KY]" type="hidden" value="false" />
		<input id="acquisition_territory_KY" name="acquisition[territory_KY]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="KY" />
		             Cayman Islands</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_IR">
		      <div class="c1">

		          <input id="acquisition_territory_IR" name="acquisition[territory_IR]" type="hidden" value="false" />
		<input id="acquisition_territory_IR" name="acquisition[territory_IR]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="IR" />
		             Iran (Islamic Republic of)</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_KI">
		      <div class="c1">

		          <input id="acquisition_territory_KI" name="acquisition[territory_KI]" type="hidden" value="false" />
		<input id="acquisition_territory_KI" name="acquisition[territory_KI]" onclick="toggleParent(this.checked,this.id, 'OCEANIA')" type="checkbox" value="KI" />
		             Kiribati</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_FO">
		      <div class="c1">

		          <input id="acquisition_territory_FO" name="acquisition[territory_FO]" type="hidden" value="false" />
		<input id="acquisition_territory_FO" name="acquisition[territory_FO]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="FO" />
		             Faeroe Islands</div>
		    </div>
		      </td></tr>
		  <tr class="odd">
		    <td class="territory">    <div id="territory_DJ">
		      <div class="c1">

		          <input id="acquisition_territory_DJ" name="acquisition[territory_DJ]" type="hidden" value="false" />
		<input id="acquisition_territory_DJ" name="acquisition[territory_DJ]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="DJ" />
		             Djibouti</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_CL">
		      <div class="c1">

		          <input id="acquisition_territory_CL" name="acquisition[territory_CL]" type="hidden" value="false" />
		<input id="acquisition_territory_CL" name="acquisition[territory_CL]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="CL" />
		             Chile</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_IQ">
		      <div class="c1">

		          <input id="acquisition_territory_IQ" name="acquisition[territory_IQ]" type="hidden" value="false" />
		<input id="acquisition_territory_IQ" name="acquisition[territory_IQ]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="IQ" />
		             Iraq</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_MH">
		      <div class="c1">

		          <input id="acquisition_territory_MH" name="acquisition[territory_MH]" type="hidden" value="false" />
		<input id="acquisition_territory_MH" name="acquisition[territory_MH]" onclick="toggleParent(this.checked,this.id, 'OCEANIA')" type="checkbox" value="MH" />
		             Marshall Islands</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_FI">
		      <div class="c1">

		          <input id="acquisition_territory_FI" name="acquisition[territory_FI]" type="hidden" value="false" />
		<input id="acquisition_territory_FI" name="acquisition[territory_FI]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="FI" />
		             Finland</div>
		    </div>
		      </td></tr>
		  <tr class="even">
		    <td class="territory">    <div id="territory_EG">
		      <div class="c1">

		          <input id="acquisition_territory_EG" name="acquisition[territory_EG]" type="hidden" value="false" />
		<input id="acquisition_territory_EG" name="acquisition[territory_EG]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="EG" />
		             Egypt</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_CO">
		      <div class="c1">

		          <input id="acquisition_territory_CO" name="acquisition[territory_CO]" type="hidden" value="false" />
		<input id="acquisition_territory_CO" name="acquisition[territory_CO]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="CO" />
		             Colombia</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_IL">
		      <div class="c1">

		          <input id="acquisition_territory_IL" name="acquisition[territory_IL]" type="hidden" value="false" />
		<input id="acquisition_territory_IL" name="acquisition[territory_IL]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="IL" />
		             Israel</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_FM">
		      <div class="c1">

		          <input id="acquisition_territory_FM" name="acquisition[territory_FM]" type="hidden" value="false" />
		<input id="acquisition_territory_FM" name="acquisition[territory_FM]" onclick="toggleParent(this.checked,this.id, 'OCEANIA')" type="checkbox" value="FM" />
		             Micronesia</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_FR">
		      <div class="c1">

		          <input id="acquisition_territory_FR" name="acquisition[territory_FR]" type="hidden" value="false" />
		<input id="acquisition_territory_FR" name="acquisition[territory_FR]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="FR" />
		             France</div>
		    </div>
		      </td></tr>
		  <tr class="odd">
		    <td class="territory">    <div id="territory_GQ">
		      <div class="c1">

		          <input id="acquisition_territory_GQ" name="acquisition[territory_GQ]" type="hidden" value="false" />
		<input id="acquisition_territory_GQ" name="acquisition[territory_GQ]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="GQ" />
		             Equatorial Guinea</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_CR">
		      <div class="c1">

		          <input id="acquisition_territory_CR" name="acquisition[territory_CR]" type="hidden" value="false" />
		<input id="acquisition_territory_CR" name="acquisition[territory_CR]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="CR" />
		             Costa Rica</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_JP">
		      <div class="c1">

		          <input id="acquisition_territory_JP" name="acquisition[territory_JP]" type="hidden" value="false" />
		<input id="acquisition_territory_JP" name="acquisition[territory_JP]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="JP" />
		             Japan</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_NR">
		      <div class="c1">

		          <input id="acquisition_territory_NR" name="acquisition[territory_NR]" type="hidden" value="false" />
		<input id="acquisition_territory_NR" name="acquisition[territory_NR]" onclick="toggleParent(this.checked,this.id, 'OCEANIA')" type="checkbox" value="NR" />
		             Nauru</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_DE">
		      <div class="c1">

		          <input id="acquisition_territory_DE" name="acquisition[territory_DE]" type="hidden" value="false" />
		<input id="acquisition_territory_DE" name="acquisition[territory_DE]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="DE" />
		             Germany</div>
		    </div>
		      </td></tr>
		  <tr class="even">
		    <td class="territory">    <div id="territory_ER">
		      <div class="c1">

		          <input id="acquisition_territory_ER" name="acquisition[territory_ER]" type="hidden" value="false" />
		<input id="acquisition_territory_ER" name="acquisition[territory_ER]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="ER" />
		             Eritrea</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_CU">
		      <div class="c1">

		          <input id="acquisition_territory_CU" name="acquisition[territory_CU]" type="hidden" value="false" />
		<input id="acquisition_territory_CU" name="acquisition[territory_CU]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="CU" />
		             Cuba</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_JO">
		      <div class="c1">

		          <input id="acquisition_territory_JO" name="acquisition[territory_JO]" type="hidden" value="false" />
		<input id="acquisition_territory_JO" name="acquisition[territory_JO]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="JO" />
		             Jordan</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_NC">
		      <div class="c1">

		          <input id="acquisition_territory_NC" name="acquisition[territory_NC]" type="hidden" value="false" />
		<input id="acquisition_territory_NC" name="acquisition[territory_NC]" onclick="toggleParent(this.checked,this.id, 'OCEANIA')" type="checkbox" value="NC" />
		             New Caledonia</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_GI">
		      <div class="c1">

		          <input id="acquisition_territory_GI" name="acquisition[territory_GI]" type="hidden" value="false" />
		<input id="acquisition_territory_GI" name="acquisition[territory_GI]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="GI" />
		             Gibraltar</div>
		    </div>
		      </td></tr>
		  <tr class="odd">
		    <td class="territory">    <div id="territory_ET">
		      <div class="c1">

		          <input id="acquisition_territory_ET" name="acquisition[territory_ET]" type="hidden" value="false" />
		<input id="acquisition_territory_ET" name="acquisition[territory_ET]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="ET" />
		             Ethiopia</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_CW">
		      <div class="c1">

		          <input id="acquisition_territory_CW" name="acquisition[territory_CW]" type="hidden" value="false" />
		<input id="acquisition_territory_CW" name="acquisition[territory_CW]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="CW" />
		             Curacao</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_KZ">
		      <div class="c1">

		          <input id="acquisition_territory_KZ" name="acquisition[territory_KZ]" type="hidden" value="false" />
		<input id="acquisition_territory_KZ" name="acquisition[territory_KZ]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="KZ" />
		             Kazakhstan</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_NZ">
		      <div class="c1">

		          <input id="acquisition_territory_NZ" name="acquisition[territory_NZ]" type="hidden" value="false" />
		<input id="acquisition_territory_NZ" name="acquisition[territory_NZ]" onclick="toggleParent(this.checked,this.id, 'OCEANIA')" type="checkbox" value="NZ" />
		             New Zealand</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_GR">
		      <div class="c1">

		          <input id="acquisition_territory_GR" name="acquisition[territory_GR]" type="hidden" value="false" />
		<input id="acquisition_territory_GR" name="acquisition[territory_GR]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="GR" />
		             Greece</div>
		    </div>
		      </td></tr>
		  <tr class="even">
		    <td class="territory">    <div id="territory_GA">
		      <div class="c1">

		          <input id="acquisition_territory_GA" name="acquisition[territory_GA]" type="hidden" value="false" />
		<input id="acquisition_territory_GA" name="acquisition[territory_GA]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="GA" />
		             Gabon</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_DM">
		      <div class="c1">

		          <input id="acquisition_territory_DM" name="acquisition[territory_DM]" type="hidden" value="false" />
		<input id="acquisition_territory_DM" name="acquisition[territory_DM]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="DM" />
		             Dominica</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_KP">
		      <div class="c1">

		          <input id="acquisition_territory_KP" name="acquisition[territory_KP]" type="hidden" value="false" />
		<input id="acquisition_territory_KP" name="acquisition[territory_KP]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="KP" />
		             Korea (DPRK)</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_NU">
		      <div class="c1">

		          <input id="acquisition_territory_NU" name="acquisition[territory_NU]" type="hidden" value="false" />
		<input id="acquisition_territory_NU" name="acquisition[territory_NU]" onclick="toggleParent(this.checked,this.id, 'OCEANIA')" type="checkbox" value="NU" />
		             Niue</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_GG">
		      <div class="c1">

		          <input id="acquisition_territory_GG" name="acquisition[territory_GG]" type="hidden" value="false" />
		<input id="acquisition_territory_GG" name="acquisition[territory_GG]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="GG" />
		             Guernsey</div>
		    </div>
		      </td></tr>
		  <tr class="odd">
		    <td class="territory">    <div id="territory_GM">
		      <div class="c1">

		          <input id="acquisition_territory_GM" name="acquisition[territory_GM]" type="hidden" value="false" />
		<input id="acquisition_territory_GM" name="acquisition[territory_GM]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="GM" />
		             Gambia</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_DO">
		      <div class="c1">

		          <input id="acquisition_territory_DO" name="acquisition[territory_DO]" type="hidden" value="false" />
		<input id="acquisition_territory_DO" name="acquisition[territory_DO]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="DO" />
		             Dominican Republic</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_KW">
		      <div class="c1">

		          <input id="acquisition_territory_KW" name="acquisition[territory_KW]" type="hidden" value="false" />
		<input id="acquisition_territory_KW" name="acquisition[territory_KW]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="KW" />
		             Kuwait</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_NF">
		      <div class="c1">

		          <input id="acquisition_territory_NF" name="acquisition[territory_NF]" type="hidden" value="false" />
		<input id="acquisition_territory_NF" name="acquisition[territory_NF]" onclick="toggleParent(this.checked,this.id, 'OCEANIA')" type="checkbox" value="NF" />
		             Norfolk Island</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_VA">
		      <div class="c1">

		          <input id="acquisition_territory_VA" name="acquisition[territory_VA]" type="hidden" value="false" />
		<input id="acquisition_territory_VA" name="acquisition[territory_VA]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="VA" />
		             Holy See</div>
		    </div>
		      </td></tr>
		  <tr class="even">
		    <td class="territory">    <div id="territory_GH">
		      <div class="c1">

		          <input id="acquisition_territory_GH" name="acquisition[territory_GH]" type="hidden" value="false" />
		<input id="acquisition_territory_GH" name="acquisition[territory_GH]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="GH" />
		             Ghana</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_EC">
		      <div class="c1">

		          <input id="acquisition_territory_EC" name="acquisition[territory_EC]" type="hidden" value="false" />
		<input id="acquisition_territory_EC" name="acquisition[territory_EC]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="EC" />
		             Ecuador</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_KG">
		      <div class="c1">

		          <input id="acquisition_territory_KG" name="acquisition[territory_KG]" type="hidden" value="false" />
		<input id="acquisition_territory_KG" name="acquisition[territory_KG]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="KG" />
		             Kyrgyzstan</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_MP">
		      <div class="c1">

		          <input id="acquisition_territory_MP" name="acquisition[territory_MP]" type="hidden" value="false" />
		<input id="acquisition_territory_MP" name="acquisition[territory_MP]" onclick="toggleParent(this.checked,this.id, 'OCEANIA')" type="checkbox" value="MP" />
		             Northern Mariana Islands</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_HU">
		      <div class="c1">

		          <input id="acquisition_territory_HU" name="acquisition[territory_HU]" type="hidden" value="false" />
		<input id="acquisition_territory_HU" name="acquisition[territory_HU]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="HU" />
		             Hungary</div>
		    </div>
		      </td></tr>
		  <tr class="odd">
		    <td class="territory">    <div id="territory_GN">
		      <div class="c1">

		          <input id="acquisition_territory_GN" name="acquisition[territory_GN]" type="hidden" value="false" />
		<input id="acquisition_territory_GN" name="acquisition[territory_GN]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="GN" />
		             Guinea</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_SV">
		      <div class="c1">

		          <input id="acquisition_territory_SV" name="acquisition[territory_SV]" type="hidden" value="false" />
		<input id="acquisition_territory_SV" name="acquisition[territory_SV]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="SV" />
		             El Salvador</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_LA">
		      <div class="c1">

		          <input id="acquisition_territory_LA" name="acquisition[territory_LA]" type="hidden" value="false" />
		<input id="acquisition_territory_LA" name="acquisition[territory_LA]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="LA" />
		             Laos (PDR)</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_PW">
		      <div class="c1">

		          <input id="acquisition_territory_PW" name="acquisition[territory_PW]" type="hidden" value="false" />
		<input id="acquisition_territory_PW" name="acquisition[territory_PW]" onclick="toggleParent(this.checked,this.id, 'OCEANIA')" type="checkbox" value="PW" />
		             Palau</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_IS">
		      <div class="c1">

		          <input id="acquisition_territory_IS" name="acquisition[territory_IS]" type="hidden" value="false" />
		<input id="acquisition_territory_IS" name="acquisition[territory_IS]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="IS" />
		             Iceland</div>
		    </div>
		      </td></tr>
		  <tr class="even">
		    <td class="territory">    <div id="territory_GW">
		      <div class="c1">

		          <input id="acquisition_territory_GW" name="acquisition[territory_GW]" type="hidden" value="false" />
		<input id="acquisition_territory_GW" name="acquisition[territory_GW]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="GW" />
		             Guinea-Bissau</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_FK">
		      <div class="c1">

		          <input id="acquisition_territory_FK" name="acquisition[territory_FK]" type="hidden" value="false" />
		<input id="acquisition_territory_FK" name="acquisition[territory_FK]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="FK" />
		             Falkland Islands (Malvinas)</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_LB">
		      <div class="c1">

		          <input id="acquisition_territory_LB" name="acquisition[territory_LB]" type="hidden" value="false" />
		<input id="acquisition_territory_LB" name="acquisition[territory_LB]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="LB" />
		             Lebanon</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_PG">
		      <div class="c1">

		          <input id="acquisition_territory_PG" name="acquisition[territory_PG]" type="hidden" value="false" />
		<input id="acquisition_territory_PG" name="acquisition[territory_PG]" onclick="toggleParent(this.checked,this.id, 'OCEANIA')" type="checkbox" value="PG" />
		             Papua New Guinea</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_IE">
		      <div class="c1">

		          <input id="acquisition_territory_IE" name="acquisition[territory_IE]" type="hidden" value="false" />
		<input id="acquisition_territory_IE" name="acquisition[territory_IE]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="IE" />
		             Ireland</div>
		    </div>
		      </td></tr>
		  <tr class="odd">
		    <td class="territory">    <div id="territory_KE">
		      <div class="c1">

		          <input id="acquisition_territory_KE" name="acquisition[territory_KE]" type="hidden" value="false" />
		<input id="acquisition_territory_KE" name="acquisition[territory_KE]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="KE" />
		             Kenya</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_GF">
		      <div class="c1">

		          <input id="acquisition_territory_GF" name="acquisition[territory_GF]" type="hidden" value="false" />
		<input id="acquisition_territory_GF" name="acquisition[territory_GF]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="GF" />
		             French Guiana</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_MO">
		      <div class="c1">

		          <input id="acquisition_territory_MO" name="acquisition[territory_MO]" type="hidden" value="false" />
		<input id="acquisition_territory_MO" name="acquisition[territory_MO]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="MO" />
		             Macao</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_PN">
		      <div class="c1">

		          <input id="acquisition_territory_PN" name="acquisition[territory_PN]" type="hidden" value="false" />
		<input id="acquisition_territory_PN" name="acquisition[territory_PN]" onclick="toggleParent(this.checked,this.id, 'OCEANIA')" type="checkbox" value="PN" />
		             Pitcairn</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_IM">
		      <div class="c1">

		          <input id="acquisition_territory_IM" name="acquisition[territory_IM]" type="hidden" value="false" />
		<input id="acquisition_territory_IM" name="acquisition[territory_IM]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="IM" />
		             Isle Of Man</div>
		    </div>
		      </td></tr>
		  <tr class="even">
		    <td class="territory">    <div id="territory_LS">
		      <div class="c1">

		          <input id="acquisition_territory_LS" name="acquisition[territory_LS]" type="hidden" value="false" />
		<input id="acquisition_territory_LS" name="acquisition[territory_LS]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="LS" />
		             Lesotho</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_GL">
		      <div class="c1">

		          <input id="acquisition_territory_GL" name="acquisition[territory_GL]" type="hidden" value="false" />
		<input id="acquisition_territory_GL" name="acquisition[territory_GL]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="GL" />
		             Greenland</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_MY">
		      <div class="c1">

		          <input id="acquisition_territory_MY" name="acquisition[territory_MY]" type="hidden" value="false" />
		<input id="acquisition_territory_MY" name="acquisition[territory_MY]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="MY" />
		             Malaysia</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_WS">
		      <div class="c1">

		          <input id="acquisition_territory_WS" name="acquisition[territory_WS]" type="hidden" value="false" />
		<input id="acquisition_territory_WS" name="acquisition[territory_WS]" onclick="toggleParent(this.checked,this.id, 'OCEANIA')" type="checkbox" value="WS" />
		             Samoa</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_IT">
		      <div class="c1">

		          <input id="acquisition_territory_IT" name="acquisition[territory_IT]" type="hidden" value="false" />
		<input id="acquisition_territory_IT" name="acquisition[territory_IT]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="IT" />
		             Italy</div>
		    </div>
		      </td></tr>
		  <tr class="odd">
		    <td class="territory">    <div id="territory_LR">
		      <div class="c1">

		          <input id="acquisition_territory_LR" name="acquisition[territory_LR]" type="hidden" value="false" />
		<input id="acquisition_territory_LR" name="acquisition[territory_LR]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="LR" />
		             Liberia</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_GD">
		      <div class="c1">

		          <input id="acquisition_territory_GD" name="acquisition[territory_GD]" type="hidden" value="false" />
		<input id="acquisition_territory_GD" name="acquisition[territory_GD]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="GD" />
		             Grenada</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_MV">
		      <div class="c1">

		          <input id="acquisition_territory_MV" name="acquisition[territory_MV]" type="hidden" value="false" />
		<input id="acquisition_territory_MV" name="acquisition[territory_MV]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="MV" />
		             Maldives</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_SB">
		      <div class="c1">

		          <input id="acquisition_territory_SB" name="acquisition[territory_SB]" type="hidden" value="false" />
		<input id="acquisition_territory_SB" name="acquisition[territory_SB]" onclick="toggleParent(this.checked,this.id, 'OCEANIA')" type="checkbox" value="SB" />
		             Solomon Islands</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_JE">
		      <div class="c1">

		          <input id="acquisition_territory_JE" name="acquisition[territory_JE]" type="hidden" value="false" />
		<input id="acquisition_territory_JE" name="acquisition[territory_JE]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="JE" />
		             Jersey</div>
		    </div>
		      </td></tr>
		  <tr class="even">
		    <td class="territory">    <div id="territory_LY">
		      <div class="c1">

		          <input id="acquisition_territory_LY" name="acquisition[territory_LY]" type="hidden" value="false" />
		<input id="acquisition_territory_LY" name="acquisition[territory_LY]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="LY" />
		             Libyan Arab Jamahiriya</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_GP">
		      <div class="c1">

		          <input id="acquisition_territory_GP" name="acquisition[territory_GP]" type="hidden" value="false" />
		<input id="acquisition_territory_GP" name="acquisition[territory_GP]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="GP" />
		             Guadeloupe</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_MN">
		      <div class="c1">

		          <input id="acquisition_territory_MN" name="acquisition[territory_MN]" type="hidden" value="false" />
		<input id="acquisition_territory_MN" name="acquisition[territory_MN]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="MN" />
		             Mongolia</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_GS">
		      <div class="c1">

		          <input id="acquisition_territory_GS" name="acquisition[territory_GS]" type="hidden" value="false" />
		<input id="acquisition_territory_GS" name="acquisition[territory_GS]" onclick="toggleParent(this.checked,this.id, 'OCEANIA')" type="checkbox" value="GS" />
		             South Georgia and South Sandwich Islands</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_LV">
		      <div class="c1">

		          <input id="acquisition_territory_LV" name="acquisition[territory_LV]" type="hidden" value="false" />
		<input id="acquisition_territory_LV" name="acquisition[territory_LV]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="LV" />
		             Latvia</div>
		    </div>
		      </td></tr>
		  <tr class="odd">
		    <td class="territory">    <div id="territory_MG">
		      <div class="c1">

		          <input id="acquisition_territory_MG" name="acquisition[territory_MG]" type="hidden" value="false" />
		<input id="acquisition_territory_MG" name="acquisition[territory_MG]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="MG" />
		             Madagascar</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_GT">
		      <div class="c1">

		          <input id="acquisition_territory_GT" name="acquisition[territory_GT]" type="hidden" value="false" />
		<input id="acquisition_territory_GT" name="acquisition[territory_GT]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="GT" />
		             Guatemala</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_MM">
		      <div class="c1">

		          <input id="acquisition_territory_MM" name="acquisition[territory_MM]" type="hidden" value="false" />
		<input id="acquisition_territory_MM" name="acquisition[territory_MM]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="MM" />
		             Myanmar</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_TK">
		      <div class="c1">

		          <input id="acquisition_territory_TK" name="acquisition[territory_TK]" type="hidden" value="false" />
		<input id="acquisition_territory_TK" name="acquisition[territory_TK]" onclick="toggleParent(this.checked,this.id, 'OCEANIA')" type="checkbox" value="TK" />
		             Tokelau</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_LI">
		      <div class="c1">

		          <input id="acquisition_territory_LI" name="acquisition[territory_LI]" type="hidden" value="false" />
		<input id="acquisition_territory_LI" name="acquisition[territory_LI]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="LI" />
		             Liechtenstein</div>
		    </div>
		      </td></tr>
		  <tr class="even">
		    <td class="territory">    <div id="territory_MW">
		      <div class="c1">

		          <input id="acquisition_territory_MW" name="acquisition[territory_MW]" type="hidden" value="false" />
		<input id="acquisition_territory_MW" name="acquisition[territory_MW]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="MW" />
		             Malawi</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_GY">
		      <div class="c1">

		          <input id="acquisition_territory_GY" name="acquisition[territory_GY]" type="hidden" value="false" />
		<input id="acquisition_territory_GY" name="acquisition[territory_GY]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="GY" />
		             Guyana</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_NP">
		      <div class="c1">

		          <input id="acquisition_territory_NP" name="acquisition[territory_NP]" type="hidden" value="false" />
		<input id="acquisition_territory_NP" name="acquisition[territory_NP]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="NP" />
		             Nepal</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_TO">
		      <div class="c1">

		          <input id="acquisition_territory_TO" name="acquisition[territory_TO]" type="hidden" value="false" />
		<input id="acquisition_territory_TO" name="acquisition[territory_TO]" onclick="toggleParent(this.checked,this.id, 'OCEANIA')" type="checkbox" value="TO" />
		             Tonga</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_LT">
		      <div class="c1">

		          <input id="acquisition_territory_LT" name="acquisition[territory_LT]" type="hidden" value="false" />
		<input id="acquisition_territory_LT" name="acquisition[territory_LT]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="LT" />
		             Lithuania</div>
		    </div>
		      </td></tr>
		  <tr class="odd">
		    <td class="territory">    <div id="territory_ML">
		      <div class="c1">

		          <input id="acquisition_territory_ML" name="acquisition[territory_ML]" type="hidden" value="false" />
		<input id="acquisition_territory_ML" name="acquisition[territory_ML]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="ML" />
		             Mali</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_HT">
		      <div class="c1">

		          <input id="acquisition_territory_HT" name="acquisition[territory_HT]" type="hidden" value="false" />
		<input id="acquisition_territory_HT" name="acquisition[territory_HT]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="HT" />
		             Haiti</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_PS">
		      <div class="c1">

		          <input id="acquisition_territory_PS" name="acquisition[territory_PS]" type="hidden" value="false" />
		<input id="acquisition_territory_PS" name="acquisition[territory_PS]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="PS" />
		             Occupied Palestinian Territory</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_TV">
		      <div class="c1">

		          <input id="acquisition_territory_TV" name="acquisition[territory_TV]" type="hidden" value="false" />
		<input id="acquisition_territory_TV" name="acquisition[territory_TV]" onclick="toggleParent(this.checked,this.id, 'OCEANIA')" type="checkbox" value="TV" />
		             Tuvalu</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_LU">
		      <div class="c1">

		          <input id="acquisition_territory_LU" name="acquisition[territory_LU]" type="hidden" value="false" />
		<input id="acquisition_territory_LU" name="acquisition[territory_LU]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="LU" />
		             Luxembourg</div>
		    </div>
		      </td></tr>
		  <tr class="even">
		    <td class="territory">    <div id="territory_MR">
		      <div class="c1">

		          <input id="acquisition_territory_MR" name="acquisition[territory_MR]" type="hidden" value="false" />
		<input id="acquisition_territory_MR" name="acquisition[territory_MR]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="MR" />
		             Mauritania</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_HN">
		      <div class="c1">

		          <input id="acquisition_territory_HN" name="acquisition[territory_HN]" type="hidden" value="false" />
		<input id="acquisition_territory_HN" name="acquisition[territory_HN]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="HN" />
		             Honduras</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_OM">
		      <div class="c1">

		          <input id="acquisition_territory_OM" name="acquisition[territory_OM]" type="hidden" value="false" />
		<input id="acquisition_territory_OM" name="acquisition[territory_OM]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="OM" />
		             Oman</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_UM">
		      <div class="c1">

		          <input id="acquisition_territory_UM" name="acquisition[territory_UM]" type="hidden" value="false" />
		<input id="acquisition_territory_UM" name="acquisition[territory_UM]" onclick="toggleParent(this.checked,this.id, 'OCEANIA')" type="checkbox" value="UM" />
		             US Minor Outlying Islands</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_MK">
		      <div class="c1">

		          <input id="acquisition_territory_MK" name="acquisition[territory_MK]" type="hidden" value="false" />
		<input id="acquisition_territory_MK" name="acquisition[territory_MK]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="MK" />
		             Macedonia (frm Yugoslavia)</div>
		    </div>
		      </td></tr>
		  <tr class="odd">
		    <td class="territory">    <div id="territory_MU">
		      <div class="c1">

		          <input id="acquisition_territory_MU" name="acquisition[territory_MU]" type="hidden" value="false" />
		<input id="acquisition_territory_MU" name="acquisition[territory_MU]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="MU" />
		             Mauritius</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_JM">
		      <div class="c1">

		          <input id="acquisition_territory_JM" name="acquisition[territory_JM]" type="hidden" value="false" />
		<input id="acquisition_territory_JM" name="acquisition[territory_JM]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="JM" />
		             Jamaica</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_PK">
		      <div class="c1">

		          <input id="acquisition_territory_PK" name="acquisition[territory_PK]" type="hidden" value="false" />
		<input id="acquisition_territory_PK" name="acquisition[territory_PK]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="PK" />
		             Pakistan</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_VU">
		      <div class="c1">

		          <input id="acquisition_territory_VU" name="acquisition[territory_VU]" type="hidden" value="false" />
		<input id="acquisition_territory_VU" name="acquisition[territory_VU]" onclick="toggleParent(this.checked,this.id, 'OCEANIA')" type="checkbox" value="VU" />
		             Vanuatu</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_MT">
		      <div class="c1">

		          <input id="acquisition_territory_MT" name="acquisition[territory_MT]" type="hidden" value="false" />
		<input id="acquisition_territory_MT" name="acquisition[territory_MT]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="MT" />
		             Malta</div>
		    </div>
		      </td></tr>
		  <tr class="even">
		    <td class="territory">    <div id="territory_YT">
		      <div class="c1">

		          <input id="acquisition_territory_YT" name="acquisition[territory_YT]" type="hidden" value="false" />
		<input id="acquisition_territory_YT" name="acquisition[territory_YT]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="YT" />
		             Mayotte</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_MQ">
		      <div class="c1">

		          <input id="acquisition_territory_MQ" name="acquisition[territory_MQ]" type="hidden" value="false" />
		<input id="acquisition_territory_MQ" name="acquisition[territory_MQ]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="MQ" />
		             Martinique</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_PH">
		      <div class="c1">

		          <input id="acquisition_territory_PH" name="acquisition[territory_PH]" type="hidden" value="false" />
		<input id="acquisition_territory_PH" name="acquisition[territory_PH]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="PH" />
		             Philippines</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_WF">
		      <div class="c1">

		          <input id="acquisition_territory_WF" name="acquisition[territory_WF]" type="hidden" value="false" />
		<input id="acquisition_territory_WF" name="acquisition[territory_WF]" onclick="toggleParent(this.checked,this.id, 'OCEANIA')" type="checkbox" value="WF" />
		             Wallis and Futuna Islands</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_MC">
		      <div class="c1">

		          <input id="acquisition_territory_MC" name="acquisition[territory_MC]" type="hidden" value="false" />
		<input id="acquisition_territory_MC" name="acquisition[territory_MC]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="MC" />
		             Monaco</div>
		    </div>
		      </td></tr>
		  <tr class="odd">
		    <td class="territory">    <div id="territory_MA">
		      <div class="c1">

		          <input id="acquisition_territory_MA" name="acquisition[territory_MA]" type="hidden" value="false" />
		<input id="acquisition_territory_MA" name="acquisition[territory_MA]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="MA" />
		             Morocco</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_MX">
		      <div class="c1">

		          <input id="acquisition_territory_MX" name="acquisition[territory_MX]" type="hidden" value="false" />
		<input id="acquisition_territory_MX" name="acquisition[territory_MX]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="MX" />
		             Mexico</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_QA">
		      <div class="c1">

		          <input id="acquisition_territory_QA" name="acquisition[territory_QA]" type="hidden" value="false" />
		<input id="acquisition_territory_QA" name="acquisition[territory_QA]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="QA" />
		             Qatar</div>
		    </div>
		      </td>  <td class="territory">  </td>  <td class="territory">    <div id="territory_ME">
		      <div class="c1">

		          <input id="acquisition_territory_ME" name="acquisition[territory_ME]" type="hidden" value="false" />
		<input id="acquisition_territory_ME" name="acquisition[territory_ME]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="ME" />
		             Montenegro</div>
		    </div>
		      </td></tr>
		  <tr class="even">
		    <td class="territory">    <div id="territory_MZ">
		      <div class="c1">

		          <input id="acquisition_territory_MZ" name="acquisition[territory_MZ]" type="hidden" value="false" />
		<input id="acquisition_territory_MZ" name="acquisition[territory_MZ]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="MZ" />
		             Mozambique</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_MS">
		      <div class="c1">

		          <input id="acquisition_territory_MS" name="acquisition[territory_MS]" type="hidden" value="false" />
		<input id="acquisition_territory_MS" name="acquisition[territory_MS]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="MS" />
		             Montserrat</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_KR">
		      <div class="c1">

		          <input id="acquisition_territory_KR" name="acquisition[territory_KR]" type="hidden" value="false" />
		<input id="acquisition_territory_KR" name="acquisition[territory_KR]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="KR" />
		             Republic of Korea</div>
		    </div>
		      </td>  <td class="territory">  </td>  <td class="territory">    <div id="territory_NL">
		      <div class="c1">

		          <input id="acquisition_territory_NL" name="acquisition[territory_NL]" type="hidden" value="false" />
		<input id="acquisition_territory_NL" name="acquisition[territory_NL]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="NL" />
		             Netherlands</div>
		    </div>
		      </td></tr>
		  <tr class="odd">
		    <td class="territory">    <div id="territory_NA">
		      <div class="c1">

		          <input id="acquisition_territory_NA" name="acquisition[territory_NA]" type="hidden" value="false" />
		<input id="acquisition_territory_NA" name="acquisition[territory_NA]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="NA" />
		             Namibia</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_NI">
		      <div class="c1">

		          <input id="acquisition_territory_NI" name="acquisition[territory_NI]" type="hidden" value="false" />
		<input id="acquisition_territory_NI" name="acquisition[territory_NI]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="NI" />
		             Nicaragua</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_SA">
		      <div class="c1">

		          <input id="acquisition_territory_SA" name="acquisition[territory_SA]" type="hidden" value="false" />
		<input id="acquisition_territory_SA" name="acquisition[territory_SA]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="SA" />
		             Saudi Arabia</div>
		    </div>
		      </td>  <td class="territory">  </td>  <td class="territory">    <div id="territory_NO">
		      <div class="c1">

		          <input id="acquisition_territory_NO" name="acquisition[territory_NO]" type="hidden" value="false" />
		<input id="acquisition_territory_NO" name="acquisition[territory_NO]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="NO" />
		             Norway</div>
		    </div>
		      </td></tr>
		  <tr class="even">
		    <td class="territory">    <div id="territory_NE">
		      <div class="c1">

		          <input id="acquisition_territory_NE" name="acquisition[territory_NE]" type="hidden" value="false" />
		<input id="acquisition_territory_NE" name="acquisition[territory_NE]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="NE" />
		             Niger</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_PA">
		      <div class="c1">

		          <input id="acquisition_territory_PA" name="acquisition[territory_PA]" type="hidden" value="false" />
		<input id="acquisition_territory_PA" name="acquisition[territory_PA]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="PA" />
		             Panama</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_SG">
		      <div class="c1">

		          <input id="acquisition_territory_SG" name="acquisition[territory_SG]" type="hidden" value="false" />
		<input id="acquisition_territory_SG" name="acquisition[territory_SG]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="SG" />
		             Singapore</div>
		    </div>
		      </td>  <td class="territory">  </td>  <td class="territory">    <div id="territory_PL">
		      <div class="c1">

		          <input id="acquisition_territory_PL" name="acquisition[territory_PL]" type="hidden" value="false" />
		<input id="acquisition_territory_PL" name="acquisition[territory_PL]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="PL" />
		             Poland</div>
		    </div>
		      </td></tr>
		  <tr class="odd">
		    <td class="territory">    <div id="territory_NG">
		      <div class="c1">

		          <input id="acquisition_territory_NG" name="acquisition[territory_NG]" type="hidden" value="false" />
		<input id="acquisition_territory_NG" name="acquisition[territory_NG]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="NG" />
		             Nigeria</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_PY">
		      <div class="c1">

		          <input id="acquisition_territory_PY" name="acquisition[territory_PY]" type="hidden" value="false" />
		<input id="acquisition_territory_PY" name="acquisition[territory_PY]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="PY" />
		             Paraguay</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_LK">
		      <div class="c1">

		          <input id="acquisition_territory_LK" name="acquisition[territory_LK]" type="hidden" value="false" />
		<input id="acquisition_territory_LK" name="acquisition[territory_LK]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="LK" />
		             Sri Lanka</div>
		    </div>
		      </td>  <td class="territory">  </td>  <td class="territory">    <div id="territory_PT">
		      <div class="c1">

		          <input id="acquisition_territory_PT" name="acquisition[territory_PT]" type="hidden" value="false" />
		<input id="acquisition_territory_PT" name="acquisition[territory_PT]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="PT" />
		             Portugal</div>
		    </div>
		      </td></tr>
		  <tr class="even">
		    <td class="territory">    <div id="territory_RE">
		      <div class="c1">

		          <input id="acquisition_territory_RE" name="acquisition[territory_RE]" type="hidden" value="false" />
		<input id="acquisition_territory_RE" name="acquisition[territory_RE]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="RE" />
		             Reunion</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_PE">
		      <div class="c1">

		          <input id="acquisition_territory_PE" name="acquisition[territory_PE]" type="hidden" value="false" />
		<input id="acquisition_territory_PE" name="acquisition[territory_PE]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="PE" />
		             Peru</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_SY">
		      <div class="c1">

		          <input id="acquisition_territory_SY" name="acquisition[territory_SY]" type="hidden" value="false" />
		<input id="acquisition_territory_SY" name="acquisition[territory_SY]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="SY" />
		             Syrian Arab Republic</div>
		    </div>
		      </td>  <td class="territory">  </td>  <td class="territory">    <div id="territory_MD">
		      <div class="c1">

		          <input id="acquisition_territory_MD" name="acquisition[territory_MD]" type="hidden" value="false" />
		<input id="acquisition_territory_MD" name="acquisition[territory_MD]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="MD" />
		             Republic of Moldova</div>
		    </div>
		      </td></tr>
		  <tr class="odd">
		    <td class="territory">    <div id="territory_RW">
		      <div class="c1">

		          <input id="acquisition_territory_RW" name="acquisition[territory_RW]" type="hidden" value="false" />
		<input id="acquisition_territory_RW" name="acquisition[territory_RW]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="RW" />
		             Rwanda</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_PR">
		      <div class="c1">

		          <input id="acquisition_territory_PR" name="acquisition[territory_PR]" type="hidden" value="false" />
		<input id="acquisition_territory_PR" name="acquisition[territory_PR]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="PR" />
		             Puerto Rico</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_TW">
		      <div class="c1">

		          <input id="acquisition_territory_TW" name="acquisition[territory_TW]" type="hidden" value="false" />
		<input id="acquisition_territory_TW" name="acquisition[territory_TW]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="TW" />
		             Taiwan</div>
		    </div>
		      </td>  <td class="territory">  </td>  <td class="territory">    <div id="territory_RO">
		      <div class="c1">

		          <input id="acquisition_territory_RO" name="acquisition[territory_RO]" type="hidden" value="false" />
		<input id="acquisition_territory_RO" name="acquisition[territory_RO]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="RO" />
		             Romania</div>
		    </div>
		      </td></tr>
		  <tr class="even">
		    <td class="territory">    <div id="territory_SH">
		      <div class="c1">

		          <input id="acquisition_territory_SH" name="acquisition[territory_SH]" type="hidden" value="false" />
		<input id="acquisition_territory_SH" name="acquisition[territory_SH]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="SH" />
		             Saint Helena</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_BL">
		      <div class="c1">

		          <input id="acquisition_territory_BL" name="acquisition[territory_BL]" type="hidden" value="false" />
		<input id="acquisition_territory_BL" name="acquisition[territory_BL]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="BL" />
		             Saint Barthelemy</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_TJ">
		      <div class="c1">

		          <input id="acquisition_territory_TJ" name="acquisition[territory_TJ]" type="hidden" value="false" />
		<input id="acquisition_territory_TJ" name="acquisition[territory_TJ]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="TJ" />
		             Tajikistan</div>
		    </div>
		      </td>  <td class="territory">  </td>  <td class="territory">    <div id="territory_RU">
		      <div class="c1">

		          <input id="acquisition_territory_RU" name="acquisition[territory_RU]" type="hidden" value="false" />
		<input id="acquisition_territory_RU" name="acquisition[territory_RU]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="RU" />
		             Russian Federation</div>
		    </div>
		      </td></tr>
		  <tr class="odd">
		    <td class="territory">    <div id="territory_ST">
		      <div class="c1">

		          <input id="acquisition_territory_ST" name="acquisition[territory_ST]" type="hidden" value="false" />
		<input id="acquisition_territory_ST" name="acquisition[territory_ST]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="ST" />
		             Sao Tome and Principe</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_KN">
		      <div class="c1">

		          <input id="acquisition_territory_KN" name="acquisition[territory_KN]" type="hidden" value="false" />
		<input id="acquisition_territory_KN" name="acquisition[territory_KN]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="KN" />
		             Saint Kitts and Nevis</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_TH">
		      <div class="c1">

		          <input id="acquisition_territory_TH" name="acquisition[territory_TH]" type="hidden" value="false" />
		<input id="acquisition_territory_TH" name="acquisition[territory_TH]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="TH" />
		             Thailand</div>
		    </div>
		      </td>  <td class="territory">  </td>  <td class="territory">    <div id="territory_SM">
		      <div class="c1">

		          <input id="acquisition_territory_SM" name="acquisition[territory_SM]" type="hidden" value="false" />
		<input id="acquisition_territory_SM" name="acquisition[territory_SM]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="SM" />
		             San Marino</div>
		    </div>
		      </td></tr>
		  <tr class="even">
		    <td class="territory">    <div id="territory_SN">
		      <div class="c1">

		          <input id="acquisition_territory_SN" name="acquisition[territory_SN]" type="hidden" value="false" />
		<input id="acquisition_territory_SN" name="acquisition[territory_SN]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="SN" />
		             Senegal</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_LC">
		      <div class="c1">

		          <input id="acquisition_territory_LC" name="acquisition[territory_LC]" type="hidden" value="false" />
		<input id="acquisition_territory_LC" name="acquisition[territory_LC]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="LC" />
		             Saint Lucia</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_TL">
		      <div class="c1">

		          <input id="acquisition_territory_TL" name="acquisition[territory_TL]" type="hidden" value="false" />
		<input id="acquisition_territory_TL" name="acquisition[territory_TL]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="TL" />
		             Timor-Leste</div>
		    </div>
		      </td>  <td class="territory">  </td>  <td class="territory">    <div id="territory_RS">
		      <div class="c1">

		          <input id="acquisition_territory_RS" name="acquisition[territory_RS]" type="hidden" value="false" />
		<input id="acquisition_territory_RS" name="acquisition[territory_RS]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="RS" />
		             Serbia</div>
		    </div>
		      </td></tr>
		  <tr class="odd">
		    <td class="territory">    <div id="territory_SC">
		      <div class="c1">

		          <input id="acquisition_territory_SC" name="acquisition[territory_SC]" type="hidden" value="false" />
		<input id="acquisition_territory_SC" name="acquisition[territory_SC]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="SC" />
		             Seychelles</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_MF">
		      <div class="c1">

		          <input id="acquisition_territory_MF" name="acquisition[territory_MF]" type="hidden" value="false" />
		<input id="acquisition_territory_MF" name="acquisition[territory_MF]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="MF" />
		             Saint Martin</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_TR">
		      <div class="c1">

		          <input id="acquisition_territory_TR" name="acquisition[territory_TR]" type="hidden" value="false" />
		<input id="acquisition_territory_TR" name="acquisition[territory_TR]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="TR" />
		             Turkey</div>
		    </div>
		      </td>  <td class="territory">  </td>  <td class="territory">    <div id="territory_SK">
		      <div class="c1">

		          <input id="acquisition_territory_SK" name="acquisition[territory_SK]" type="hidden" value="false" />
		<input id="acquisition_territory_SK" name="acquisition[territory_SK]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="SK" />
		             Slovakia</div>
		    </div>
		      </td></tr>
		  <tr class="even">
		    <td class="territory">    <div id="territory_SL">
		      <div class="c1">

		          <input id="acquisition_territory_SL" name="acquisition[territory_SL]" type="hidden" value="false" />
		<input id="acquisition_territory_SL" name="acquisition[territory_SL]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="SL" />
		             Sierra Leone</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_PM">
		      <div class="c1">

		          <input id="acquisition_territory_PM" name="acquisition[territory_PM]" type="hidden" value="false" />
		<input id="acquisition_territory_PM" name="acquisition[territory_PM]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="PM" />
		             Saint Pierre and Miquelon</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_TM">
		      <div class="c1">

		          <input id="acquisition_territory_TM" name="acquisition[territory_TM]" type="hidden" value="false" />
		<input id="acquisition_territory_TM" name="acquisition[territory_TM]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="TM" />
		             Turkmenistan</div>
		    </div>
		      </td>  <td class="territory">  </td>  <td class="territory">    <div id="territory_SI">
		      <div class="c1">

		          <input id="acquisition_territory_SI" name="acquisition[territory_SI]" type="hidden" value="false" />
		<input id="acquisition_territory_SI" name="acquisition[territory_SI]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="SI" />
		             Slovenia</div>
		    </div>
		      </td></tr>
		  <tr class="odd">
		    <td class="territory">    <div id="territory_SO">
		      <div class="c1">

		          <input id="acquisition_territory_SO" name="acquisition[territory_SO]" type="hidden" value="false" />
		<input id="acquisition_territory_SO" name="acquisition[territory_SO]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="SO" />
		             Somalia</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_VC">
		      <div class="c1">

		          <input id="acquisition_territory_VC" name="acquisition[territory_VC]" type="hidden" value="false" />
		<input id="acquisition_territory_VC" name="acquisition[territory_VC]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="VC" />
		             Saint Vincent and the Grenadines</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_AE">
		      <div class="c1">

		          <input id="acquisition_territory_AE" name="acquisition[territory_AE]" type="hidden" value="false" />
		<input id="acquisition_territory_AE" name="acquisition[territory_AE]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="AE" />
		             United Arab Emirates</div>
		    </div>
		      </td>  <td class="territory">  </td>  <td class="territory">    <div id="territory_ES">
		      <div class="c1">

		          <input id="acquisition_territory_ES" name="acquisition[territory_ES]" type="hidden" value="false" />
		<input id="acquisition_territory_ES" name="acquisition[territory_ES]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="ES" />
		             Spain</div>
		    </div>
		      </td></tr>
		  <tr class="even">
		    <td class="territory">    <div id="territory_ZA">
		      <div class="c1">

		          <input id="acquisition_territory_ZA" name="acquisition[territory_ZA]" type="hidden" value="false" />
		<input id="acquisition_territory_ZA" name="acquisition[territory_ZA]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="ZA" />
		             South Africa</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_SX">
		      <div class="c1">

		          <input id="acquisition_territory_SX" name="acquisition[territory_SX]" type="hidden" value="false" />
		<input id="acquisition_territory_SX" name="acquisition[territory_SX]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="SX" />
		             Sint Maarten (Dutch part)</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_UZ">
		      <div class="c1">

		          <input id="acquisition_territory_UZ" name="acquisition[territory_UZ]" type="hidden" value="false" />
		<input id="acquisition_territory_UZ" name="acquisition[territory_UZ]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="UZ" />
		             Uzbekistan</div>
		    </div>
		      </td>  <td class="territory">  </td>  <td class="territory">    <div id="territory_SJ">
		      <div class="c1">

		          <input id="acquisition_territory_SJ" name="acquisition[territory_SJ]" type="hidden" value="false" />
		<input id="acquisition_territory_SJ" name="acquisition[territory_SJ]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="SJ" />
		             Svalbard and Jan Mayen Islands</div>
		    </div>
		      </td></tr>
		  <tr class="odd">
		    <td class="territory">    <div id="territory_SS">
		      <div class="c1">

		          <input id="acquisition_territory_SS" name="acquisition[territory_SS]" type="hidden" value="false" />
		<input id="acquisition_territory_SS" name="acquisition[territory_SS]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="SS" />
		             South Sudan</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_SR">
		      <div class="c1">

		          <input id="acquisition_territory_SR" name="acquisition[territory_SR]" type="hidden" value="false" />
		<input id="acquisition_territory_SR" name="acquisition[territory_SR]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="SR" />
		             Suriname</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_VN">
		      <div class="c1">

		          <input id="acquisition_territory_VN" name="acquisition[territory_VN]" type="hidden" value="false" />
		<input id="acquisition_territory_VN" name="acquisition[territory_VN]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="VN" />
		             Viet Nam</div>
		    </div>
		      </td>  <td class="territory">  </td>  <td class="territory">    <div id="territory_SE">
		      <div class="c1">

		          <input id="acquisition_territory_SE" name="acquisition[territory_SE]" type="hidden" value="false" />
		<input id="acquisition_territory_SE" name="acquisition[territory_SE]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="SE" />
		             Sweden</div>
		    </div>
		      </td></tr>
		  <tr class="even">
		    <td class="territory">    <div id="territory_SD">
		      <div class="c1">

		          <input id="acquisition_territory_SD" name="acquisition[territory_SD]" type="hidden" value="false" />
		<input id="acquisition_territory_SD" name="acquisition[territory_SD]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="SD" />
		             Sudan</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_TT">
		      <div class="c1">

		          <input id="acquisition_territory_TT" name="acquisition[territory_TT]" type="hidden" value="false" />
		<input id="acquisition_territory_TT" name="acquisition[territory_TT]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="TT" />
		             Trinidad and Tobago</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_YE">
		      <div class="c1">

		          <input id="acquisition_territory_YE" name="acquisition[territory_YE]" type="hidden" value="false" />
		<input id="acquisition_territory_YE" name="acquisition[territory_YE]" onclick="toggleParent(this.checked,this.id, 'ASIA')" type="checkbox" value="YE" />
		             Yemen</div>
		    </div>
		      </td>  <td class="territory">  </td>  <td class="territory">    <div id="territory_CH">
		      <div class="c1">

		          <input id="acquisition_territory_CH" name="acquisition[territory_CH]" type="hidden" value="false" />
		<input id="acquisition_territory_CH" name="acquisition[territory_CH]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="CH" />
		             Switzerland</div>
		    </div>
		      </td></tr>
		  <tr class="odd">
		    <td class="territory">    <div id="territory_SZ">
		      <div class="c1">

		          <input id="acquisition_territory_SZ" name="acquisition[territory_SZ]" type="hidden" value="false" />
		<input id="acquisition_territory_SZ" name="acquisition[territory_SZ]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="SZ" />
		             Swaziland</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_TC">
		      <div class="c1">

		          <input id="acquisition_territory_TC" name="acquisition[territory_TC]" type="hidden" value="false" />
		<input id="acquisition_territory_TC" name="acquisition[territory_TC]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="TC" />
		             Turks and Caicos Islands</div>
		    </div>
		      </td>  <td class="territory">  </td>  <td class="territory">  </td>  <td class="territory">    <div id="territory_UA">
		      <div class="c1">

		          <input id="acquisition_territory_UA" name="acquisition[territory_UA]" type="hidden" value="false" />
		<input id="acquisition_territory_UA" name="acquisition[territory_UA]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="UA" />
		             Ukraine</div>
		    </div>
		      </td></tr>
		  <tr class="even">
		    <td class="territory">    <div id="territory_TG">
		      <div class="c1">

		          <input id="acquisition_territory_TG" name="acquisition[territory_TG]" type="hidden" value="false" />
		<input id="acquisition_territory_TG" name="acquisition[territory_TG]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="TG" />
		             Togo</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_VI">
		      <div class="c1">

		          <input id="acquisition_territory_VI" name="acquisition[territory_VI]" type="hidden" value="false" />
		<input id="acquisition_territory_VI" name="acquisition[territory_VI]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="VI" />
		             US Virgin Islands</div>
		    </div>
		      </td>  <td class="territory">  </td>  <td class="territory">  </td>  <td class="territory">    <div id="territory_GB">
		      <div class="c1">

		          <input id="acquisition_territory_GB" name="acquisition[territory_GB]" type="hidden" value="false" />
		<input id="acquisition_territory_GB" name="acquisition[territory_GB]" onclick="toggleParent(this.checked,this.id, 'EUROPE')" type="checkbox" value="GB" />
		             United Kingdom of Great Britain</div>
		    </div>
		      </td></tr>
		  <tr class="odd">
		    <td class="territory">    <div id="territory_TN">
		      <div class="c1">

		          <input id="acquisition_territory_TN" name="acquisition[territory_TN]" type="hidden" value="false" />
		<input id="acquisition_territory_TN" name="acquisition[territory_TN]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="TN" />
		             Tunisia</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_US">
		      <div class="c1">

		          <input id="acquisition_territory_US" name="acquisition[territory_US]" type="hidden" value="false" />
		<input id="acquisition_territory_US" name="acquisition[territory_US]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="US" />
		             United States of America</div>
		    </div>
		      </td>  <td class="territory">  </td>  <td class="territory">  </td>  <td class="territory">  </td></tr>
		  <tr class="even">
		    <td class="territory">    <div id="territory_UG">
		      <div class="c1">

		          <input id="acquisition_territory_UG" name="acquisition[territory_UG]" type="hidden" value="false" />
		<input id="acquisition_territory_UG" name="acquisition[territory_UG]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="UG" />
		             Uganda</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_UY">
		      <div class="c1">

		          <input id="acquisition_territory_UY" name="acquisition[territory_UY]" type="hidden" value="false" />
		<input id="acquisition_territory_UY" name="acquisition[territory_UY]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="UY" />
		             Uruguay</div>
		    </div>
		      </td>  <td class="territory">  </td>  <td class="territory">  </td>  <td class="territory">  </td></tr>
		  <tr class="odd">
		    <td class="territory">    <div id="territory_TZ">
		      <div class="c1">

		          <input id="acquisition_territory_TZ" name="acquisition[territory_TZ]" type="hidden" value="false" />
		<input id="acquisition_territory_TZ" name="acquisition[territory_TZ]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="TZ" />
		             United Republic of Tanzania</div>
		    </div>
		      </td>  <td class="territory">    <div id="territory_VE">
		      <div class="c1">

		          <input id="acquisition_territory_VE" name="acquisition[territory_VE]" type="hidden" value="false" />
		<input id="acquisition_territory_VE" name="acquisition[territory_VE]" onclick="toggleParent(this.checked,this.id, 'AMERICAS')" type="checkbox" value="VE" />
		             Venezuela</div>
		    </div>
		      </td>  <td class="territory">  </td>  <td class="territory">  </td>  <td class="territory">  </td></tr>
		  <tr class="even">
		    <td class="territory">    <div id="territory_EH">
		      <div class="c1">

		          <input id="acquisition_territory_EH" name="acquisition[territory_EH]" type="hidden" value="false" />
		<input id="acquisition_territory_EH" name="acquisition[territory_EH]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="EH" />
		             Western Sahara</div>
		    </div>
		      </td>  <td class="territory">  </td>  <td class="territory">  </td>  <td class="territory">  </td>  <td class="territory">  </td></tr>
		  <tr class="odd">
		    <td class="territory">    <div id="territory_ZM">
		      <div class="c1">

		          <input id="acquisition_territory_ZM" name="acquisition[territory_ZM]" type="hidden" value="false" />
		<input id="acquisition_territory_ZM" name="acquisition[territory_ZM]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="ZM" />
		             Zambia</div>
		    </div>
		      </td>  <td class="territory">  </td>  <td class="territory">  </td>  <td class="territory">  </td>  <td class="territory">  </td></tr>
		  <tr class="even">
		    <td class="territory">    <div id="territory_ZW">
		      <div class="c1">

		          <input id="acquisition_territory_ZW" name="acquisition[territory_ZW]" type="hidden" value="false" />
		<input id="acquisition_territory_ZW" name="acquisition[territory_ZW]" onclick="toggleParent(this.checked,this.id, 'AFRICA')" type="checkbox" value="ZW" />
		             Zimbabwe</div>
		    </div>
		      </td>  <td class="territory">  </td>  <td class="territory">  </td>  <td class="territory">  </td>  <td class="territory">  </td></tr>
		<tr class="last_row"><td colspan="5"></td></tr>
		</tbody>
		<tfoot><tr></tr></tfoot>
		</table>
		<span style="float:right;display:none;" class="button bg-color-blue fg-color-white" onclick="save();"><?php echo label("save",$_SESSION['clang'])?></span>
		<br>
</div>
<script>
	$( document ).ready(function() {
		fillCountries();
		$("#territories_tab input[type=checkbox]").each(function() {
		    $(this).wrap('<label class="input-control checkbox"></label>');
		    $('<span class="helper">').insertAfter( $(this) );
		    //$(this).append('<span class="helper">');
		});
	});
	
	//uncheckTerritory('WORLD');
	function save () {
		$.post('process/processWorld.php?albumId=<?php echo $albumId;?>&action=1',{json:JSON.stringify(jsonObj)});
	}
	 function fillCountries(){
		 var countries = '<?php echo $country;?>';
		 $.each(countries.split('<'), function(index,value) {
			 if ( value.replace('>','')=="WORLD") {
				wd=true;
			 }
			 var box = $('#acquisition_territory_' + value.replace('>','') +  '[type="checkbox"]');
			 box.attr('checked',true );
			});

	 }
		    
</script>

            </div>