<?php
	list($period, $id) = split('[=]', $etc);
	
	if ($id <> NULL){
		list($title,$languageId,$artistId,$genreId,$copyright,$releaseDate,$prereleased,$pricing,$upc,$art) = albumInfo($id);
        $appleId = getAdamId($upc);
        if ($appleId == NULL){
//            echo "upc:".$upc."/adam: NULL - No record of Sale";
            $arrow = "";
        }else{
//            echo "upc:".$upc."/adam:".$appleId;
            $arrow = "<span style='float:right;'><a href='?research&id=".$appleId."&".$etc2."'><img src='/images/magglass.png' width='10' length='12'></a></span>";
        }
		$te = $title."&nbsp;&nbsp;<span class=\"label\">".label(contentStatus($id),$_SESSION['clang'])."</span>";
		$form = "<form method='POST' action='?showcontent&$etc2'>";
		$remove = "<span style='float:right;'><a href='?remove_album&id=".$id."'><i class='icon-remove'></i></a></span>";


        if ($art<>NULL){
			$artfile = "src=\"uploads/".$_SESSION['userid']."/".$id."/".$art."\"";
			$artfiletext = label("changeArt",$_SESSION['clang']);
		}else{
			$artfile = "src=\"uploads/album.jpg\"";
			$artfiletext = label("newArt",$_SESSION['clang']);

		}
	}else{

        echo "opppps";
	}
	echo "<div id=\"Main\">";
	//topSectionContent($page,$period,$date);//Creates the second layer menu and top heading 

	?>	

	<?=$form;?>
	<h2><?=$te;?><?=$arrow;?></h2>
	<div id="topbarsub" style="height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;"></div>
	<div class="inner">    
	 	<div class="feature_explanation">
			<h3><?=label("info",$_SESSION['clang']);?></h3>			
			<table style="width: 90%; text-align: left; margin-left: auto; margin-right: auto;" border="0" cellpadding="2" cellspacing="0">
	 		 <tbody>
				<tr>
					<td rowspan= "6" width='10%' valign='top' align="middle">
						<div id="myModal" class="modal hide fade in" style="display: none;color:#000;">
						  <div class="modal-header">
						    <h3><?=$title;?></h3>

						  </div>
						  <div class="modal-body" >
							<img <?=$artfile;?> width="350px">
						  </div>
						  <div class="modal-footer">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
						  </div>
						</div>
						
						<a href="#myModal" data-toggle="modal" data-target="#myModal"><span class='jewelImage'><img <?=$artfile;?> width='120px'></span></a>
					</td>
					<td width='45%'>
						 <div class="input-control text">
					    <input type="text" placeholder="<?=label("title",$_SESSION['clang']);?>" name="title" value="<?=$title;?>" disabled/>
					    </div>
					</td>
					<td width='45%'>
						<div class="input-control select">
					    <select name="languageId" disabled>
						  <option><?=label("lang",$_SESSION['clang']);?></option>
						<? languages($languageId);?>
					    </select>
					    </div>
					</td>
				</tr>
				<tr>
					<td>
						 <div class="input-control text">
					    <input type="text" placeholder="UPC" name="upc" value="<?=$upc;?>" id="upc" maxlength="13" disabled/>
					    </div>
					</td>
					<td>
						<div class="input-control select">
					    <select name="genreId" disabled>
						    <option><?=label("genre",$_SESSION['clang']);?></option>
							<? genres($genreId);?>
					    </select>
					    </div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="input-control text">
					    <input type="text" placeholder="<?=label("copyright",$_SESSION['clang']);?>" name="copyright" value="<?=$copyright;?>" id="copyright" disabled/>
					    </div>
					</td>
					<td>
						<div class="input-control select">
					    <select name="price" disabled>
                        <option value="0"><?=label("free",$_SESSION['clang']);?></option>
                        <option value="" selected><?=label("defaultpricing",$_SESSION['clang']);?></option>
					    <option value="0.99">0.99</option>
					    <option value="1.99">1.99</option>
					    <option value="2.99">2.99</option>
					    <option value="3.99">3.99</option>
					    <option value="4.99">4.99</option>
					    <option value="5.99">5.99</option>
					    <option value="6.99">6.99</option>
					    <option value="7.99">7.99</option>
					    <option value="8.99">8.99</option>
					    <option value="9.99">9.99</option>
					    <option value="10.99">10.99</option>
					    <option value="11.99">11.99</option>
					    <option value="12.99">12.99</option>
					    <option value="13.99">13.99</option>
					    <option value="14.99">14.99</option>
					    <option value="15.99">15.99</option>
					    <option value="16.99">16.99</option>
					    </select>
					    </div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="input-control select">
					    <select name="released" id='released' disabled>
					    <option selected ><?=label("released",$_SESSION['clang']);?></option>
						<?
							if ($prereleased == 1){
								echo "<option value = \"1\" selected>Yes</option>";
								echo "<option value = \"0\">No</option>";
							}else{
								echo "<option value = \"1\">Yes</option>";
								echo "<option value = \"0\" selected>No</option>";
							}
						?>
					    </select>
					    </div>
					</td>
					
					<td>
						 <div class="input-control text">
					    <input type="text" placeholder="<?=label("releasedate",$_SESSION['clang']);?>" name="releasedate" id="releasedate" value="<?=date ("Y-m-d", strtotime($releaseDate));?>" disabled/>
					    </div>
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td>

					</td>
				</tr>
				<tr>
					<td>

					</td>
					<td>
						
					</td>
				</tr>
			</tbody>
			</table>
		</div>
	</div>		
	<div id="topbarsub" style="height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;"></div>
	<?
	if ($id <> NULL){
		getArtistsNoEdit($id);
	}else{?>
		<div class="inner">    
		 	<div class="feature_explanation">
				<h3><?=label("artists",$_SESSION['clang']);?></h3>
				<?=label("noartist",$_SESSION['clang']);?>
			</div>
		</div>
	<?}?>
	<div id="topbarsub" style="height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;"></div>
	<?
	if ($id <> NULL){
		getTrackswPlayerNoEdit($id,$etc2);
	}else{?>
		<div class="inner">    
		 	<div class="feature_explanation">
				<h3><?=label("tracks",$_SESSION['clang']);?></h3>
			</div>
		</div>
	<?}?>
	<div id="topbarsub" style="height: 1px; width: 100%; overflow: hidden; background-color: #ffffff; border-bottom: 1px solid #d5d5d5;"></div>
	<br>
	<div><p align="right">
		<button id="submitFormButton" submit class="button bg-color-blue fg-color-white"><?=label("showcontent",$_SESSION['clang']);?></button></p>
	</div>
	</form>
<?
	echo "</div>";
	echo "<br>";
	callFooterContent();
?>

