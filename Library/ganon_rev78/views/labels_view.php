
<div id=\"Main\"><br>
	<h2><?=label("labels",$_SESSION['clang']);?></h2>
	<br>
	<div class="page-control" data-role="page-control">
	        <!-- Responsive controls -->
	        <span class="menu-pull"></span> 
	        <div class="menu-pull-bar"></div>
	        <!-- Tabs -->
			<ul>
	            <li class="active"><a href="#frame1"><?=label("search",$_SESSION['clang']);?></a></li>
	            <li><a href="#frame2"><?=label("browsebylabel",$_SESSION['clang']);?></a></li>
	            </ul>
	        <div class="frames">
	            <div class="frame active" id="frame1"> 
		            <div class="input-control text">
					    <input type="text" placeholder="<? echo(label("search",$_SESSION['clang'])); ?>"/>
					    <button class="btn-search"></button>
					</div>
				</div>
	            <div class="frame" id="frame2"> 
	            List of labels, click will get you to labelDetail page
				</div>
			</div>
		</div>
   </div>
<?
	callFooterAdmin();

?>