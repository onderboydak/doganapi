<div id=\"Main\"><br>
	<h2><?=label("invites",$_SESSION['clang']);?></h2>
	<br>
		<div class="page-control" data-role="page-control">
		        <!-- Responsive controls -->
		        <span class="menu-pull"></span> 
		        <div class="menu-pull-bar"></div>
		        <!-- Tabs -->
				<ul>
		            <li class="active"><a href="#frame1"><?=label("label",$_SESSION['clang']);?></a></li>
		            <li><a href="#frame2"><?=label("band",$_SESSION['clang']);?></a></li>
		            <li><a href="#frame3"><?=label("artist",$_SESSION['clang']);?></a></li>
		        </ul>
		        <div class="frames">
		            <div class="frame active" id="frame1"> 
						<?invitations(3);?>
					</div>
		            <div class="frame" id="frame2"> 
						<?invitations(2);?>
					</div>
					<div class="frame" id="frame3"> 
						<?invitations(1);?>
					</div>
				</div>
			</div>
	</div>
<?callFooterAdmin();?>