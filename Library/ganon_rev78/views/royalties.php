<?php 
	session_start();
	$searchText = isset($_POST['searchlabel']) ? ltrim($_POST['searchlabel']) : "";
	$month = isset($_POST['monthId']) ? $_POST['monthId'] : '0';
	if ($month=='') {
		$month=0;
	}
?>

<div style="border-bottom: 1px solid #d5d5d5;padding-bottom:10px;">
	<span class="input-add text" >
		<input type="text" id="searchlabel" style="width:50%;" name="searchlabel" placeholder="<?php echo label("label", $_SESSION['clang']);?>" value="<?php echo $searchText;?>"></input>
			<a style='margin-left:-20px;' href='' onclick='javascript:cancelFilter();'><i class="icon-cancel-2"></i></a>
	</span>
		<span class="input-add select" >
		    <select id="invoicePeriod" name="invoicePeriod" style="width:20%;float:right;">
		    <option value='0'><?php echo label("allperiods",$_SESSION['clang']);?></option>
		    <?php 
		    	$query = "select concat(year(invoiceStatuses.scopeDate),' / ',month(invoiceStatuses.scopeDate)),
							concat(year(invoiceStatuses.scopeDate),case when length(month(invoiceStatuses.scopeDate))=1 then concat('0',month(invoiceStatuses.scopeDate)) else month(invoiceStatuses.scopeDate) end)  as m 
							from db_royalty2.invoiceStatuses 
							group by invoiceStatuses.scopeDate ";
		    	$result = execute($query);
		    	while (list($processDate,$allMonths) = mysqli_fetch_array($result)) {
					echo "<option value='".$allMonths."' ".(($allMonths==$month) ? "selected" : "").">".$processDate."</option>";
				}
		    ?>
			</select>
	    </span>
</div>
<br>
<table class="hovered">
	<tr>
		<td>
		</td>
		<td>
		</td>
		<td>
		</td>
		<td>
		</td>
		<td style="text-align: center;">
		      <?php echo label('invoice',$_SESSION["clang"]);?>
		</td>
		<td style="text-align: center;">
		       <?php echo label('paid',$_SESSION["clang"]);?>
		</td>
		<td>
		</td>
	</tr>
	<?php 
		$query = "SELECT invoiceId,user.vendorname,concat(year(invoiceStatuses.scopeDate),' / ',month(invoiceStatuses.scopeDate)) as periot,platform,accountPaid,invoices.accountCurrency as currency,invoiceStatuses.status,invoices.invoice,invoices.paid ,
							date(invoices.scopeDate) as scopeDate ,invoices.userId,db_royalty2.fgetinvoiceDetail(invoiceId)
						FROM db_royalty2.invoices
						left outer join db.user on user.id=invoices.userId
						inner join db_royalty2.invoiceStatuses on invoiceStatuses.invoiceStatusId=invoices.invoiceStatusId 
					where 
						('".$searchText."'='' or user.vendorname='".$searchText."') and 
						('".$month."'='0' or invoices.scopeShortDate='".$month."') 
					order by 
						user.vendorname";
		$result = execute($query);
		$sTRY =0;
		$sEUR =0;
		$sUSD=0;
		while(list($invoiceId,$fullname,$processDate,$platform,$accountPaid,$currency,$status,$invoice,$paid,$scopeDate,$labelUserId,$invoiceDetail)=mysqli_fetch_array($result))
		{
			switch ($currency) {
				case "TRY":
							$sTRY +=$accountPaid;
							break;
				case "EUR":
							$sEUR +=$accountPaid;
							break;
				case "USD":
							$sUSD +=$accountPaid;
							break;
			}
			echo "<tr id='row_".$invoiceId."'>";
			echo "<td style='font-size:small;'>",$fullname,"</td>";
			echo "<td style='font-size:small;'>",$processDate,"</td>";
			//echo "<td><span class='modernlabel modernlabel-info'>",$platform,"</span></td>";
			echo "<td style='text-align:right;font-size:small;'><span original-title='".$invoiceDetail."' id='apid'".$invoiceId." class='accountPaid".$currency."'>",$accountPaid,"</span>
	           <script>
                	$('#apid".$invoiceId."').tipsy({gravity: 'n',html:true})
                </script>
	        </td>";
			echo "<td style='width:5%;'><span class='modernlabel modernlabel-success'>",$currency,"</span></td>";
			echo "<td style='width:8%;text-align:center;'><label title='".label('invoice',$_SESSION['clang'])."' class='input-control checkbox' style='margin-bottom:3px;'><input class='invoice' type='checkbox' ".(($invoice==1) ? " checked " : "").(($status==0) ? "disabled" : "") ." id='invoiveCheck_".$invoiceId."'  name='invoiveCheck_".$invoiceId."' /><span class='helper'></span></label></td>";
			echo "<td style='width:8%;text-align:center;'><label title='".label('paid',$_SESSION['clang'])."' class='input-control checkbox' style='margin-bottom:3px;'><input class='paid' type='checkbox' ".(($paid==1) ? " checked " : "").(($status==0) ? "disabled" : "") ." id='paidCheck_".$invoiceId."' name='paidCheck_".$invoiceId."' /><span class='helper'></span></label></td>";
			echo "<td style='width:8%;'><a href='views/royaltyDetails.php?scopeDate=".$scopeDate."&labelUserId=".$labelUserId."&label=".$fullname."'><i class='icon-download-2'></i></a></td>";
			echo "</tr>";
		}
	?>
	<tr>
		<td>
			<?php echo label("Total",$_SESSION["clang"]);?>
		</td>
		<td colspan="3" style="text-align: right;padding-right:2px;">
			<b><span id="total">
				<?php 
						echo  $sTRY."&nbsp;<span class='modernlabel modernlabel-success'>TRY</span>&nbsp;&nbsp;" ;
						echo  $sEUR."&nbsp;<span class='modernlabel modernlabel-success'>EUR</span>&nbsp;&nbsp;" ;
						echo  $sUSD."&nbsp;<span class='modernlabel modernlabel-success'>USD</span>&nbsp;&nbsp;" ;
				
				?></span></b>
		</td>
	</tr>
</table>
	<span class="jewelImage">
								<img src="<?php echo $picPath;?>" width="64px"  id="pinnedRelease" original-title="<?php echo $pinnedTitle;?>"/>
							</span>


<script type="text/javascript">
var options, a;
jQuery(function(){
   options = { 
		width:300,
   		serviceUrl:'autocomplete.php',
   		onSelect : function(value) {
      					 $("#monthId").val($("#invoicePeriod").val());
						$("#frminvoice").submit();
   					}
   };
   a = $('#searchlabel').autocomplete(options);
});

function round(value, exp) {
	  if (typeof exp === 'undefined' || +exp === 0)
	    return Math.round(value);

	  value = +value;
	  exp  = +exp;

	  if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
	    return NaN;

	  // Shift
	  value = value.toString().split('e');
	  value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));

	  // Shift back
	  value = value.toString().split('e');
	  return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
	}
function cancelFilter()
{
	$("#selectedFrame").val("#frame2");
	$("#searchlabel").text("");
}	
function totalPaid()
{
	var tt=0;
	$(".accountPaidTRY").each(function() {
		 if (!isNaN(parseFloat($(this).text())))  {
			  tt=tt+parseFloat($(this).text());
		 }
	});
	if (tt>0) {
		$("#total").text($("#total").text()+round(tt,2)+" TRY ");
	}

	 tt=0;
	$(".accountPaidEUR").each(function() {
		 if (!isNaN(parseFloat($(this).text())))  {
			  tt=tt+parseFloat($(this).text());
		 }
	});
	if (tt>0) {
		$("#total").text($("#total").text()+", "+round(tt,2)+ " EUR ");
	}

	 tt=0;
		$(".accountPaidUSD").each(function() {
			 if (!isNaN(parseFloat($(this).text())))  {
				  tt=tt+parseFloat($(this).text());
			 }
		});
		if (tt>0) {
			$("#total").text($("#total").text()+", "+round(tt,2)+" USD ");
		}

		if ($("#total").text().substring(0, 1)==',') {
			$("#total").text($("#total").text().substring(1, $("#total").text().length-1));
		};
}

$(document).ready(function() {
$("#invoicePeriod").change(function(){
	 $("#monthId").val($(this).val());
	$("#frminvoice").submit();
	});
});
//totalPaid();

$('input[type=checkbox]').click(function() {
	var objId = $(this).attr("id").split("_")[1];
    if($(this).is(':checked')) {
     	if ($(this).attr("id").indexOf("invoiveCheck")>=0) {
     		$.post("process/processinvoice.php?status=4",{invoiceId:objId,chk:1});
     	} else {
     		$.post("process/processinvoice.php?status=5",{invoiceId:objId,chk:1});
     	}
    } else {
    	if ($(this).attr("id").indexOf("invoiveCheck")>=0) {
     		$.post("process/processinvoice.php?status=4",{invoiceId:objId,chk:0});
     	} else {
     		$.post("process/processinvoice.php?status=5",{invoiceId:objId,chk:0});
     	}
    }
});
</script>
