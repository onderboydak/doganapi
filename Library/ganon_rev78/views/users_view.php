<div id=\"Main\"><br>
	<h2><?=label("users",$_SESSION['clang']);?></h2>
	<br>
	<div class="page-control" data-role="page-control">
	        <!-- Responsive controls -->
	        <span class="menu-pull"></span> 
	        <div class="menu-pull-bar"></div>
	        <!-- Tabs -->
			<ul>
	            <li class="active"><a href="#frame1"><?=label("activeusers",$_SESSION['clang']);?></a></li>
	            <li><a href="#frame2"><?=label("deletedusers",$_SESSION['clang']);?></a></li>
	        </ul>
	        <div class="frames">
	            <div class="frame active" id="frame1"> 
					<?listUsers($etc,1);?>
				</div>
	            <div class="frame" id="frame2"> 
					<?listUsers($etc,0);?>
				</div>
			</div>
		</div>
   </div>

<?callFooterAdmin();?>
