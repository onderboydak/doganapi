<?php

$query = "select * from db_web.slider order by postDate";
$result = execute($query);
$imagePath ="http://www.ctmef.com/assets/img/slider/";


?>
<div id=\"webslider\">
	<br>
	<h2><?=label("webslider",$_SESSION['clang']);?></h2>
	<br>
	<div class="page-control" data-role="page-control">
		<!-- Responsive controls -->
		<span class="menu-pull"></span>
		<div class="menu-pull-bar"></div>
		<table class="hovered" width="95%">
		<tr><td colspan='8' align='right'><a href="?addslide&<?php echo $etc;?>"><span class='modernlabel modernlabel-info'><i class='icon-plus-2' style='font-size: 11px;'></i></span></a></td></tr>
			<?php 
			while($row=mysqli_fetch_array($result)) {
			?>
			<tr id="row_<?php echo $row["id"];?>">
				<td>
					<?php echo $row["title"];?>
				</td>
				<td>
					<?php echo $row["tags"];?>
				</td>
				<td>
					<?php echo dately($row["postDate"]);?>
				</td>
				<td>
					<span id="status_<?php echo  $row["id"];?>" class='modernlabel modernlabel-info'><?php 
						$st = "READY TO GO LIVE";
						switch ($row["status"]) {
							case 0 :
									$st = "READY TO GO LIVE";
									break;
							case 1:
									$st = "LIVE";
									break;
							case 2:
								$st = "ARCHIEVED";
								break;

						}
					echo label($st,$_SESSION['clang']);
					
					?></span>
				</td>
				<td>
					<?php 
						if ($row["imageLink"]!='') {
							?>
								<a href="#myModal<?php echo $row["id"];?>" data-toggle="modal" data-target="#myModal<?php echo $row["id"];?>"><span class='jewelImage'><i class="icon-pictures"></i></a>
								<div id="myModal<?php echo $row["id"];?>" class="modal hide fade in" style="display: none;color:#000;">
									  <div class="modal-header">
									    <h3><?=$row["title"];?></h3>
									  </div>
									  <div class="modal-body" style="text-align:center;" >
										<img style="width:350px;height:350px;" src="<?php 
											$imageFile =$imagePath .$row["imageLink"];
											echo $imageFile;?>">
									  </div>
									  <div class="modal-footer">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
									  </div>
									</div>
							<?php 
						}
					?>
				</td>
				
				<td id="statusIcon_<?php echo $row["id"];?>">
					<?php if ($row["status"]==0) {?>
						<a href="javascript:status(<?php echo $row["id"];?>,1)"><i class="icon-checkmark"></i></a>
					<?php } else {?>
						<a href="javascript:status(<?php echo $row["id"];?>,0)"><i class="icon-cancel-2"></i></a>
					<?php }?>
				</td>
				<td width='5%' align='right'>
					<a href='?addslide&id=<?php echo $row["id"];?>'><i class="icon-pencil"></i></a>
				</td>
				<td width='5%' align='right'>
					<a href='javascript:removePost(<?php echo $row["id"];?>);'><i class="icon-remove"></i></a>
				</td>
			</tr>
			<?php 
			}
			?>
		</table>

	</div>
</div>
<script>
	function status (sliderId,stat) {
		$.post("process/processWebslide.php",{action:4,sliderId:sliderId,status:stat},function(data) {
			var rowStatus = '#status_'+sliderId;
			var statusIcon = '#statusIcon_'+sliderId;
			if (stat==1) {
				$(rowStatus).html("<?php echo label("LIVE",$_SESSION['clang']);?>");
				$(statusIcon).children().remove();
				$(statusIcon).append ("<a href='javascript:status("+sliderId+",0)'><i class='icon-cancel-2'></i></a>");
			} else {
				$(rowStatus).html("<?php echo label("READY TO GO LIVE",$_SESSION['clang']);?>");
				$(statusIcon).children().remove();
				$(statusIcon).append ("<a href='javascript:status("+sliderId+",1)'><i class='icon-checkmark'></i></a>");
			}
			//var rowId = "#row_"+postId;
			//$(rowId).remove();
		});
	}

	function showImage(imageFile) {
		
	}
	function removePost(sliderId) {

		$.post("process/processWebslide.php",{action:3,sliderId:sliderId},function(data) {
			var rowId = "#row_"+sliderId;
			$(rowId).remove();
		});
		
	}

</script>


