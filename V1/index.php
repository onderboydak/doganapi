<?php
/*
    EF CRM API V1
    Start Date : 2018-12-20
    Last Update : 2019-01-28

    ********* FUNCTIONS ***********

    get /accounts/{asID}/{accountID}
    post /account/save/{accountID}
    post /account/delete/{accountID}

    get /transactions/{asID}/{accountID}

    get /users/{userID}
    get /users/login/{userName}/{password} *
    get /users/forgot/{userEmail} *
    post /users/save/{id} *
    get /users/roles/{userID}
    
    get /menus/{userID}

    post /customer/save/{customerID} *
    get /customers/asID/isVendor/{customerID} *

       
    get /members/asID/{employeeID} *
    post /member/save/{employeeID} *
    post /member/delete/{employeeID} *

    post /invoice/save/{invoiceID} body post *
    post /invoice/payment/save/{invoiceID} body post *
    get /invoices/{invoiceID}  
    post /invoice/status/{invoiceID}/{statusID} 

    get /expenses/{expenseID} 
    post /expense/save/{expenseID} *

    post /receipts/import *

    get /products/asID/isVendor/{productID} *
    post /product/save/{productID} *
*/

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

date_default_timezone_set('Europe/Istanbul');
setlocale(LC_ALL, "tr_TR");

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

// standard libraries
require_once dirname(dirname(__FILE__)) . "/Library/slim/autoload.php";
require_once dirname(dirname(__FILE__)) . "/Library/tuupolaBasic/autoload.php";
require_once dirname(dirname(__FILE__)) . "/BL/Tables/users.php";


$config = [
    'settings' => [
        'addContentLengthHeader' => false,
        'displayErrorDetails' => true,
        'logger' => [
            'name' => 'charter App',
            'level' => Monolog\Logger::DEBUG,
            'path' => __DIR__ . '/Logs/app.log'
        ]
    ]
];
$app = new \Slim\App($config);

$app->add(new \Slim\Middleware\HttpBasicAuthentication([
    "secure" => true,
    "realm" => "Protected",
    "authenticator" => function ($arguments) {
                            $user = users::getAuth($arguments["user"],$arguments["password"]);
                            return ($user->ID>0) ? true : false;
                        },
    /*
    "relaxed" => [
        "localhost","192.168.1.51"
    ],
   
    "users" => [
        "root" => "1234"
    ]
    */
]));


//country
$app->get('/country', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/country.php";
    //echo $request->getServerParams()["PHP_AUTH_USER"] ;
        $country= new country();
        $sql ="select * from country order by ID";
        $countryResult = $country->executenonquery($sql,true);        
        $countryResponse = checkNull($country->toJson);
   
    return $response->withStatus(200)
        ->write($countryResponse);
});


// financial accounts (banks+cash)
// cash account olunca accountID=0 olsun yani ozel bir hesap
$app->get('/banks[/{bankID}]', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/banks.php";
    //echo $request->getServerParams()["PHP_AUTH_USER"] ;
        $banks= new banks();
        $bankResult = $banks->getBanks($args["bankID"]);        
        $bankResponse = checkNull($banks->toJson);
   
    return $response->withStatus(200)
        ->write($bankResponse);
});

$app->post('/bank/save/{bankID}', function ($request, $response, $args) {
    
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/banks.php";

    $json = $request->getBody();
    $data = json_decode($json, true);

    $ba = banks::getBankFrombankID($args["bankID"]);

    $ba->bankID=$args["bankID"];
    $ba->name=$data['name'];
    $ba->countryID = $data['countryID'];
    $ba->SWIFT = $data['SWIFT'];
    $ba->phone = $data['phone'];
    $ba->kepEmail = $data['kepEmail'];
    $ba->address=$data['address'];

    $baResponse = $ba->save();
    return $response->withStatus(200)
        ->write($baResponse);
});

$app->delete('/bank/delete/{bankID}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/banks.php";

    $ba = banks::getBankFrombankID($args["bankID"]);
    $usResponse=$ba->delete(1);

    return $response->withStatus(200)
    ->write($usResponse);

});

$app->get('/bankBranches/{bankID}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/bankBranches.php";
    //echo $request->getServerParams()["PHP_AUTH_USER"] ;
        $bankBranches= new bankBranches();
        $bankBranchesResult = $bankBranches->getBankBranches($args["bankID"]);        
        $bankBranchesResponse = checkNull($bankBranches->toJson);
   
    return $response->withStatus(200)
        ->write($bankBranchesResponse);
});


$app->get('/accounts/{asID}/{accountID}[/{isBank}]', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/bankAccounts.php";
    //echo $request->getServerParams()["PHP_AUTH_USER"] ;
        $account= new bankAccounts();
        $accountResult = $account->getBankAccounts($args["asID"],$args["accountID"],$args["isBank"]);        
        $accountResponse = checkNull($account->toJson);
   
    return $response->withStatus(200)
        ->write($accountResponse);
});

$app->get('/accountBalances/{asID}/{accountID}[/{isBank}]', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/bankAccounts.php";
    //echo $request->getServerParams()["PHP_AUTH_USER"] ;
        $account= new bankAccounts();
        $accountResult = $account->getLatestBalances($args["asID"],$args["accountID"],$args["isBank"]);        
        $accountResponse = checkNull($account->toJson);
   
    return $response->withStatus(200)
        ->write($accountResponse);
});

$app->post('/account/save/{accountID}', function ($request, $response, $args) {
    
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/bankAccounts.php";

    $json = $request->getBody();
    $data = json_decode($json, true);

    $ba = new bankAccounts($args["accountID"]);
    $ba->account=$data['account'];
    $ba->accountID=$data['accountID'];
    $ba->bankID = $data['bankID'];
    $ba->branchID = $data['branchID'];
    $ba->IBAN = $data['IBAN'];
    $ba->asID = $data['asID'];
    $ba->currency=$data['currencyID'];

    $baResponse = $ba->save();
    return $response->withStatus(200)
        ->write($baResponse);
});

$app->delete('/account/delete/{accountID}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/bankAccounts.php";

    $account=new bankAccounts($args["accountID"]);
    $usResponse=$account->delete(1);

    return $response->withStatus(200)
    ->write($usResponse);

});

//transactions
$app->get('/transactions/{asID}/{accountID}/{startDate}/{endDate}[/{transactionID}]', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/bankTransactions.php";
    //echo $request->getServerParams()["PHP_AUTH_USER"] ;
        $tx= new bankTransactions();
        $transactionID = isset($args["transactionID"]) ? $args["transactionID"] : 0;
        $startDate = date("Y-m-d",strtotime($args["startDate"]));
        $endDate = date("Y-m-d",strtotime($args["endDate"]));
        $txResult = $tx->getBankTransactions($args["accountID"],$args["asID"],$transactionID,$startDate,$endDate);        
        $txResponse = checkNull($tx->toJson);
   
    return $response->withStatus(200)
        ->write($txResponse);
});

$app->post('/transaction/save/{transactionID}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/bankTransactions.php";
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/invoices.php";
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/payments.php";


    $json = $request->getBody();
    $data = json_decode($json, true);

    $bt = new bankTransactions($args["transactionID"]);
    $bt->notes=$data['notes'];
    $bt->relatedInvoiceID=$data['relatedInvoiceID'];
    /*
    if ( $bt->relatedInvoiceID!=$data['relatedInvoiceID'] && $data['relatedInvoiceID']!="") {
        $invoice = invoices::getInvoiceFromMerchant($data['relatedInvoiceID']);
        if ($invoice->ID>0) {
            
            $bt->relatedInvoiceID=$data['relatedInvoiceID'];
            
            $payment = payments::getPaymentFromInvoice($invoice->ID);
            $payment->amount=$bt->transaction;
            $payment->paymentDate=$bt->tranDate;
            $payment->invoiceID=$invoice->ID;
            $payment->paymentMethod=3;
            $payment->save();
        }    
    } 
    */
    $bt->save();
    $btResponse = checkNull($bt->toJson());
    return $response->withStatus(200)
        ->write($btResponse);
});


//expenses
$app->get('/expenses/{asID}[/{expenseID}]', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/expenses.php";
    //echo $request->getServerParams()["PHP_AUTH_USER"] ;
        $expense= new expenses();
        $expenseResult = $expense->getExpenses($args["expenseID"],$args["asID"]);        
        $expenseResponse = checkNull($expense->toJson);
   
    return $response->withStatus(200)
        ->write($expenseResponse);
});

$app->get('/expenseCategories/{asID}[/{ecID}]', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/expenseCategories.php";
    //echo $request->getServerParams()["PHP_AUTH_USER"] ;
        $ecID = isset($data['ecID']) ? $data['ecID'] : 0;
        $ec= new expenseCategories();
        $ecResult = $ec->getExpenseCategories($ecID,$args["asID"]);        
        $ecResponse = checkNull($ec->toJson);
   
    return $response->withStatus(200)
        ->write($ecResponse);
});

$app->delete('/expense/delete/{expenseID}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/expenses.php";

    $expense=new expenses($args["expenseID"]);
    $usResponse=$expense->delete(1);

    return $response->withStatus(200)
    ->write($usResponse);

});

$app->post('/expense/save/{expenseID}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/expenses.php";

    $json = $request->getBody();
    $data = json_decode($json, true);

    $cs = new expenses($args["expenseID"]);
    $cs->merchant=$data['merchant'];
    $cs->expenseDate = $data['expenseDate'];
    $cs->amount = str_replace(",",".",$data['amount']);
    $cs->vatAmount = str_replace(",",".",$data['vatAmount']);
    $cs->totalAmount = str_replace(",",".",$data['totalAmount']);
    $cs->expenseAccount = $data['expenseAccount'];
    $cs->asID = $data['asID'];
    $cs->expenseCategory = $data['expenseCategory'];
    $cs->currency=$data['currency'];
    $cs->notes=$data['notes'];
    $cs->status=$data['status'];
    $cs->expenseNumber=$data['expenseNumber'];
    $cs->vatRate=$data['vatRate'];
    $csResponse = $cs->save();

     // insert Accounted
     $sql = "call postAccounting(".$csResponse.",3)";
     $cs->executenonquery($sql,NULL,true);

    return $response->withStatus(200)
        ->write($csResponse);
});

//invoices


$app->get('/invoices/{invoiceType}/{asID}/{invoiceID}[/{archieve}]', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/invoices.php";
        $arc = isset($args["archieve"]) ? $args["archieve"] : 0;
        $invoice= new invoices();
        $invoiceResult = $invoice->getInvoices($args["invoiceID"],$args["invoiceType"],$args["asID"],$arc);
        $invoiceResponse = checkNull($invoice->toJson);
   
    return $response->withStatus(200)
        ->write($invoiceResponse);
});

$app->get('/invoiceDueDates/{asID}/{invoiceType}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/invoices.php";
        $invoice= new invoices();
        $invoiceResult = $invoice->getInvoiceDueDates($args["asID"],$args["invoiceType"]);
        $invoiceResponse = checkNull($invoice->toJson);
   
    return $response->withStatus(200)
        ->write($invoiceResponse);
});

$app->get('/invoiceCustomerDueDates/{asID}/{customerID}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/invoices.php";
        $invoice= new invoices();
        $invoiceResult = $invoice->getCustomerInvoiceDueDates($args["asID"],$args["customerID"]);
        $invoiceResponse = checkNull($invoice->toJson);
   
    return $response->withStatus(200)
        ->write($invoiceResponse);
});

$app->get('/invoiceCustomerSummary/{asID}/{customerID}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/invoices.php";
        $invoice= new invoices();
        $invoiceResult = $invoice->getCustomerInvoiceSummary($args["asID"],$args["customerID"]);
        $invoiceResponse = checkNull($invoice->toJson);
   
    return $response->withStatus(200)
        ->write($invoiceResponse);
});

$app->delete('/invoice/delete/{invoiceID}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/invoices.php";

    $invoice=new invoices($args["invoiceID"]);
    $usResponse=$invoice->delete(1);

    return $response->withStatus(200)
    ->write($usResponse);

});

$app->post('/invoice/archieve/{invoiceID}/{archieve}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/invoices.php";

    $inv = new invoices($args["invoiceID"]);
    if ($args["archieve"]==1) {
        $inv->archieveDate=date("Y-m-d H:i");
    } else {
        $inv->archieveDate="null";
    }
    $aResponse=$inv->save();
    
    return $response->withStatus(200)
    ->write($aResponse);
});

$app->post('/invoice/clone/{invoiceID}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/invoices.php";

    $inv = new invoices();
    $result = $inv->invoiceClone($args["invoiceID"]);
    list($newInvoiceID)=mysqli_fetch_array($result);

    return $response->withStatus(200)
    ->write($newInvoiceID);
});

$app->get('/aggr/{asID}/{customerID}/{startDate}/{endDate}/{currencyID}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/payments.php";
    $us = new payments();
    $startDate = date("Y-m-d",strtotime($args["startDate"]));
    $endDate = date("Y-m-d",strtotime($args["endDate"]));
    $usResult = $us->getAggr($args["asID"],$args["customerID"],$startDate,$endDate,$args["currencyID"]);
    $usResponse = checkNull($us->toJson);

return $response->withStatus(200)
    ->write($usResponse);
});

$app->get('/balance/{asID}/{startDate}/{endDate}/{currencyID}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/payments.php";
    $us = new payments();
    $startDate = date("Y-m-d",strtotime($args["startDate"]));
    $endDate = date("Y-m-d",strtotime($args["endDate"]));
    $usResult = $us->getBalance($startDate,$endDate,$args["currencyID"],$args["asID"]);
    $usResponse = checkNull($us->toJson);

return $response->withStatus(200)
    ->write($usResponse);
});

$app->get('/balanceDetails/{asID}/{startDate}/{endDate}/{currencyID}/{accountID}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/payments.php";
    $us = new payments();
    $startDate = date("Y-m-d",strtotime($args["startDate"]));
    $endDate = date("Y-m-d",strtotime($args["endDate"]));
    $usResult = $us->getBalanceDetails($startDate,$endDate,$args["currencyID"],$args["asID"],$args["accountID"]);
    $usResponse = checkNull($us->toJson);

return $response->withStatus(200)
    ->write($usResponse);
});

$app->post('/invoice/payment/save[/{invoiceID}]', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/payments.php";

    $json = $request->getBody();
    $data = json_decode($json, true);
    //echo var_dump($data);
    //$data = $data[0];
    $pay = payments::getPaymentFromInvoice($args["invoiceID"]);
    $pay->invoiceID=$args["invoiceID"];
    $pay->paymentDate=date("Y-m-d H:i",strtotime($data['paymentDate']));
    $pay->amount=str_replace(",",".",$data['amount']);
    $pay->paymentAccount=$data['paymentAccount'];
    $pay->paymentMethod=$data['paymentMethod'];
    $pay->notes=$data['notes'];

    $usResponse = $pay->save();

    return $response->withStatus(200)
    ->write($usResponse);
});

$app->post('/invoice/save[/{invoiceID}]', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/invoices.php";
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/invoiceDetails.php";
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/rules.php";
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/chartOfAccounts.php";
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/invoicePeriods.php";

    $json = $request->getBody();
    $data = json_decode($json, true);
    //echo var_dump($data);
    //$data = $data[0];


    $us = new invoices($args["invoiceID"]);
    if ($us->status<=2)
    {

        $us->invoiceDate=date("Y-m-d H:i",strtotime($data['invoiceDate']));
        $us->invoiceType=$data['invoiceType'];
        $us->invoiceClass=$data['invoiceClass'];
        $us->invoiceCurrency=$data['invoiceCurrency'];
        $us->customerID=$data['customerID'];
        $us->description=$data['description'];
        $us->tags=isset($data['tags']) ? $data['tags'] : "";
        $us->autoGenerated=isset($data['autoGenerated']) ? $data['autoGenerated'] : 0;
        $us->notes=$data['notes'];
        $us->asID=$data['asID'];
        if ($data['invoiceType']==2) {
            $us->invoiceNumber=isset($data['invoiceNumber']) ? $data['invoiceNumber'] : "";
        }
        $us->isProforma=isset($data['isProforma']) ? $data['isProforma'] : 0;
        $us->dueDateOptions=isset($data['dueDateOptions']) ? $data['dueDateOptions'] : 0;
        $us->po=isset($data['po']) ? $data['po'] : "";
        $us->account = isset($data['account']) ? $data['account'] : "";

        if (isset($data['statusID'])) {
            if ($data['statusID']>0) {
                $us->status=$data['statusID'];
             } elseif (!$args["invoiceID"]>0) {
                $us->status=0;
             }
        }
        $usResponse = $us->save();

        if (isset($data['autoGenerate'])) {
            if ($data['autoGenerate']==1) { 
                $ip = invoicePeriods::getInvoicePeriodsFromInvoiceID($usResponse);
                $ip->invoicePeriod=$data['invoicePeriod'];
                $ip->periodLastDate=date("Y-m-d",strtotime($data['periodLastDate']));
                $ip->invoiceID=$usResponse;
                $ip->save();
            } else {
                $ip = invoicePeriods::getInvoicePeriodsFromInvoiceID($usResponse);
                $ip->delete(1);
            }
        } else {
            $ip = invoicePeriods::getInvoicePeriodsFromInvoiceID($usResponse);
            $ip->delete(1);
        }
        

        $ids = $data["invoiceDetails"];

        if ($ids>0) {

            $sql = "delete from invoiceDetails where invoiceID=".$usResponse;
            $us->executenonquery($sql,null,true);

            foreach ($ids as $line) {
                    $id = new invoiceDetails();
                    $id->invoiceID=$usResponse;
                    $id->rowNumber=$line["rowNumber"];
                    $id->item=$line["item"];
                    $id->vatRate=$line["vatRate"];
                    $id->vatAmount=str_replace(",",".",$line["vatAmount"]);
                    $id->amount=str_replace(",",".",$line["amount"]);
                    $id->unit=$line["unit"];
                    $id->totalAmount=str_replace(",",".",$line["totalAmount"]);
                    $id->productID=isset($line["productID"]) ? $line["productID"] : 0;
                    $id->save();
            }
        }
    } else {
        if (isset($data['statusID'])) {
            if ($data['statusID']==3) {
                $us->status=$data['statusID'];
            }
        }
        $us->isProforma=isset($data['isProforma']) ? $data['isProforma'] : 0;
        $us->account = isset($data['account']) ? $data['account'] : "";
        $usResponse=$us->save();
    }
    
    $rule = isset($data['rule']) ? $data['rule'] : false;
    if ($rule==true) {
        $coa = chartOfAccounts::getCoaFromAccount($data['asID'],$data['account']);

        $rules = new rules();
        $rules->customerID=$data['customerID'];
        $rules->accountID=$coa->ID;
        $rules->invoiceType=$data['invoiceType'];
        $rules->asID=$data['asID'];
        $rules->currency=$data['invoiceCurrency'];
        $rules->save();
    }

    return $response->withStatus(200)
        ->write($usResponse);
});

// users

$app->get('/userTypes', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/users.php";
    $us = new users();
    $sql = "select * from userTypes order by ID";
    $usResult = $us->executenonquery($sql,true);
    $usResponse = checkNull($us->toJson);

return $response->withStatus(200)
    ->write($usResponse);
});

$app->get('/users/login/{userName}/{password}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/users.php";
    $us = users::getAuth($args["userName"],$args["password"]);
    $usResponse = checkNull($us->toJson());
    return $response->withStatus(200)
        ->write($usResponse);
});

$app->get('/users/{asID}[/{userID}]', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/users.php";
    $us = new users();
    $userID = isset($args["userID"]) ? $args["userID"] : 0;
    $usResult = $us->getUserLists($userID,$args["asID"]);
    $usResponse = checkNull($us->toJson);

return $response->withStatus(200)
    ->write($usResponse);
});

$app->get('/userRoles/{userID}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/menuRights.php";
    $mr = new menuRights();
    //echo var_dump($data);
    $mr->getUserRole($args["userID"]);
    $usResponse = checkNull($mr->toJson);
    return $response->withStatus(200)
        ->write($usResponse);
});

$app->get('/user/forgot/{userName}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/user.php";
    $user = users::getUserFromUserName($args["userName"]);
    if ($user->ID > 0) {
        $rp = $user->randomPassword();
        $user->password = md5(rp);
        $mtResponse = checkNull($user->save());
        
        $body = $rp . " is your new password. ";
        $mail = new Mail($user->userName, ($user->firstName . " " . $user->lastName), ("Forgot Password"), $body);
        $mail->sendMail();
    } else {
        $mtResponse = 0;
    }
    return $response->withStatus(200)
        ->write($mtResponse);
});

$app->post('/user/save/{userID}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/users.php";
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/userAccess.php";

    $json = $request->getBody();
    $data = json_decode($json, true);
    //$data = $data;

    $us = new users($args["userID"]);
    $contractID = isset($data['contractID']) ? $data['contractID'] : 0;
    if ($contractID>0) {
        $us->contractID=$contractID;
        $us->approveDate=date("Y-m-d H:i");
    } else {
        $us->email = $data['email'];
        $us->password = md5($data['password']);
        $us->firstName = $data['firstName'];
        $us->lastName = $data['lastName'];
        $us->phone=isset($data['phone']) ? $data['phone'] : "";
        $us->userType=$data['userType'];
        $us->ownerID=$data['ownerID'];
        $us->clang=$data['clang'];
    }
    $usResponse = $us->save();

    if (isset($data["AS"])) {
       
        if ($usResponse>0) {
            $sql = "delete from userAccess where userID=".$usResponse;
            $us->executenonquery($sql,null,true);
            $ids = $data["AS"];
                foreach ($ids as $line) {
                    if ($line["uaID"]>=1) {
                        $ua= new userAccess();
                        $ua->userID=$usResponse;
                        $ua->asID=$line["ID"];
                        $ua->save();
                    }
                }
        }
    }

    return $response->withStatus(200)
        ->write($usResponse);
});

$app->delete('/user/delete/{userID}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/users.php";

    $user=new users($args["userID"]);
    $usResponse=$user->delete();

    return $response->withStatus(200)
    ->write($usResponse);

});

$app->delete('/rule/delete/{ruleID}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/rules.php";

    $rule=new rules($args["ruleID"]);
    $usResponse=$rule->delete(1);

    return $response->withStatus(200)
    ->write($usResponse);

});

// customers

$app->get('/customers/{asID}/{isVendor}[/{customerID}]', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/customers.php";
        $customers = new customers();
        $customerResult = $customers->getCustomers($args["customerID"],$args["asID"],$args["isVendor"]);
        $customerResponse = checkNull($customers->toJson);
   
    return $response->withStatus(200)
        ->write($customerResponse);
});

$app->delete('/customer/delete/{customerID}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/customers.php";

    $customer=new customers($args["customerID"]);
    $usResponse=$customer->delete(1);

    return $response->withStatus(200)
    ->write($usResponse);

});

$app->post('/customer/save/{customerID}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/customers.php";

    $json = $request->getBody();
    $data = json_decode($json, true);

    $cs = new customers($args["customerID"]);
    $cs->customerShortName = $data['customerShortName'];
    $cs->customer = $data['customer'];
    $cs->customerType = $data['customerType'];
    $cs->address = $data['address'];
    $cs->vatNumber = $data['vatNumber'];
    $cs->address = $data['address'];
    $cs->vatNumber = $data['vatNumber'];
    $cs->vatAuthority = $data['vatAuthority'];
    $cs->commerceAuthorityNo = $data['commerceAuthorityNo'];
    $cs->phone = $data['phone'];
    $cs->email = $data['email'];
    $cs->customerContact = $data['customerContact'];
    $cs->customerContactEmail = $data['customerContactEmail'];
    $cs->buildingNumber = $data['buildingNumber'];
    $cs->district = $data['district'];
    $cs->city = $data['city'];
    $cs->country = $data['country'];
    $cs->isVendor = $data['isVendor'];
    $cs->asID = $data['asID'];
    $cs->pbox = $data['pbox'];
    $cs->dueDateOptions=isset($data['dueDateOptions']) ? $data['dueDateOptions'] : 0;

    $csResponse = $cs->save();
    return $response->withStatus(200)
        ->write($csResponse);
});

// files

$app->delete('/files/delete/{fileID}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/files.php";

    $file=new files($args["fileID"]);
    $fileResponse=$file->delete(1);

    $path = dirname(dirname(__FILE__))."/Uploads/files/";
    $fileName = $path.$args["fileName"];
    unlink($fileName);

    return $response->withStatus(200)
    ->write($fileResponse);

});

$app->get('/files/{itemID}[/{table}]', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/files.php";
        $files = new files();
        $table = isset($args["table"]) ? $args["table"] : "";
        $filesResult = $files->getFiles($args["itemID"],$table);
        $filesResponse = checkNull($files->toJson);
   
    return $response->withStatus(200)
        ->write($filesResponse);
});

$app->post('/files/upload/{table}/{itemID}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/files.php";


    $json = $request->getBody();
    $data = json_decode($json, true);
    $path = dirname(dirname(__FILE__))."/Uploads/files/";
   
    $array = explode('.', $data['fileName']);
    $extension = end($array);

    $md5File = md5(date("Y-m-d H:i:s")).".".$extension;
    $fileName = $path.$md5File;

    // Base64 to file
    $imgdata = base64_decode($data["file"]);
    $f = finfo_open();
    $mime_type = finfo_buffer($f, $imgdata, FILEINFO_MIME_TYPE);
    $fl=new files();
    if ($fl->check_file_type($mime_type)) {

        $file = fopen($fileName, "wb");
        $bytes=fwrite($file, base64_decode($data["file"]));
        fclose($file);
        if ($bytes>0) {
            $f=new files();
            $f->fileName=$md5File;
            $f->tableName=$args["table"];
            $f->tableRowID=$args["itemID"];
            $f->userID=$data['userID'];
            $f->description=$data['description'];
            $fResponse=$f->save();
        } else {
            $fResponse=0;
        }
    } else {
        $fResponse=0;
    }
   
    return $response->withStatus(200)
        ->write($fResponse);
});

// team

$app->get('/members/{asID}[/{employeeID}]', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/employees.php";
        $members = new employees();
        $membersResult = $members->getMembers($args["employeeID"],$args["asID"]);
        $membersResponse = checkNull($members->toJson);
   
    return $response->withStatus(200)
        ->write($membersResponse);
});

$app->delete('/member/delete/{employeeID}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/employees.php";

    $members=new employees($args["employeeID"]);
    $membersResponse=$members->delete(1);

    return $response->withStatus(200)
    ->write($membersResponse);

});

$app->post('/member/save/{employeeID}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/employees.php";

    $json = $request->getBody();
    $data = json_decode($json, true);

    $cs = new employees($args["employeeID"]);
    $cs->TCKN = isset($data['TCKN']) ? $data['TCKN'] :"";
    $cs->name = isset($data['name']) ? $data['name'] : "";
    $cs->lastName = isset($data['lastName']) ? $data['lastName'] : "";
    $cs->email = isset($data['email']) ? $data['email'] : "";
    $cs->homePhone = isset($data['homePhone']) ? $data['homePhone'] : "";
    $cs->cellPhone = isset($data['cellPhone']) ? $data['cellPhone'] : "";
    $cs->birthday = isset($data['birthday']) ? $data['birthday'] : "";
    $cs->title = isset($data['title']) ? $data['title'] : "";
    $cs->startDate = isset($data['startDate']) ? $data['startDate'] : "";
    $cs->salary = isset($data['salary']) ? $data['salary'] : "";
    $cs->school = isset($data['school']) ? $data['school'] : "";
    $cs->department = isset($data['department']) ? $data['department'] : "";
    $cs->socialSecDoc = isset($data['socialSecDoc']) ? $data['socialSecDoc'] : "";
    $cs->idDoc = isset($data['idDoc']) ? $data['idDoc'] : "";
    $cs->gradDoc = isset($data['gradDoc']) ? $data['gradDoc'] : "";
    $cs->asID = $data['asID'];
    $cs->isNet = isset($data['isNet']) ? $data['isNet'] : 0;
    $cs->isMarried = isset($data['isMarried']) ? $data['isMarried'] : 0;
    $cs->numberOfKids = isset($data['numberOfKids']) ? $data['numberOfKids'] : 0;
    $cs->isSpouseWorking = isset($data['isSpouseWorking']) ? $data['isSpouseWorking'] : 0;
    $cs->payrollInfo = isset($data['payrollInfo']) ? $data['payrollInfo'] : "";
    
    $csResponse = $cs->save();
    return $response->withStatus(200)
        ->write($csResponse);
});


//chartofaccounts

$app->get('/chartOfAccounts/{asID}/{accounts}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/chartOfAccounts.php";
        $coa= new chartOfAccounts();
        $accounts = isset($args["accounts"]) ? $args["accounts"] : "";
        $coaResult = $coa->getCoa($args["asID"],$accounts);
        $coaResponse = checkNull($coa->toJson); 
    return $response->withStatus(200)
        ->write($coaResponse);
});


//products

$app->get('/products/{asID}/{isVendor}/{productID}[/{invoiceID}]', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/products.php";
        $products= new products();
        $invoiceID = isset($args["invoiceID"]) ? $args["invoiceID"] : 0;
        $productResult = $products->getProducts($args["productID"],$args["asID"],$args["isVendor"],$invoiceID);
        $productResponse = checkNull($products->toJson);
   
    return $response->withStatus(200)
        ->write($productResponse);
});

$app->delete('/product/delete/{productID}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/products.php";

    $product=new products($args["productID"]);
    $usResponse=$product->delete(1);

    return $response->withStatus(200)
    ->write($usResponse);

});

$app->post('/product/save/{productID}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/products.php";

    $json = $request->getBody();
    $data = json_decode($json, true);

    $cs = new products($args["productID"]);
    $cs->product=$data['product'];
    $cs->description = $data['description'];
    $cs->price = str_replace(",",".",$data['price']);
    $cs->accountBuy = str_replace(",",".",(isset($data['accountBuy']) ? $data['accountBuy'] :""));
    $cs->accountSell = str_replace(",",".",(isset($data['accountCell']) ? $data['accountCell'] : "")) ;
    $cs->taxRate = $data['taxRate'];
    $cs->asID = $data['asID'];
    $cs->isVendor = $data['isVendor'];
    $cs->currency = isset($data['currency']) ? $data['currency'] : "";
    $csResponse = $cs->save();
    return $response->withStatus(200)
        ->write($csResponse);
});

$app->delete('/xsltfile/delete/{xsltID}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/xsltfiles.php";

    $xf=new xsltfiles($args["xsltID"]);
    $xfResponse=$xf->delete(1);

    return $response->withStatus(200)
    ->write($xfResponse);

});

$app->post('/xsltfile/save/{xsltID}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/xsltfiles.php";

    $json = $request->getBody();
    $data = json_decode($json, true);

    $xf = new xsltfiles($args["xsltID"]);
    $xf->template=$data['template'];
    $xf->xsltfile=$data['xsltfile'];
    $xf->templateCode=$data['templateCode'];
    $xf->asID=$data['asID'];
    $xfResponse = $xf->save();
    return $response->withStatus(200)
        ->write($xfResponse);
});


$app->get('/xsltfiles/{asID}[/{xsltID}]', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/xsltfiles.php";
    $xsltID = isset($args["xsltID"]) ? $args["xsltID"] : 0;
        $xsltfiles = new xsltfiles();
        $xsltfilesResult = $xsltfiles->getxsltfiles($args["asID"],$xsltID);
        
        $xsltfilesResponse = checkNull($xsltfiles->toJson);
   
    return $response->withStatus(200)
        ->write($xsltfilesResponse);
});

// business entities

$app->get('/accountingSuppliers[/{acID}]', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/accountingSuppliers.php";
        $accountingSupplier = new accountingSuppliers();
        $accountingSupplierResult = $accountingSupplier->getAccountingSuppliers($args["acID"]);
        
        $accountingSupplierResponse = checkNull($accountingSupplier->toJson);
   
    return $response->withStatus(200)
        ->write($accountingSupplierResponse);
});

$app->get('/accountingSuppliersWithSupplierType/{parentID}/{supplierType}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/accountingSuppliers.php";
        $accountingSupplier = new accountingSuppliers();
        $accountingSupplierResult = $accountingSupplier->getAccountingSuppliersWithSupplierType($args["supplierType"],$args["parentID"]);
        
        $accountingSupplierResponse = checkNull($accountingSupplier->toJson);
   
    return $response->withStatus(200)
        ->write($accountingSupplierResponse);
});

$app->get('/contracts[/{contractID}]', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/contracts.php";
    $contractID = isset($_POST["contractID"]) ? $_POST["contractID"] : 0;
    $contracts= new contracts();
    $contractResult = $contracts->getContracts($contractID);
    
    $contractResponse = checkNull($contracts->toJson);
   
    return $response->withStatus(200)
        ->write($contractResponse);
});

$app->get('/activeContract', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/contracts.php";
    $contract= contracts::getActiveContract();    
    $contractResponse = checkNull($contract->toJson());
   
    return $response->withStatus(200)
        ->write($contractResponse);
});

$app->delete('/contract/delete/{contractID}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/contracts.php";

    $contract=new contracts($args["contractID"]);
    $contractResponse=$contract->delete(1);

    return $response->withStatus(200)
    ->write($contractResponse);

});

$app->post('/contract/save/{contractID}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/contracts.php";

    $json = $request->getBody();
    $data = json_decode($json, true);

    $contract = new contracts($args["contractID"]);
    $contract->contractTitle=$data['contractTitle'];
    $contract->contractDate=date("Y-m-d",strtotime($data["contractDate"]));
    $contract->userID=$data['userID'];
    $contract->contract=$data['contract'];
    $contract->isActive=$data['isActive'];
    $contractResponse = $contract->save();
    return $response->withStatus(200)
        ->write($contractResponse);
});

$app->get('/banners[/{bannerID}]', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/banners.php";
    $bannerID = isset($args["bannerID"]) ? $args["bannerID"] : 0;
    $banners= new banners();
    $bannerResult = $banners->getBanners($bannerID);
    
    $bannerResponse = checkNull($banners->toJson);
   
    return $response->withStatus(200)
        ->write($bannerResponse);
});

$app->get('/showBanners', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/banners.php";
    $banners= new banners();
    $bannerResult = $banners->showBanners();
    
    $bannerResponse = checkNull($banners->toJson);
   
    return $response->withStatus(200)
        ->write($bannerResponse);
});

$app->delete('/banner/delete/{bannerID}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/banners.php";

    $banner=new banners($args["bannerID"]);
    $bannerResponse=$banner->delete(1);

    return $response->withStatus(200)
    ->write($bannerResponse);

});

$app->post('/banner/save/{bannerID}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/banners.php";

    $json = $request->getBody();
    $data = json_decode($json, true);

    $banner = new banners($args["bannerID"]);
    $banner->banner=$data['banner'];
    $banner->startTime=date("Y-m-d H:i",strtotime($data["startTime"]));
    $banner->endTime=date("Y-m-d H:i",strtotime($data["endTime"]));
    $banner->photo=$data['photo'];
    $bannerResponse = $banner->save();
    return $response->withStatus(200)
        ->write($bannerResponse);
});


$app->get('/userAccountingSuppliers/{ownerID}/{userID}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/accountingSuppliers.php";
        $accountingSupplier = new accountingSuppliers();
        $accountingSupplierResult = $accountingSupplier->getUserAccountingSuppliers($args["ownerID"],$args["userID"]);
        
        $accountingSupplierResponse = checkNull($accountingSupplier->toJson);
   
    return $response->withStatus(200)
        ->write($accountingSupplierResponse);
});

$app->post('/accountingSuppliers/save/{acID}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/accountingSuppliers.php";

    $json = $request->getBody();
    $data = json_decode($json, true);

    $as = new accountingSuppliers($args["acID"]);
    $as->supplierShortName = isset($data['supplierShortName']) ? $data['supplierShortName'] :"";
    $as->supplier = isset($data['supplier']) ? $data['supplier'] :"";
    $as->supplierType = isset($data['supplierType']) ? $data['supplierType'] :0;
    $as->tckVKNNo = isset($data['tckVKNNo']) ? $data['tckVKNNo'] :"";
    $as->address = isset($data['address']) ? $data['address'] :"";
    $as->vatNumber = isset($data['vatNumber']) ? $data['vatNumber'] :"";
    $as->vatAuthority = isset($data['vatAuthority']) ? $data['vatAuthority'] :"";
    $as->commerceAuthorityNo = isset($data['commerceAuthorityNo']) ? $data['commerceAuthorityNo'] :"";
    $as->phone = isset($data['phone']) ? $data['phone'] :"";
    $as->email = isset($data['email']) ? $data['email'] :"";
    $as->supplierContact = isset($data['supplierContact']) ? $data['supplierContact'] :"";
    $as->supplierContactEmail = isset($data['supplierContactEmail']) ? $data['supplierContactEmail'] :"";
    $as->buildingNumber = isset($data['buildingNumber']) ? $data['buildingNumber'] :"";
    $as->district = isset($data['district']) ? $data['district'] :"";
    $as->city = isset($data['city']) ? $data['city'] :"";
    $as->country = isset($data['country']) ? $data['country'] :"";
    $as->apiUrl = isset($data['apiUrl']) ? $data['apiUrl'] :"";
    $as->apiUser = isset($data['apiUser']) ? $data['apiUser'] :"";
    $as->apiPass = isset($data['apiPass']) ? $data['apiPass'] :"";
    $as->notes = isset($data['notes']) ? $data['notes'] :"";
    $as->ownerID = isset($data['ownerID']) ? $data['ownerID'] :0;
    $as->parentID = $args["acID"];
    $as->channelID = isset($data['channelID']) ? $data['channelID'] :0;
    $as->distributorID = isset($data['distributorID']) ? $data['distributorID'] :0;
    $as->sellerID = isset($data['sellerID']) ? $data['sellerID'] :0;
    if (isset($data["autoInvoiceNumber"])) {
        if ($data["autoInvoiceNumber"]==true) {
            $as->autoInvoiceNumber=1;
            $as->invoicePreFix=isset($data['invoicePreFix']) ? $data['invoicePreFix'] :"";
            $as->invoiceNumber=isset($data['invoiceNumber']) ? $data['invoiceNumber'] :"";
        } else {
            $as->autoInvoiceNumber=0;
            $as->invoicePreFix="";
            $as->invoiceNumber="";

        }
    }
    $asResponse = $as->save();
    return $response->withStatus(200)
        ->write($asResponse);
});

$app->delete('/accountingSuppliers/delete/{acID}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/accountingSuppliers.php";

    $as=new accountingSuppliers($args["acID"]);
    $asResponse=$as->delete();

    return $response->withStatus(200)
    ->write($asResponse);

});

// menus

$app->get('/menus[/{userID}]', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/menus.php";
    $menus = new menus();
    $menuResult = $menus->getMenus($args["userID"]);  
    
    $menuResponse = checkNull($menus->toJson);

    return $response->withStatus(200)
    ->write($menuResponse);
});
$app->get('/babs/{asID}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/babs.php";
    $babs = new babs();
    $babsResult = $babs->getBABSwithSupplier($args["asID"]);  
    
    $babsResponse = checkNull($babs->toJson);

    return $response->withStatus(200)
    ->write($babsResponse);
});
$app->post('/babs/{year}/{month}/{withMail}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/invoices.php";
    require_once dirname(dirname(__FILE__)) . "/BL/communication.php";

    $invoice = new invoices();
    $resultInv = $invoice->generateBABS($args["year"],$args["month"]);
    
    if ($args["withMail"]==1) {
        $result = $invoice->getBABS($args["year"],$args["month"]);
        $term = str_pad($args["month"], 2, '0', STR_PAD_LEFT).".".$args["year"];
        $subject =$term. " Mutabakati - ";
        while($row=mysqli_fetch_array($result)) {
            if ($row["email"]!="") {
                $template = file_get_contents ( "https://crmapi.efdigitalcodes.com/templates/babsmailtemplate.html" );
                $tl = str_replace ( "@customer", $row["customer"], $template );
                $tl = str_replace ( "@taxnumber", $row["vatNumber"], $tl );
                $tl = str_replace ( "@amount", $row["amount"], $tl );
                $tl = str_replace ( "@formType", $row["formType"], $tl );
                $tl = str_replace ( "@term", $term, $tl );
                $tl = str_replace ( "@invoiceCount", $row["invoiceCount"], $tl );
                $token=md5($row["month"].":".$row["year"].":".$row["asID"].":".$row["customerID"]);
                $tl = str_replace ( "@token", $token, $tl );
                
                $sm = new Mail($row["email"], $row["customer"],  ($subject.$row["supplier"]),$tl);
                $sm->sendMail();
                
            }
        }
    }

    $asResponse = resultInv;
    return $response->withStatus(200)
        ->write($asResponse);
}); 

// receipts

$app->post('/receipts/import/{asID}', function ($request, $response, $args) {
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/expenses.php";
    require_once dirname(dirname(__FILE__)) . "/BL/Tables/expenseCategories.php";

    $json = $request->getBody();
    $data = json_decode($json, true);
    $path = dirname(dirname(__FILE__))."/Uploads/receipts/";
    $fileName = $path.$data['fileName'];


    // Base64 to file

    $file = fopen($fileName, "wb");
    fwrite($file, base64_decode($data["file"]));
    fclose($file);

    $row = 0;
    if (($handle = fopen($fileName, "r")) !== FALSE) {
        while (($datal = fgetcsv($handle, 1000, ",")) !== FALSE) {
            //echo var_dump($data);
            $num = count($datal);
            $row++;
            if ($row>1) {
                if (floatval($datal[5])>0 && strlen(ltrim($datal[0]))>0) {
                $cs = expenses::getExpenseFromNumber($datal[0]);
                $cs->expenseNumber=$datal[0];
                $cs->merchant=$datal[3];
                $cs->expenseDate = $datal[1];
                $cs->amount = str_replace(",",".",round($datal[5]/1.18,2));
                $cs->vatAmount = str_replace(",",".",round($datal[5]-$datal[5]/1.18,2));
                $cs->totalAmount = str_replace(",",".",$datal[5]);
                switch ($datal[9]){
                    case "Cash on Hand":
                        $cs->expenseAccount = 1;
                        break;
                    default :
                        $cs->expenseAccount = 2;

                }
                $cs->asID = $args['asID'];

                $ec = expenseCategories::getExpenseCategoriesID($args['asID'],$datal[2]);
                $cs->expenseCategory=$ec->ID;
                
                /*switch ($datal[2]){
                    case "Meals and Entertainment":
                        $cs->expenseCategory = 1;
                        break;
                    default :
                        $cs->expenseCategory = 2;

                }
                */
                $cs->vatRate=18;
                $cs->currency=1;
                $cs->notes=isset($datal['notes']) ? $datal['notes'] : "";
                $cs->status=1;    
                $csID = $cs->save();

                
                // insert Accounted
                $sql = "call postAccounting(".$csID.",3)";
                $cs->executenonquery($sql,NULL,true);


                }
            }        
    

        }
        fclose($handle);
    }
    if ($row>1) {
        $csResponse = 1;
    } else {
        $csResponse = 0;    
    }
    return $response->withStatus(200)
        ->write($csResponse);
});

// izibiz services
$app->get('/checkUser/{vkn}/{title}', function ($request, $response, $args) {
    $client = new SoapClient(izibiz::apiURL);
    $Req["USER_NAME"] =	izibiz::apiUser;
    $Req["PASSWORD"] = izibiz::apiPass;
    $RequestHeader["SESSION_ID"] = "-1";
    $Req["REQUEST_HEADER"] = $RequestHeader;
    $Res = $client->Login($Req);

    $Request = array(
        "REQUEST_HEADER"	=>	array(
            "SESSION_ID"	=>	$Res->SESSION_ID,
            "COMPRESSED"	=>	"N"
                                ),
            "USER"          =>	($args["vkn"]!="" && $args["vkn"]!="null") ?
                                    array("IDENTIFIER" =>$args["vkn"]) :   array("TITLE" =>$args["title"]),                                 
            "DOCUMENT_TYPE"	=>	"INVOICE" 
    );
    $sendF = $client->CheckUser($Request);
    $babsResponse = json_encode($sendF);
    return $response->withStatus(200)
    ->write($babsResponse);
});


$app->run();


function checkNull($value){
    if ($value == "null") {
        return "{'No record'}";
    } else {
        return $value;
    }
}

?>



