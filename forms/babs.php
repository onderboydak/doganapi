<?php
header("Content-type: text/html; charset=utf-8");
date_default_timezone_set('Europe/Istanbul');
setlocale(LC_ALL, "tr_TR");
require_once dirname(dirname(__FILE__)) . "/BL/Tables/babs.php";
$token=isset($_GET["token"]) ? $_GET["token"] : "";
if ($token=="") {
    echo "Token problem!";
    exit();
}
$form = babs::getbabs($token);
$term = str_pad($form->month, 2, '0', STR_PAD_LEFT).".".$form->year;
if ($form->ID>0) {
?>
<!doctype html>
<html lang="tr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Mutabakat">
    <meta name="author" content="Onder Boydak">
    <meta name="generator" content="V.1.0">
    <title>Mutabakat</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body>  
    <div class="container" style="margin-top:5%;">
            <?php
                    if ($form->responseDate!="") {
                       echo $form->responseDate. " tarihinde cevabiniz alindi!<br>Tesekkurler.";      
                    }
                    elseif (isset($_POST['submit'])) { 
                        $form->response=($_POST["response"]=="on") ? 1 : 0;
                        $form->responseDate=date("Y-m-d H:i");

                        $md5File="";

                        if (! empty ( $_FILES ["responseFile"] ) && check_file_type()) {
                            $myFile = $_FILES ["responseFile"];
                            if ($myFile ["error"] !== UPLOAD_ERR_OK) {
                                echo "<p>An error occurred.</p>";
                                exit();
                            }
                            $name = preg_replace ( "/[^A-Z0-9._-]/i", "_", $myFile ["name"] );
                            
                            $i = 0;
                            $parts = pathinfo ( $name );
                            while ( file_exists ( dirname ( dirname ( __FILE__ ) ) . "/Uploads/babs/" . $name ) ) {
                                $i ++;
                                $name = $parts ["filename"] . "-" . $i . "." . $parts ["extension"];
                            }
                            $md5File = md5($name).".". $parts ["extension"] ;
                            $success = move_uploaded_file ( $myFile ["tmp_name"], dirname ( dirname ( __FILE__ ) ) . "/Uploads/babs/" .$md5File );
                            if (! $success) {
                                //echo "<p>Unable to save file.</p>";
                            }
                        } else {
                            //echo "<p>Unable to save file.</p>";
                        }    

                        $form->responseFile=$md5File;
                        $form->save();    
                        echo "Tesekkurler..."; 
                    }  else {
            ?>

            <h3><?php echo $form->customer;?></h3>
            <p><?php echo $term." Donemine ait Form ".$form->formType." bilgileriniz asagidadir.";?></p>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label col-form-label-sm">VN:</label>
                <label class="col-sm-3 col-form-label col-form-label-sm"><?php echo $form->vatNumber;?></label>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label col-form-label-sm">Form:</label>
                <label class="col-sm-3 col-form-label col-form-label-sm"><?php echo $form->formType;?></label>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label col-form-label-sm">Evrak Sayisi:</label>
                <label class="col-sm-3 col-form-label col-form-label-sm"><?php echo $form->invoiceCount;?></label>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label col-form-label-sm">Tutar:</label>
                <label class="col-sm-3 col-form-label col-form-label-sm"><?php echo $form->amount;?></label>
            </div>
            <hr>
            <form method="post" enctype="multipart/form-data" action="">
                <input type="hidden" id="token" name="token" value="<?php echo $token;?>">
                <div class="form-group row" style="margin-left:5px;">
                    <div class="custom-control custom-radio">
                        <input id="response1" name="response" type="radio" class="custom-control-input" checked>
                        <label class="custom-control-label" for="response1">Mutabikiz</label>
                    </div>&nbsp;
                    <div class="custom-control custom-radio">
                        <input id="response0" name="response" type="radio" class="custom-control-input" >
                        <label class="custom-control-label" for="response0">Mutabik Degiliz</label>
                    </div>
                    
                </div>
                <div class="form-group row">
                     <label class="col-sm-2 col-form-label col-form-label-sm">PDF Dosya:</label>
                    <div class="col-sm-4">
                        <input
                            type="file" class="form-control form-control-sm" id="responseFile" accept=".pdf"
                            name="responseFile" placeholder="Dosya"
                            value="" maxlength="100"> 
                    </div> 
                </div>
                <div class="form-group row">
                    <div class="col-sm-1">
                            <button type="submit" name="submit" class="btn btn-primary btn-sm btn-block">Gonder</button> 
                    </div>        
                </div>
            </form>
                    <?php } ?>
    </div>
</body>

</html>
<?php } else {
    echo "Token Problem !";

}
function check_file_type() {
    $mime_type = file_mime_type($_FILES['responseFile']);
    
    switch ($mime_type) {
       
        case 'application/pdf': {
            return true;
        }
        
        default: {
            return false;
        }
    }
}

function file_mime_type($file)
{
    // We'll need this to validate the MIME info string (e.g. text/plain; charset=us-ascii)
    $regexp = '/^([a-z\-]+\/[a-z0-9\-\.\+]+)(;\s.+)?$/';
    /* Fileinfo extension - most reliable method
     *
     * Unfortunately, prior to PHP 5.3 - it's only available as a PECL extension and the
     * more convenient FILEINFO_MIME_TYPE flag doesn't exist.
     */
    if (function_exists('finfo_file'))
    {
        $finfo = finfo_open(FILEINFO_MIME);
        if (is_resource($finfo)) // It is possible that a FALSE value is returned, if there is no magic MIME database file found on the system
        {
            $mime = @finfo_file($finfo, $file['tmp_name']);
            finfo_close($finfo);
            /* According to the comments section of the PHP manual page,
             * it is possible that this function returns an empty string
             * for some files (e.g. if they don't exist in the magic MIME database)
             */
            if (is_string($mime) && preg_match($regexp, $mime, $matches))
            {
                $file_type = $matches[1];
                return $file_type;
            }
        }
    }
    /* This is an ugly hack, but UNIX-type systems provide a "native" way to detect the file type,
     * which is still more secure than depending on the value of $_FILES[$field]['type'], and as it
     * was reported in issue #750 (https://github.com/EllisLab/CodeIgniter/issues/750) - it's better
     * than mime_content_type() as well, hence the attempts to try calling the command line with
     * three different functions.
     *
     * Notes:
     *	- the DIRECTORY_SEPARATOR comparison ensures that we're not on a Windows system
     *	- many system admins would disable the exec(), shell_exec(), popen() and similar functions
     *	  due to security concerns, hence the function_exists() checks
     */
    if (DIRECTORY_SEPARATOR !== '\\')
    {
        $cmd = 'file --brief --mime ' . escapeshellarg($file['tmp_name']) . ' 2>&1';
        if (function_exists('exec'))
        {
            /* This might look confusing, as $mime is being populated with all of the output when set in the second parameter.
             * However, we only neeed the last line, which is the actual return value of exec(), and as such - it overwrites
             * anything that could already be set for $mime previously. This effectively makes the second parameter a dummy
             * value, which is only put to allow us to get the return status code.
             */
            $mime = @exec($cmd, $mime, $return_status);
            if ($return_status === 0 && is_string($mime) && preg_match($regexp, $mime, $matches))
            {
                $file_type = $matches[1];
                return $file_type;
            }
        }
        if ( (bool) @ini_get('safe_mode') === FALSE && function_exists('shell_exec'))
        {
            $mime = @shell_exec($cmd);
            if (strlen($mime) > 0)
            {
                $mime = explode("\n", trim($mime));
                if (preg_match($regexp, $mime[(count($mime) - 1)], $matches))
                {
                    $file_type = $matches[1];
                    return $file_type;
                }
            }
        }
        if (function_exists('popen'))
        {
            $proc = @popen($cmd, 'r');
            if (is_resource($proc))
            {
                $mime = @fread($proc, 512);
                @pclose($proc);
                if ($mime !== FALSE)
                {
                    $mime = explode("\n", trim($mime));
                    if (preg_match($regexp, $mime[(count($mime) - 1)], $matches))
                    {
                        $file_type = $matches[1];
                        return $file_type;
                    }
                }
            }
        }
    }
    // Fall back to the deprecated mime_content_type(), if available (still better than $_FILES[$field]['type'])
    if (function_exists('mime_content_type'))
    {
        $file_type = @mime_content_type($file['tmp_name']);
        if (strlen($file_type) > 0) // It's possible that mime_content_type() returns FALSE or an empty string
        {
            return $file_type;
        }
    }
    return $file['type'];
}

?>



